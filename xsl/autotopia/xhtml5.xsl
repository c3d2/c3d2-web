<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:str="http://exslt.org/strings"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
                exclude-result-prefixes="xsl date str dc itunes">

  <xsl:include href="../common.xsl" />

  <xsl:output method="xml"
              version="1.0"
              encoding="utf-8"
              indent="yes"/>
  <xsl:strip-space elements="p ul td"/>

  <xsl:template match="/page">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>

    <xsl:variable name="title">
      <xsl:choose>
        <xsl:when test="/page/@pagetitle">
          <xsl:value-of select="/page/@pagetitle"/>
        </xsl:when>
        <xsl:when test="/page/@title">
          <xsl:value-of select="/page/@title"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:comment><xsl:text> missing title </xsl:text></xsl:comment>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <html lang="de" xml:lang="de" dir="ltr">
      <xsl:call-template name="htmlhead">
        <xsl:with-param name="title" select="$title"/>
      </xsl:call-template>
      <body>
        <div class="wrapper">
          <nav>
            <ul>
              <li class="index">
                <a title="Start" href="index.html">Start</a>
              </li>
              <!-- <li> -->
              <!--   <a href="about.html">Über</a> -->
              <!-- </li> -->
              <!-- <li> -->
              <!--   <a href="cfp.html">CfP</a> -->
              <!-- </li> -->
              <!-- <li> -->
              <!--   <a href="fahrplan.html">Programm</a> -->
              <!-- </li> -->
            </ul>
            <div class="a-content">
              <a href="{$baseurl}index.html">
                <img title="Autotopia, Header" alt="Autotopia, Header" class="header-image" src="{$baseurl}autotopia2_flat.svg"/>
              </a>
            </div>
          </nav>
          <article>

            <input type="radio" id="lang-de" name="langselect" checked="" />
            <input type="radio" id="lang-en" name="langselect"/>
            <div class="a-content vertical-header">
              <div class="a-item">
                <label lang="en"    for="lang-de"><h1>Auf deutsch anzeigen</h1></label>
                <label lang="de-DE" for="lang-en"><h1>Switch to english</h1></label>
              </div>
            </div>
            <div class="a-content a-main">
              <xsl:apply-templates/>
            </div>
            <div class="a-panel a-content">
              <div class="a-item">
                <h1>meta</h1>
                <p>
                  <a href="https://wiki.c3d2.de/GesellschaftMachtTechnik">wiki</a>
                  |
                  <a href="https://hackmd.c3d2.de/Autotopia">pad</a>
                </p>
                <p>
                  email: <a href="mailto:autotopia@c3d2.de">autotopia@c3d2.de</a>
                </p>
                <!--<p>
                  pgp: <a href="{$baseurl}autotopia@c3d2.de.asc">
                  <span lang="en">public key</span>
                  <span lang="de-DE">öffentlicher Schlüssel</span>
                </a>
                | AA02 D6BC 2B6B 5264 E952 145C 54F8 77CE CBC2 D22E
                </p>-->
                <p>
                  mastodon: <a href="https://chaos.social/tags/autotopiadd">#AutotopiaDD</a> via <a href="https://chaos.social/@autotopia">@autotopia</a>
                </p>
                <p>
                  twitter: <a href="https://twitter.com/hashtag/AutotopiaDD?src=hash">#AutotopiaDD</a> via <a href="https://twitter.com/AutotopiaDD">@AutotopiaDD</a>
                </p>
              </div>
              <div class="a-item">
                <h1 lang="en">events</h1>
                <h1 lang="de-DE">veranstaltungen</h1>
                <p>
                  <table>
                    <xsl:for-each select="document('../../autotopia-events.xml')/page/news/newsfile">
                      <xsl:sort select="date:seconds(document(concat('../../', .))/page/event/start)" data-type="number" order="ascending" />
                      <xsl:apply-templates mode="event-list" select="document(concat('../../', .))/page/event">
                        <xsl:with-param name="file" select="."/>
                      </xsl:apply-templates>
                    </xsl:for-each>
                  </table>
                </p>
              </div>
            </div>

          </article>
        </div>
        <footer>
          <a href="{$baseurl}code_of_conduct.html">Code of Conduct</a>
          <a href="{$baseurl}datenschutz.html">Dateschutzerklärung</a>
        </footer>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="htmlhead">
    <xsl:param name="title"/>
    <head>
      <meta charset="utf-8" />
      <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
      <meta name="description" content="Veranstaltungsreihe die sich mit gesellschaftlichen Auswirkungen von Automatiserung beschäftigt" />
      <meta name="keywords" content="automatisierung" />
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
      <meta name="robots" content="index,follow" />
      <meta name="language" content="German" />
      <link rel="stylesheet" type="text/css" href="{$baseurl}autotopia.css" />
      <link rel="shortcut icon" href="{$baseurl}favicon.ico" type="image/x-icon" />
      <!--link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="news-rss.xml" />
      <link rel="alternate" type="application/atom+xml" title="Atom 1.0" href="news-atom.xml" /-->
      <title>
        <xsl:text>Autotopia — </xsl:text>
        <xsl:value-of select="$title" />
      </title>
    </head>
  </xsl:template>

  <xsl:template match="news-list">
    <xsl:variable name="prefix" select="@prefix"/>
    <xsl:variable name="details" select="@details"/>

    <xsl:for-each select="document('../../news.xml')/page/news/newsfile">
      <xsl:sort select="date:seconds(document(concat('../../', .))/item/@date)" data-type="number" order="descending" />
      <xsl:variable name="newsfile" select="."/>

      <xsl:for-each select="document(concat('../../', $newsfile))/item">
        <xsl:if test="((string-length($prefix) = 0) or starts-with($newsfile, concat('content/news/', $prefix))) and (($details != 'resources-only') or (count(resource) &gt; 0))">

          <h3 class="summary">
            <date>
              <xsl:call-template name="get-short-datestring">
                <xsl:with-param name="date" select="@date"/>
              </xsl:call-template>
            </date>
            <a href="{$baseurl}/news/{substring-before(substring-after($newsfile, 'content/'), '.xml')}.html" class="url">
              <xsl:value-of select="@title"/>
            </a>
          </h3>
          <xsl:apply-templates/>
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="item/image">
    <!-- Nix Bilder in den Autotopia-News -->
  </xsl:template>

  <xsl:template match="event">
    <div class="a-item">
      <xsl:apply-templates/>
    </div>
  </xsl:template>
  
  <xsl:template match="event/title">
    <h1 lang="{@lang}" class="a-event-title header">
      <xsl:copy-of select="./node()"/>
    </h1>
  </xsl:template>

  <xsl:template match="event/start">
    <p datetime="{./text()}" class="a-event-start">
      <xsl:value-of select="format-number(date:day-in-month(.), '00')"/>
      <xsl:text>.</xsl:text>
      <xsl:value-of select="format-number(date:month-in-year(.), '00')"/>
      <xsl:text>. </xsl:text>
      <xsl:value-of select="format-number(date:hour-in-day(.), '00')"/>
      <xsl:text>:</xsl:text>
      <xsl:value-of select="format-number(date:minute-in-hour(.), '00')"/>
    </p>
  </xsl:template>

  <xsl:template match="event/location">
    <p lang="{@lang}" class="a-event-location">
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="event/speaker">
    <p lang="{@lang}" class="a-event-speaker">
      <xsl:apply-templates/>
    </p>
  </xsl:template>
  
  <xsl:template match="event/description">
    <div  lang="{@lang}" class="a-event-description">
      <xsl:copy-of select="./node()"/>
    </div>
  </xsl:template>
  
  <xsl:template match="table" >
    <xsl:copy-of select="."/>
  </xsl:template>
  
  <xsl:template match="h1" >
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template match="h2" >
    <xsl:copy-of select="."/>
  </xsl:template>

  <xsl:template mode="event-list" match="event">
    <xsl:param name="file"/>

    <tr>
      <td>
        <time datetime="{start/text()}">
          <xsl:value-of select="format-number(date:day-in-month(start), '00')"/>
          <xsl:text>.</xsl:text>
          <xsl:value-of select="format-number(date:month-in-year(start), '00')"/>
          <xsl:text>. </xsl:text>
          <xsl:value-of select="format-number(date:hour-in-day(start), '00')"/>
          <xsl:text>:</xsl:text>
          <xsl:value-of select="format-number(date:minute-in-hour(start), '00')"/>
        </time>
      </td>
      <td class="event-list-location">
        <xsl:value-of select="location"/>
      </td>
      <td>
        <a href="{$baseurl}{str:replace(str:replace($file, 'content/autotopia/', ''), '.xml', '.html')}">
          <xsl:for-each select="title">
            <span lang="{@lang}" class="event-title">
              <xsl:copy-of select="./node()"/>
            </span>
          </xsl:for-each>
        </a>
        <span lang="en"> by </span>
        <span lang="de-DE"> von </span>
        <xsl:value-of select="speaker"/>
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
