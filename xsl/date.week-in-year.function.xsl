<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:func="http://exslt.org/functions" xmlns:date="http://exslt.org/dates-and-times" extension-element-prefixes="date func">
<!-- clou'd from http://exslt.org/date/functions/week-in-year/index.html (by "Jeni Tennison" <mail@jenitennison.com>) -->
<xsl:param name="date:date-time" select="'2000-01-01T00:00:00Z'"/>

<date:month-lengths>
   <date:month>31</date:month>
   <date:month>28</date:month>
   <date:month>31</date:month>
   <date:month>30</date:month>
   <date:month>31</date:month>
   <date:month>30</date:month>
   <date:month>31</date:month>
   <date:month>31</date:month>
   <date:month>30</date:month>
   <date:month>31</date:month>
   <date:month>30</date:month>
   <date:month>31</date:month>
</date:month-lengths>

<func:function name="date:week-in-year">
   <xsl:param name="date-time">
      <xsl:choose>
         <xsl:when test="function-available('date:date-time')">
            <xsl:value-of select="date:date-time()"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$date:date-time"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:param>

   <xsl:variable name="neg" select="starts-with($date-time, '-')"/>

   <xsl:variable name="dt-no-neg">
      <xsl:choose>
         <xsl:when test="$neg or starts-with($date-time, '+')">
            <xsl:value-of select="substring($date-time, 2)"/>
         </xsl:when>
         <xsl:otherwise>
            <xsl:value-of select="$date-time"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>

   <xsl:variable name="dt-no-neg-length" select="string-length($dt-no-neg)"/>

   <xsl:variable name="timezone">
      <xsl:choose>
         <xsl:when test="substring($dt-no-neg, $dt-no-neg-length) = 'Z'">Z</xsl:when>
         <xsl:otherwise>
            <xsl:variable name="tz" select="substring($dt-no-neg, $dt-no-neg-length - 5)"/>
            <xsl:if test="(substring($tz, 1, 1) = '-' or substring($tz, 1, 1) = '+') and substring($tz, 4, 1) = ':'">
               <xsl:value-of select="$tz"/>
            </xsl:if>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>

   <xsl:variable name="week">
      <xsl:if test="not(string($timezone)) or $timezone = 'Z' or (substring($timezone, 2, 2) &lt;= 23 and substring($timezone, 5, 2) &lt;= 59)">
         <xsl:variable name="dt" select="substring($dt-no-neg, 1, $dt-no-neg-length - string-length($timezone))"/>
         <xsl:variable name="dt-length" select="string-length($dt)"/>
         <xsl:variable name="year" select="substring($dt, 1, 4)"/>
         <xsl:variable name="leap" select="(not($year mod 4) and $year mod 100) or not($year mod 400)"/>
         <xsl:variable name="month" select="substring($dt, 6, 2)"/>
         <xsl:variable name="day" select="substring($dt, 9, 2)"/>
         <xsl:if test="number($year) and substring($dt, 5, 1) = '-' and $month &lt;= 12 and substring($dt, 8, 1) = '-' and $day &lt;= 31 and ($dt-length = 10 or (substring($dt, 11, 1) = 'T' and substring($dt, 12, 2) &lt;= 23 and substring($dt, 14, 1) = ':' and substring($dt, 15, 2) &lt;= 59 and substring($dt, 17, 1) = ':' and substring($dt, 18) &lt;= 60))">
            <xsl:variable name="month-days" select="sum(document('')/*/date:month-lengths/date:month[position() &lt; $month])"/>
            <xsl:value-of select="date:_week-in-year($month-days + $day + ($leap and $month &gt; 2), $year)"/>
         </xsl:if>
      </xsl:if>
   </xsl:variable>

   <func:result select="number($week)"/>

</func:function>

<func:function name="date:_week-in-year">
   <xsl:param name="days"/>
   <xsl:param name="year"/>
   <xsl:variable name="previous-year" select="$year - 1"/>
   <xsl:variable name="day-of-week" select="($previous-year + floor($previous-year div 4) - floor($previous-year div 100) + floor($previous-year div 400) + $days) mod 7"/>
   <xsl:variable name="dow">
      <xsl:choose>
         <xsl:when test="$day-of-week"><xsl:value-of select="$day-of-week"/></xsl:when>
         <xsl:otherwise>7</xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <xsl:variable name="start-day" select="($days - $dow + 7) mod 7"/>
   <xsl:variable name="week-number" select="floor(($days - $dow + 7) div 7)"/>
    <xsl:variable name="is-leap" select="1*(($year mod 4 = 0 and $year mod 100 != 0) or ($year mod 400 = 0))"/>

<!-- vv01f: debug -->
<!--
	<xsl:if test="$days &gt; 362">
	   <xsl:message>
		   <xsl:value-of select="$year"/>: 
		   <xsl:value-of select="$days"/> days
		   <xsl:value-of select="$is-leap"/> &lt;= leap?
		   <xsl:value-of select="$week-number"/> week
		   <xsl:value-of select="$start-day"/> start day
	   </xsl:message>
	</xsl:if>
-->

   <xsl:choose>
      <xsl:when test="($days - $is-leap) &gt; 363">
         <func:result select="1"/>
      </xsl:when>
      <xsl:when test="$start-day &gt;= 4">
         <func:result select="$week-number + 1"/>
      </xsl:when>
      <xsl:otherwise>
         <xsl:choose>
            <xsl:when test="not($week-number)">
               <func:result select="date:_week-in-year(365 + ((not($previous-year mod 4) and $previous-year mod 100) or not($previous-year mod 400)), $previous-year)"/>
            </xsl:when>
            <xsl:otherwise>
               <func:result select="$week-number"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:otherwise>
   </xsl:choose>
</func:function>
<!-- vv01f: rewrite wg. Fehler "53. KW statt 1. KW Folgejahr" in 2024; Ausgabe ist aber NaN … -->
<!--
<func:function name="date:week-in-year">
    <xsl:param name="date-time"/>

    <xsl:variable name="year" select="substring($date-time, 1, 4)"/>
    <xsl:variable name="month" select="substring($date-time, 6, 2)"/>
    <xsl:variable name="day" select="substring($date-time, 9, 2)"/>

    <xsl:variable name="is-leap" select="($year mod 4 = 0 and $year mod 100 != 0) or ($year mod 400 = 0)"/>

    <xsl:variable name="day-of-year">
      <xsl:choose>
         <xsl:when test="function-available('date:day-in-year')">
            <xsl:value-of select="date:day-in-year($date-time)"/>
         </xsl:when>
         <xsl:otherwise>
            ToDo: Implement adding $days-of-months-before + $days-in-current-month, take care of $is-leap
         </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="jan1weekday" select="(number($year + '-01-01') mod 7) + 1"/>

    <xsl:variable name="offset">
        <xsl:choose>
            <xsl:when test="$jan1weekday &lt;= 4">number($jan1weekday) - 1</xsl:when>
            <xsl:otherwise>number($jan1weekday) - 8</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="corrected-day" select="$day-of-year + $offset"/>

    <xsl:variable name="week">
        <xsl:choose>
            <xsl:when test="$corrected-day &lt;= 0">
                <xsl:value-of select="date:week-in-year(concat($year - 1, '-12-31'))"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="ceiling($corrected-day div 7)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <func:result select="$week"/>
</func:function>
-->


</xsl:stylesheet>
