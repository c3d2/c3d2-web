<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:atom="http://www.w3.org/2005/Atom"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8" indent="yes"/>

<xsl:template match="/">
  <page>
    <xsl:apply-templates/>
  </page>
</xsl:template>

<xsl:template match="/config">
  <portal>
    <!-- Extract from fetched XML files -->
    <xsl:apply-templates select="*[@id]"/>

    <!-- Extract from news -->
    <xsl:for-each select="document('../news.xml')//newsfile">
      <xsl:variable name="news" select="document(concat('../', .))"/>
      <xsl:if test="starts-with(date:difference(date:date-time(), concat($news/item/@date, '+02:00')), '-')">
        <xsl:apply-templates select="$news/item"
                             mode="news">
	  <xsl:with-param name="link"
			  select="concat(substring-before(substring-after(., 'content/'), '.xml'), '.html')"/>
        </xsl:apply-templates>
      </xsl:if>
    </xsl:for-each>
  </portal>
</xsl:template>

<xsl:template match="/config/gallery">
  <xsl:apply-templates select="document(concat('../portal/portal-', @id, '.xml'))/atom:feed/atom:entry" mode="gallery"/>
</xsl:template>

<xsl:template match="/config/wiki">
  <xsl:apply-templates select="document(concat('../portal/portal-', @id, '.xml'))/api/query/recentchanges" mode="wiki"/>
</xsl:template>

<xsl:template match="*">
</xsl:template>

<xsl:template match="atom:entry" mode="gallery">
  <item date="{atom:updated[1]}">
    <xsl:apply-templates select="atom:title[1]" mode="gallery"/>
    <xsl:apply-templates select="atom:content[@type='html'][1]" mode="gallery"/>
    <xsl:apply-templates select="atom:link[@rel='alternate'][1]" mode="gallery"/>
    <xsl:apply-templates select="atom:link[@rel='enclosure'][1]" mode="gallery"/>
  </item>
</xsl:template>
<xsl:template match="atom:title" mode="gallery">
  <header><xsl:value-of select="."/></header>
</xsl:template>
<xsl:template match="atom:content[@type='html']" mode="gallery">
  <footer><xsl:value-of select="." disable-output-escaping="yes"/></footer>
</xsl:template>
<xsl:template match="atom:link[@rel='enclosure' and @href]" mode="gallery">
  <xsl:variable name="href" select="substring-after(@href, '/mgoblin_media/')"/>

  <content-image>
    <xsl:text>/mgoblin_media/</xsl:text>

    <xsl:choose>
      <xsl:when test="contains($href, '.jpg')">
        <xsl:value-of select="concat(substring-before($href, '.jpg'), '.medium.jpg')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$href"/>
      </xsl:otherwise>
    </xsl:choose>
  </content-image>
</xsl:template>
<xsl:template match="atom:link[@rel='alternate' and @href]" mode="gallery">
  <link><xsl:value-of select="@href"/></link>
</xsl:template>

<xsl:template match="rc" mode="wiki">
  <xsl:variable name="title" select="@title"/>
  <!-- Don't repeat any titles -->
  <xsl:if test="not(preceding-sibling::rc/@title = $title or
		    starts-with($title, 'Intern:') or
                    @type = 'log'
                   )">
    <item date="{@timestamp}" class="wiki">
      <header>C3D2 Wiki</header>
      <footer>
	<xsl:text>by </xsl:text>
	<xsl:value-of select="@user"/>
	<xsl:for-each select="following-sibling::rc[@title = $title]">
	  <xsl:variable name="user" select="@user"/>
	  <xsl:if test="not(preceding-sibling::rc[@title = $title and @user = $user])">
	    <xsl:text>, </xsl:text>
	    <xsl:value-of select="normalize-space(@user)"/>
	  </xsl:if>
	</xsl:for-each>
      </footer>
      <content-text><xsl:value-of select="@title"/></content-text>
      <link>https://wiki.c3d2.de/<xsl:value-of select="@title"/></link>
    </item>
  </xsl:if>
</xsl:template>

<xsl:template match="item" mode="news">
  <xsl:param name="link"/>

  <item date="{@date}" class="news">
    <header>News</header>
    <footer>by <xsl:value-of select="@author"/></footer>
    <content-text><xsl:value-of select="@title"/></content-text>
    <xsl:apply-templates select=".//image[1]" mode="news"/>
    <link><xsl:value-of select="$link"/></link>
  </item>
</xsl:template>

<xsl:template match="item/image[1]" mode="news">
  <content-image>
    <xsl:text>images/news/</xsl:text>
    <xsl:value-of select="."/>
  </content-image>
</xsl:template>
<xsl:template match="image[contains(., '//')]" mode="news">
  <content-image>
    <xsl:value-of select="."/>
  </content-image>
</xsl:template>
<xsl:template match="image" mode="news">
  <content-image>
    <xsl:value-of select="."/>
  </content-image>
</xsl:template>

</xsl:stylesheet>
