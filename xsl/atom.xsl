<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns="http://www.w3.org/2005/Atom"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:activity="http://activitystrea.ms/spec/1.0/"
  xmlns:poco="http://portablecontacts.net/spec/1.0"
  xmlns:media="http://purl.org/syndication/atommedia"
  xmlns:ostatus="http://ostatus.org/schema/1.0"
  xmlns:mastodon="http://mastodon.social/schema/1.0"
  exclude-result-prefixes="xsl date">

  <xsl:include href="common.xsl" />

  <xsl:output method="xml"
              version="1.0"
              encoding="utf-8"
              indent="yes"/>

  <xsl:variable name="baseurl">http://www.c3d2.de/</xsl:variable>
  <xsl:param name="archivefeed"/>

  <xsl:template match="news">
    <feed xml:lang="de">
      <!-- Datum des neuesten item für /feed/updated holen -->
      <xsl:for-each select="newsfile">
        <xsl:sort select="date:seconds(document(.)/item/@date)" data-type="number" order="descending" />
        <xsl:if test="position() = 1">
          <updated><xsl:value-of select="document(.)/item/@date"/>Z</updated>
        </xsl:if>
      </xsl:for-each>

      <!-- Allgemeines -->
      <title>www.c3d2.de Newsfeed</title>
      <link rel="self" href="http://www.c3d2.de/news-atom.xml" />
      <link rel="alternate" type="application/xhtml+xml" href="http://www.c3d2.de/" />
      <xsl:if test="$archivefeed">
	<link rel="prev-archive" href="{$archivefeed}"/>
      </xsl:if>
      <id>http://www.c3d2.de/</id>
      <subtitle>Neues aus dem Chaos Computer Club Dresden</subtitle>
      <logo><xsl:value-of select="$baseurl"/>images/ck.png</logo>
      <author>
        <id>https://www.c3d2.de/news.html</id>
        <uri>https://www.c3d2.de/news.html</uri>
        <name>C3D2 News</name>
        <email>mail@c3d2.de</email>
        <summary>Chaos Computer Club Dresden News</summary>
        <link rel="alternate" type="text/html"
              href="https://www.c3d2.de/news.html"/>
        <link rel="avatar" type="image/png"
              href="{$baseurl}images/ck.png"/>
        <activity:object-type>http://activitystrea.ms/schema/1.0/team</activity:object-type>
        <poco:preferredUsername>c3d2</poco:preferredUsername>
        <poco:displayName>C3D2</poco:displayName>
        <mastodon:scope>public</mastodon:scope>
      </author>

      <!-- Alle item zu atom-entry umformen -->
      <xsl:for-each select="newsfile">
        <xsl:sort select="date:seconds(document(.)/item/@date)" data-type="number" order="descending" />
        <xsl:variable name="newsfile" select="."/>
        <xsl:if test="(position() &lt;= $maxitems) and
                      starts-with(substring-after(substring-after($newsfile, '/'), '/'), $prefix) and
                      starts-with(date:difference(date:date-time(), concat(document(.)/item/@date, '+02:00')), '-')">
          <xsl:for-each select="document($newsfile)/item">
            <xsl:call-template name="atom-entry">
              <xsl:with-param name="newsfile" select="$newsfile"/>
            </xsl:call-template>
          </xsl:for-each>
          
        </xsl:if>
      </xsl:for-each>
    </feed>
  </xsl:template>

  <xsl:template name="atom-entry">
    <xsl:param name="newsfile"/>
    <entry>
      <xsl:variable name="href">
        <xsl:value-of select="$baseurl"/>
        <xsl:value-of select="substring-before(substring-after($newsfile, 'content/'), '.xml')"/>
        <xsl:text>.html</xsl:text>
      </xsl:variable>
      <id><xsl:value-of select="$href"/></id>
      <title><xsl:value-of select="@title"/></title>
      <link rel="alternate" type="application/xhtml+xml" href="{$href}"/>
      <updated><xsl:value-of select="@date"/>Z</updated>
      <mastodon:scope>public</mastodon:scope>
      <activity:verb>http://activitystrea.ms/schema/1.0/post</activity:verb>
      <activity:object-type><xsl:choose>
          <xsl:when test="starts-with($newsfile, 'content/news/ta-')">http://activitystrea.ms/schema/1.0/event</xsl:when>
          <xsl:when test="starts-with($newsfile, 'content/news/event-')">http://activitystrea.ms/schema/1.0/event</xsl:when>
          <xsl:otherwise>http://activitystrea.ms/schema/1.0/article</xsl:otherwise>
        </xsl:choose></activity:object-type>
      <author>
        <name><xsl:value-of select="@author"/></name>
      </author>
      <category>
        <xsl:attribute name="term"><xsl:choose>
          <xsl:when test="starts-with($newsfile, 'content/news/ta-')">Themenabend</xsl:when>
          <xsl:when test="starts-with($newsfile, 'content/news/event-')">Event</xsl:when>
          <xsl:otherwise>Misc</xsl:otherwise>
        </xsl:choose></xsl:attribute>
      </category>
      <content type="xhtml">
        <div xmlns="http://www.w3.org/1999/xhtml">
	  <xsl:apply-templates select="document($newsfile)/item/*"/>
        </div>
      </content>

      <xsl:for-each select=".//*[name() = 'resource' or name() = 'alternative']">
        <xsl:variable name="resource_href">
          <xsl:call-template name="make-href">
            <xsl:with-param name="href" select="@url"/>
          </xsl:call-template>
        </xsl:variable>

        <link rel="enclosure"
              href="{$resource_href}"
              type="{@type}"
              length="{@size}" />

	<xsl:copy-of select="sc:chapters"
		     xmlns:sc="http://podlove.de/simple-chapters"/>
      </xsl:for-each>

    </entry>
  </xsl:template>

  <xsl:template match="item/image" xmlns="http://www.w3.org/1999/xhtml">
    <img>
      <xsl:choose>
        <xsl:when test="contains(., '://')">
          <xsl:attribute name="src"><xsl:value-of select="."/></xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="src"><xsl:value-of select="concat($baseurl, 'images/news/', .)"/></xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>

      <xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute>
      <xsl:attribute name="alt"><xsl:value-of select="title"/></xsl:attribute>
    </img>
  </xsl:template>
</xsl:stylesheet>
