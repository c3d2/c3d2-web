<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  exclude-result-prefixes="xsl date">


<xsl:import href="date.week-in-year.function.xsl" />
<xsl:include href="ddate.xsl" />

<xsl:template match="/page/calendar">
  <xsl:apply-templates select="document(@src)/page/calendar-summary"/>
</xsl:template>

<xsl:template match="/page/calendar-summary">
  <table class="calendar">
    <tr>
      <th>Montag</th>
      <th>Dienstag</th>
      <th>Mittwoch</th>
      <th>Donnerstag</th>
      <th>Freitag</th>
      <th>Sonnabend</th>
      <th>Sonntag</th>
    </tr>

    <xsl:call-template name="output-week">
      <xsl:with-param name="date">
        <xsl:call-template name="get-past-monday">
          <xsl:with-param name="date" select="concat(date:year(date:date()), '-', format-number(date:month-in-year(date:date()), '00'), '-01')"/>
        </xsl:call-template>
      </xsl:with-param>
      <xsl:with-param name="stopmonth" select="date:month-in-year(date:add(date:date(), 'P2M'))"/>
    </xsl:call-template>

  </table>
</xsl:template>

<xsl:template name="output-week">
  <xsl:param name="date"/>
  <xsl:param name="stopmonth"/>
  <tr>
    <xsl:call-template name="output-day">
      <xsl:with-param name="date" select="$date"/>
    </xsl:call-template>
  </tr>

  <xsl:variable name="nextweek" select="date:add($date, 'P7D')"/>
  <xsl:if test="date:month-in-year($nextweek) != $stopmonth">
    <xsl:call-template name="output-week">
      <xsl:with-param name="date" select="$nextweek"/>
      <xsl:with-param name="stopmonth" select="$stopmonth"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<!--
  td/@class ist wie folgend aufgebaut:
  * Beginnt immer mit 'cal'
  * Dann 'week' für Wochentage, 'sun' für Sonntage
  * '1' für aktuellen Monat, '2' für folgenden Monat, '3' für sonstige Monate
-->
<xsl:template name="output-day">
  <xsl:param name="date"/>
  <xsl:variable name="today"><xsl:value-of select="substring(date:date(),1,10)"/></xsl:variable>
  <td>
    <xsl:attribute name="class"><xsl:if test="$today = $date">today </xsl:if>
      <xsl:text>cal</xsl:text>
      <xsl:choose>
        <xsl:when test="date:day-in-week($date) = 1">sun</xsl:when>
        <xsl:otherwise>week</xsl:otherwise>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="date:month-in-year($date) = date:month-in-year(date:date())">1</xsl:when>
        <xsl:when test="date:month-in-year($date) = date:month-in-year(date:add(date:date(), 'P1M'))">2</xsl:when>
        <xsl:otherwise>3</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <!-- vv01f: added id to jump to a specific date with links and hashtag like {$month}-{$day} //-->
    <xsl:attribute name="id">
        <xsl:value-of select="concat('eventday-', date:month-in-year($date), '-', date:day-in-month($date))"/>
    </xsl:attribute>

    <xsl:variable name="ddate">
      <xsl:call-template name="ddate">
        <xsl:with-param name="date" select="$date"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:if test="$today = $date"><a name="today"/></xsl:if><!-- vv01f: ancor so that #today is the link to current day -->

    <!-- clou'd calendar week for XSLT 1.0 from http://exslt.org/date/functions/week-in-year/index.html (by "Jeni Tennison" <mail@jenitennison.com>) -->
    <xsl:if test="date:day-in-week($date) = 2">
        <xsl:variable name="calweek">
            <xsl:value-of select="date:week-in-year($date)"/>
            <!-- XSLT 2.0 <xsl:value-of select="date:format-date(date,'[W]')"/-->
        </xsl:variable>
        <span class="calweek" id="calweek-{$calweek}" title="{$calweek}. Kalenderwoche">
            <xsl:value-of select="$calweek"/>
        </span>
    </xsl:if>

    <span class="day" title="{$ddate}"
      id="calendar-{date:year($date)}-{date:month-in-year($date)}-{date:day-in-month($date)}">
      <xsl:value-of select="date:day-in-month($date)"/>
    </span>

    <ul>
      <xsl:variable name="dholiday">
        <xsl:call-template name="ddate-holiday">
          <xsl:with-param name="date" select="$date"/>
        </xsl:call-template>
      </xsl:variable>
      <li><xsl:value-of select="normalize-space($dholiday)"/></li>

      <xsl:for-each select="event">
        <xsl:sort select="start"/>

        <xsl:variable name="start">
          <xsl:call-template name="strip-time-from-date">
            <xsl:with-param name="date" select="start"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="end">
          <xsl:call-template name="strip-time-from-date">
            <xsl:with-param name="date" select="end"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="date">
          <xsl:call-template name="strip-time-from-date">
            <xsl:with-param name="date" select="$date"/>
          </xsl:call-template>
        </xsl:variable>

        <xsl:if test="(date:seconds($start) &lt;= date:seconds($date))
                      and (date:seconds($date) &lt;= date:seconds($end))">
          <li>
            <span style="color: black;">
              <xsl:if test="string-length(location) = 0">
                <xsl:attribute name="class">ico ico-unknown</xsl:attribute>
                <xsl:attribute name="title">Unbekannt</xsl:attribute>
              </xsl:if>
              <xsl:if test="string-length(location) &gt; 0">
                <xsl:choose>
                  <xsl:when test="(contains(location, 'Jitsi') or contains(location, '/remote') or contains(location, 'BBB'))">
                    <xsl:attribute name="class">ico ico-remote</xsl:attribute>
                    <xsl:attribute name="title">Remote</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="contains(location, 'HQ') and (contains(location, 'Starkduftkabine') or contains(location, '/smoke') or contains(location, 'backstage'))">
                    <xsl:attribute name="class">ico ico-smoking</xsl:attribute>
                    <xsl:attribute name="title">HQ/SDK</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="contains(location, 'HQ') and (contains(location, '/null') or contains(location, 'Keller') or contains(location, 'Kerker'))">
                    <xsl:attribute name="class">ico ico-cellar</xsl:attribute>
                    <xsl:attribute name="title">Keller</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="(contains(location, 'HQ') or contains(location, 'ZW') or contains(location, 'Zentralwerk')) and (contains(location, 'Garten') or contains(location, 'Terrasse') or contains(location, 'Grill'))">
                    <xsl:attribute name="class">ico ico-garden</xsl:attribute>
                    <xsl:attribute name="title">Garten</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="(contains(location, 'HQ') or contains(location, 'ZW') or contains(location, 'Zentralwerk')) and (contains(location, '/top') or contains(location, 'Dach') or contains(location, 'Antenne') or contains(location, '/antenna'))">
                    <xsl:attribute name="class">ico ico-antenna</xsl:attribute>
                    <xsl:attribute name="title">ZW/Turmdach/Antenne</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="(contains(location, 'ZW') or contains(location, 'Zentrakwerk')) and (contains(location, 'Feuerschale') or contains(location, 'campfire') or contains(location, 'Innenhof'))">
                    <xsl:attribute name="class">ico ico-campfire</xsl:attribute>
                    <xsl:attribute name="title">Innenhof/Feuerschale</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="contains(location, 'HQ') or contains(location, '/proc')">
                    <xsl:attribute name="class">ico ico-house</xsl:attribute>
                    <xsl:attribute name="title">HQ</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="(contains(location, 'Coloradio') or contains(location, 'ZW') or contains(location, 'Zentralwerk')) and contains(location, 'Seminarraum')">
                    <xsl:attribute name="class">ico ico-seminar</xsl:attribute>
                    <xsl:attribute name="title">Coloradio/Seminarraum</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="(contains(location, 'ZW') or contains(location, 'Zentrakwerk')) and (contains(location, 'Coloradio') or contains(location, 'Radio'))">
                    <xsl:attribute name="class">ico ico-radio</xsl:attribute>
                    <xsl:attribute name="title">Coloradio</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="contains(location, 'ZW') or contains(location, 'Zentralwerk')">
                    <xsl:attribute name="class">ico ico-factory</xsl:attribute>
                    <xsl:attribute name="title">Zentralwerk</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="contains(location, 'Dresden') or contains(location, 'Pieschen')">
                    <xsl:attribute name="class">ico ico-city</xsl:attribute>
                    <xsl:attribute name="title">Dresden</xsl:attribute>
                  </xsl:when>
                  <xsl:when test="contains(location, 'Sachsen') or contains(location, 'Saxony')">
                    <xsl:attribute name="title">Sachsen</xsl:attribute>
                    SN
                  </xsl:when>
                  <xsl:when test="contains(location, 'Brandenburg')">
                    <xsl:attribute name="title">Brandenburg</xsl:attribute>
                    BB
                  </xsl:when>
                  <xsl:when test="contains(location, 'Sachsen-Anhalt') or contains(location, 'Sachsen Anhalt') or contains(location, 'Sassen-Anholt') or contains(location, 'Saxony-Anhalt')">
                    <xsl:attribute name="title">Sachsen-Anhalt</xsl:attribute>
                    ST
                  </xsl:when>
                  <xsl:when test="contains(location, 'Berlin')">
                    <xsl:attribute name="title">Berlin</xsl:attribute>
                    BE
                  </xsl:when>
                  <xsl:when test="contains(location, 'Bremen')">
                    <xsl:attribute name="title">Bremen</xsl:attribute>
                    HB
                  </xsl:when>
                  <xsl:when test="contains(location, 'Baden-Württemberg') or contains(location, 'Baden Württemberg') or contains(location, 'Baden') or contains(location, 'Württemberg')">
                    <xsl:attribute name="title">Baden-Württemberg</xsl:attribute>
                    BW
                  </xsl:when>
                  <xsl:when test="contains(location, 'Bayern') or contains(location, 'Bavaria') or contains(location, 'Franken') or contains(location, 'Oberfranken') or contains(location, 'Mittelfranken') or contains(location, 'Unterfranken') or contains(location, 'Oberbayern') or contains(location, 'Niederbayern') or contains(location, 'Schwaben') or contains(location, 'Oberpfalz')">
                    <xsl:attribute name="title">Bayern</xsl:attribute>
                    BY
                  </xsl:when>
                  <xsl:when test="contains(location, 'Hamburg')">
                    <xsl:attribute name="title">Hamburg</xsl:attribute>
                    HH
                  </xsl:when>
                  <xsl:when test="contains(location, 'Hessen') or contains(location, 'Hessia')">
                    <xsl:attribute name="title">Hessen</xsl:attribute>
                    HE
                  </xsl:when>
                  <xsl:when test="contains(location, 'Mecklenburg-Vorpommern') or contains(location, 'Mäkelborg-Vörpommern') or contains(location, 'Mecklenburg') or contains(location, 'Vorpommern')">
                    <xsl:attribute name="title">Mecklenburg-Vorpommern</xsl:attribute>
                    MV
                  </xsl:when>
                  <xsl:when test="contains(location, 'Niedersachsen') or contains(location, 'Lower Saxony') or contains(location, 'Neddersassen') or contains(location, 'Läichsaksen')">
                    <xsl:attribute name="title">Niedersachsen</xsl:attribute>
                    NI
                  </xsl:when>
                  <xsl:when test="contains(location, 'Nordrhein-Westfalen') or contains(location, 'North Rhine-Westphalia') or contains(location, 'Noordryn-Westfaulen')">
                    <xsl:attribute name="title">Nordrhein-Westfalen</xsl:attribute>
                    NW
                  </xsl:when>
                  <xsl:when test="contains(location, 'Rheinland-Pfalz') or contains(location, 'Rhineland-Palatinate') or contains(location, 'Rhoilond-Palz')">
                    <xsl:attribute name="title">Rheinland-Pfalz</xsl:attribute>
                    RP
                  </xsl:when>
                  <xsl:when test="contains(location, 'Saarland') or contains(location, 'Sarre')">
                    <xsl:attribute name="title">Saarland</xsl:attribute>
                    SL
                  </xsl:when>
                  <xsl:when test="contains(location, 'Schleswig-Holstein') or contains(location, 'Schleswig') or contains(location, 'Holstein') or contains(location, 'Slesvig-Holsten') or contains(location, 'Sleswig-Holsteen') or contains(location, 'Slaswik-Holstiinj')">
                    <xsl:attribute name="title">Schleswig-Holstein</xsl:attribute>
                    SH
                  </xsl:when>
                  <xsl:when test="contains(location, 'Thüringen') or contains(location, 'Thuringia')">
                    <xsl:attribute name="title">Thüringen</xsl:attribute>
                    TH
                  </xsl:when>
                  <xsl:when test="contains(location, 'Deutschland') or contains(location, 'Germany') or contains(location, 'BRD') or contains(location, 'Germania')">
                    <xsl:attribute name="title">Deutschland</xsl:attribute>
                    DEU
                  </xsl:when>
                  <xsl:when test="contains(location, 'Tschechische Republik') or contains(location, 'Czechia') or contains(location, 'Tschechien') or contains(location, 'Böhmen') or contains(location, 'Bohemia') or contains(location, 'Mähren') or contains(location, 'Moravia')">
                    <xsl:attribute name="title">Tschechische Republik</xsl:attribute>
                    CZE
                  </xsl:when>
                  <xsl:when test="contains(location, 'Polen') or contains(location, 'Polska')">
                    <xsl:attribute name="title">Polen</xsl:attribute>
                    POL
                  </xsl:when>
                  <xsl:when test="contains(location, 'Österreich') or contains(location, 'Austria')">
                    <xsl:attribute name="title">Österreich</xsl:attribute>
                    AUT
                  </xsl:when>
                  <xsl:when test="contains(location, 'Schweiz') or contains(location, 'Svizzera') or contains(location, 'svizra') or contains(location, 'Helvetica')">
                    <xsl:attribute name="title">Schweiz</xsl:attribute>
                    CHE
                  </xsl:when>
                  <xsl:when test="contains(location, 'Dänemark') or contains(location, 'Danmark') or contains(location, 'Denmark')">
                    <xsl:attribute name="title">Dänemark</xsl:attribute>
                    DNK
                  </xsl:when>
                  <xsl:when test="contains(location, 'Belgien') or contains(location, 'België') or contains(location, 'Belgique') or contains(location, 'Belgium')">
                    <xsl:attribute name="title">Belgien</xsl:attribute>
                    BEL
                  </xsl:when>
                  <xsl:when test="contains(location, 'Niederlande') or contains(location, 'Nederland') or contains(location, 'Nederlân')">
                    <xsl:attribute name="title">Niederlande</xsl:attribute>
                    NLD
                  </xsl:when>
                  <xsl:when test="contains(location, 'Luxemburg') or contains(location, 'Lëtzebuerg') or contains(location, 'Luxembourg')">
                    <xsl:attribute name="title">Luxemburg</xsl:attribute>
                    LUX
                  </xsl:when>
                  <xsl:when test="contains(location, 'Frankreich') or contains(location, 'France')">
                    <xsl:attribute name="title">Frankreich</xsl:attribute>
                    FRA
                  </xsl:when>
                  <xsl:when test="contains(location, 'USA')">
                    <xsl:attribute name="title">Vereinigte Staaten von Amerika</xsl:attribute>
                    USA
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:attribute name="class">ico ico-world</xsl:attribute>
                    <xsl:attribute name="title">Erde, Sonne, Milchstraße</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
            </span>
            <a>
              <!-- a/@href -->
          <xsl:if test="string-length(link) &gt; 0">
            <xsl:attribute name="href">
                <xsl:call-template name="make-href">
                    <xsl:with-param name="href" select="link"/>
                </xsl:call-template>
            </xsl:attribute>
          </xsl:if>

<!--    vv01f: add time for easier display of start time
            <xsl:if test="data-before"/>
-->
            <xsl:attribute name="data-before">
                <xsl:value-of select="substring(date:time(start),1,5)"/>
            </xsl:attribute>
<!--
            <xsl:if test="data-after"/>
            <xsl:attribute name="data-after">
                <xsl:value-of select=""/>
            </xsl:attribute>
-->

              <!-- a/@title -->
                <xsl:attribute name="title">
                    <xsl:if test="string-length(location) &gt; 0">
                        <xsl:value-of select="concat('Ort: ', location,'&#xA;')"/>
                    </xsl:if>
                    <!-- sometimes things dont have a place to happen
                    <xsl:if test="string-length(location) = 0">
                        <xsl:value-of select="concat('Ort: ', 'GCHQ, Riesaer Straße 32, 01127 Dresden')"/>
                    </xsl:if>
                    -->
                    <xsl:if test="string-length(date:time(start)) &gt; 0 or string-length(date:time(end)) &gt; 0">
                        <xsl:if test="date:day-in-month($date) = date:day-in-month(start) or date:day-in-month($date) = date:day-in-month(end)">
                            <xsl:value-of select="concat('', 'Zeit:')" disable-output-escaping="yes"/>
                            <xsl:if test="date:day-in-month($date) = date:day-in-month(start) and string-length(date:time(start)) &gt; 0 and substring(date:time(start),1,5) != ''">
                                <xsl:value-of select="concat(' ab ', substring(date:time(start),1,5))"/>
                            </xsl:if>
                            <xsl:if test="date:day-in-month($date) = date:day-in-month(end) and string-length(date:time(end)) &gt; 0 and substring(date:time(end),1,5) != '' and substring(date:time(end),1,5) !=  substring(date:time(start),1,5)">
                                <xsl:value-of select="concat('&#160;bis&#160;', substring(date:time(end),1,5))"/>
                            </xsl:if>
                        </xsl:if>
                    </xsl:if>
                </xsl:attribute>
              <!-- string(a) -->
              <xsl:value-of select="title"/>
            </a>
          </li>
        </xsl:if>
      </xsl:for-each>

    </ul>
  </td>
  <xsl:if test="date:day-in-week($date) != 1">  <!-- Sunday? Break and return to output-week... -->
    <xsl:call-template name="output-day">
      <xsl:with-param name="date" select="date:add($date, 'P1D')"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<xsl:template name="calendar-title">
  <xsl:variable name="d1" select="date:date()"/>
  <xsl:variable name="d2" select="date:add(date:date(), 'P1M')"/>

  <xsl:text>Kalender: </xsl:text>

  <xsl:call-template name="get-monthstring">
    <xsl:with-param name="date" select="$d1"/>
  </xsl:call-template>

  <xsl:if test="date:year($d1) != date:year($d2)">
    <xsl:text> </xsl:text>
    <xsl:value-of select="date:year($d1)"/>
  </xsl:if>

  <xsl:text> &amp; </xsl:text>

  <xsl:call-template name="get-monthstring">
    <xsl:with-param name="date" select="$d2"/>
  </xsl:call-template>

  <xsl:text> </xsl:text>

  <xsl:value-of select="date:year($d2)"/>
</xsl:template>

<xsl:template name="get-past-monday">
  <xsl:param name="date"/>
  <xsl:choose>
    <xsl:when test="date:day-in-week($date) = 2">
      <xsl:value-of select="$date"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="get-past-monday">
        <xsl:with-param name="date" select="date:add($date, '-P1D')"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
