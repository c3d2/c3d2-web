<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:rss="http://purl.org/rss/1.0/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:str="http://exslt.org/strings"
  exclude-result-prefixes="xsl rss rdf date str">

  <xsl:template match="pre">
    <pre>
      <xsl:call-template name="auto-id"/>
      <xsl:apply-templates/>
    </pre>
  </xsl:template>

	<xsl:template match="br">
		<br/>
		<xsl:apply-templates/>
	</xsl:template>

  <xsl:template match="p">
    <xsl:if test="@title">
      <h3>
        <xsl:call-template name="auto-id"/>
        <xsl:value-of select="@title"/>
      </h3>
    </xsl:if>

    <p>
      <xsl:if test="@itemprop">
        <xsl:attribute name="itemprop"><xsl:value-of select="@itemprop"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemscope">
        <xsl:attribute name="itemscope"><xsl:value-of select="@itemscope"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemtype">
        <xsl:attribute name="itemtype"><xsl:value-of select="@itemtype"/></xsl:attribute>
      </xsl:if>

      <xsl:if test="@class">
        <xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@lang">
        <xsl:attribute name="lang"><xsl:value-of select="@lang"/></xsl:attribute>
      </xsl:if>

      <xsl:if test="not(@title)">
        <xsl:call-template name="auto-id"/>
      </xsl:if>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="div">
    <xsl:if test="@title">
      <h3>
        <xsl:call-template name="auto-id"/>
        <xsl:value-of select="@title"/>
      </h3>
    </xsl:if>

    <div>
      <xsl:if test="@itemprop">
        <xsl:attribute name="itemprop"><xsl:value-of select="@itemprop"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemscope">
        <xsl:attribute name="itemscope"><xsl:value-of select="@itemscope"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemtype">
        <xsl:attribute name="itemtype"><xsl:value-of select="@itemtype"/></xsl:attribute>
      </xsl:if>

      <xsl:if test="@class">
        <xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@lang">
        <xsl:attribute name="lang"><xsl:value-of select="@lang"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="not(@title)">
        <xsl:call-template name="auto-id"/>
      </xsl:if>

      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="span">
    <span>
      <xsl:if test="@itemprop">
        <xsl:attribute name="itemprop"><xsl:value-of select="@itemprop"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemscope">
        <xsl:attribute name="itemscope"><xsl:value-of select="@itemscope"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemtype">
        <xsl:attribute name="itemtype"><xsl:value-of select="@itemtype"/></xsl:attribute>
      </xsl:if>

      <xsl:if test="@class">
        <xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@lang">
        <xsl:attribute name="lang"><xsl:value-of select="@lang"/></xsl:attribute>
      </xsl:if>

      <xsl:if test="@title">
        <!-- todo: conditional in case  class is "value-title" -->
        <xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute>
      </xsl:if>

      <xsl:apply-templates/>
    </span>
  </xsl:template>

  <xsl:template match="time">
    <time>
      <xsl:if test="@itemprop">
        <xsl:attribute name="itemprop"><xsl:value-of select="@itemprop"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemscope">
        <xsl:attribute name="itemscope"><xsl:value-of select="@itemscope"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemtype">
        <xsl:attribute name="itemtype"><xsl:value-of select="@itemtype"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@datetime">
        <xsl:attribute name="datetime"><xsl:value-of select="@datetime"/></xsl:attribute>
      </xsl:if>

      <xsl:if test="@class">
        <xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@lang">
        <xsl:attribute name="lang"><xsl:value-of select="@lang"/></xsl:attribute>
      </xsl:if>

      <xsl:if test="@title">
        <!-- todo: conditional in case  class is "value-title" -->
        <xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute>
      </xsl:if>

      <xsl:apply-templates/>
    </time>
  </xsl:template>

  <xsl:template match="dl">
    <dl class="{@class}">
      <xsl:call-template name="auto-id"/>
      <xsl:apply-templates/>
    </dl>
  </xsl:template>

  <xsl:template match="dh">
    <dt class="main"><xsl:call-template name="auto-id"/><xsl:apply-templates/></dt>
  </xsl:template>

  <xsl:template match="dt">
    <dt><xsl:call-template name="auto-id"/><xsl:apply-templates/></dt>
  </xsl:template>

  <xsl:template match="dd">
    <dd><xsl:call-template name="auto-id"/><xsl:apply-templates/></dd>
  </xsl:template>

  <xsl:template match="ul">
    <ul>
      <xsl:call-template name="auto-id"/>
      <xsl:if test="@itemprop">
        <xsl:attribute name="itemprop"><xsl:value-of select="@itemprop"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemscope">
        <xsl:attribute name="itemscope"><xsl:value-of select="@itemscope"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@itemtype">
        <xsl:attribute name="itemtype"><xsl:value-of select="@itemtype"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@class">
        <xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@lang">
        <xsl:attribute name="lang"><xsl:value-of select="@lang"/></xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </ul>
  </xsl:template>

  <xsl:template match="ol">
    <ol><xsl:call-template name="auto-id"><xsl:with-param name="tagname" select="name()"/></xsl:call-template><xsl:apply-templates/></ol>
  </xsl:template>

  <xsl:template match="li">
    <li><xsl:apply-templates/></li>
  </xsl:template>

  <xsl:template match="blockquote">
    <blockquote><xsl:call-template name="auto-id"/><xsl:apply-templates/></blockquote>
  </xsl:template>

  <xsl:template match="source">
	<xsl:variable name="medium">
		<xsl:value-of select="local-name(..)"/><!-- parent::node() -->
	</xsl:variable>
	<xsl:variable name="type">
      <xsl:choose>
		<xsl:when test="substring(@type,string-length(@type)-3,1)='/'"><xsl:value-of select="substring(@type,string-length(@type)-2,string-length(@type))"/></xsl:when>
		<xsl:when test="substring(@type,string-length(@type)-4,1)='/'"><xsl:value-of select="substring(@type,string-length(@type)-3,string-length(@type))"/></xsl:when>
		<xsl:when test="substring(@src,string-length(@src)-3,1)='.'"><xsl:value-of select="substring(@src,string-length(@src)-2,string-length(@src))"/></xsl:when>
		<xsl:when test="substring(@src,string-length(@src)-4,1)='.'"><xsl:value-of select="substring(@src,string-length(@src)-3,string-length(@src))"/></xsl:when>
		<xsl:otherwise></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <source>
		<xsl:if test="@src"><xsl:attribute name="src"><xsl:value-of select="@src"/></xsl:attribute></xsl:if>
		<xsl:if test="@srcset"><xsl:attribute name="srcset"><xsl:value-of select="@srcset"/></xsl:attribute></xsl:if>
		<xsl:if test="@media"><xsl:attribute name="media"><xsl:value-of select="@media"/></xsl:attribute></xsl:if>
		<xsl:if test="@sizes"><xsl:attribute name="sizes"><xsl:value-of select="@sizes"/></xsl:attribute></xsl:if>
		<xsl:if test="$type!=''">
			<xsl:attribute name="type">
				<xsl:call-template name="mime-by-extension">
					<xsl:with-param name="extension"><xsl:value-of select="$type"/></xsl:with-param>
					<xsl:with-param name="medium"><xsl:value-of select="$medium"/></xsl:with-param>
				</xsl:call-template>
			</xsl:attribute>
		</xsl:if>
		<xsl:value-of select="$medium"/>
    </source>
  </xsl:template>

  <xsl:template match="label">
    <label>
		<xsl:if test="@for"><xsl:attribute name="for"><xsl:value-of select="@for"/></xsl:attribute></xsl:if>
		<xsl:if test="@id"><xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute></xsl:if>
		<xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute></xsl:if>
		<xsl:if test="@style"><xsl:attribute name="style"><xsl:value-of select="@style"/></xsl:attribute></xsl:if>
		<xsl:if test="@lang"><xsl:attribute name="lang"><xsl:value-of select="@lang"/></xsl:attribute></xsl:if>
		<xsl:apply-templates/>
    </label>
  </xsl:template>

  <xsl:template match="input">
    <input>
		<xsl:if test="@type"><xsl:attribute name="type"><xsl:value-of select="@type"/></xsl:attribute></xsl:if>
		<xsl:if test="@name"><xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute></xsl:if>
		<xsl:if test="@value"><xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute></xsl:if>
		<xsl:if test="@checked"><xsl:attribute name="checked"><xsl:value-of select="@checked"/></xsl:attribute></xsl:if>
		<xsl:if test="@selected"><xsl:attribute name="selected"><xsl:value-of select="@selected"/></xsl:attribute></xsl:if>
		<xsl:if test="@id"><xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute></xsl:if>
		<xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute></xsl:if>
		<xsl:if test="@style"><xsl:attribute name="style"><xsl:value-of select="@style"/></xsl:attribute></xsl:if>
		<xsl:apply-templates/>
    </input>
  </xsl:template>

  <xsl:template match="video">
	<xsl:variable name="license">
		<xsl:choose>
			<xsl:when test="@license">
				<xsl:value-of select="translate(@license, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
			</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="author">
		<xsl:choose>
			<xsl:when test="@author"><xsl:value-of select="@author"/></xsl:when>
			<xsl:otherwise><!-- anonymous/unknown --></xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="class"><xsl:if test="@class"><xsl:value-of select="@class"/></xsl:if></xsl:variable>
    <figure>
		<xsl:attribute name="class"><xsl:value-of select="$class"/></xsl:attribute>
	  <video>
		<xsl:choose>
		  <xsl:when test="@type='loop'">
			<xsl:attribute name="loop"><xsl:value-of select="@type"/></xsl:attribute>
			<xsl:attribute name="autoplay">autoplay</xsl:attribute>
			<xsl:attribute name="muted">muted</xsl:attribute>
		  </xsl:when>
		  <xsl:otherwise></xsl:otherwise>
      </xsl:choose>
      <xsl:if test="@id"><xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute></xsl:if>
      <xsl:apply-templates/>
      <p class="error">Dein Browser unterstützt keine HTML5-Videos.</p>
      </video>
      <xsl:variable name="caption">
		  <xsl:choose>
			  <xsl:when test="@caption"><xsl:value-of select="@caption"/></xsl:when>
			  <xsl:when test="@title"><xsl:value-of select="@title"/></xsl:when>
			  <xsl:otherwise></xsl:otherwise>
		  </xsl:choose>
      </xsl:variable>
      <figcaption>
		<xsl:if test="$caption!=''"><em><xsl:value-of select="$caption"/></em></xsl:if>
		<xsl:variable name="licenselink">
			<xsl:call-template name="licenselinks"><xsl:with-param name="license"><xsl:value-of select="$license"/></xsl:with-param></xsl:call-template>
		</xsl:variable>
		<xsl:variable name="licensehref">
			<xsl:call-template name="make-href"><xsl:with-param name="href">
				<xsl:choose>
					<xsl:when test="not(@licenselink) or @licenselink=''">
						<xsl:value-of select="$licenselink"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@licenselink"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:with-param></xsl:call-template>
		</xsl:variable>
		<xsl:if test="$license!=''">
			<xsl:value-of select="' '"/>
			<xsl:choose>
				<xsl:when test="$licensehref=''">
					<span><xsl:attribute name="rel">license</xsl:attribute><xsl:value-of select="$license"/></span>
				</xsl:when>
				<xsl:otherwise>
					<a>
						<xsl:attribute name="href"><xsl:value-of select="$licensehref"/></xsl:attribute>
						<xsl:attribute name="rel">license</xsl:attribute>
						<xsl:attribute name="class"><xsl:value-of select="concat('license-link ',@class)"/></xsl:attribute>
						<xsl:call-template name="licenselogos"><xsl:with-param name="license"><xsl:value-of select="$license"/></xsl:with-param></xsl:call-template>
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="$author!=''"><xsl:value-of select="' '"/><span><xsl:attribute name="rel">author</xsl:attribute><xsl:value-of select="$author"/></span></xsl:if>
      </figcaption>
    </figure>
  </xsl:template>

	<xsl:template name="licenselinks">
		<xsl:param name="license"/>

		<xsl:variable name="l">
			<xsl:call-template name="string-to-upper">
				<xsl:with-param name="string" select="$license"/>
				<xsl:with-param name="forspace">-</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$l='PDM'">https://creativecommons.org/share-your-work/public-domain/pdm/</xsl:when>
			<xsl:when test="$l='CC0'">https://creativecommons.org/publicdomain/zero/1.0/legalcode</xsl:when>
			<xsl:when test="$l='CC-BY'">https://creativecommons.org/licenses/by/4.0/legalcode</xsl:when>
			<xsl:when test="$l='CC-BY-SA'">https://creativecommons.org/licenses/by-sa/4.0/legalcode</xsl:when>
			<xsl:when test="$l='CC-BY-ND'">https://creativecommons.org/licenses/by-nd/4.0/legalcode</xsl:when>
			<xsl:when test="$l='CC-BY-NC'">https://creativecommons.org/licenses/by-nc/4.0/legalcode</xsl:when>
			<xsl:when test="$l='CC-BY-NC-SA'">https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode</xsl:when>
			<xsl:when test="$l='CC-BY-NC-ND'">https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode</xsl:when>
			<xsl:otherwise></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="licenselogos">
		<xsl:param name="license"/>

		<xsl:variable name="image">
			<xsl:choose>
				<xsl:when test="$license='CC0'">0</xsl:when>
				<xsl:when test="$license='PDM'">pd</xsl:when>
				<xsl:when test="substring($license,1,2)='CC'"><xsl:value-of select="substring($license,4,string-length($license)-3)"/></xsl:when>
				<xsl:otherwise><!-- no image/rude defined for this license --></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<img>
			<xsl:attribute name="src">./images/cc-<xsl:call-template name="string-to-lower">
					<xsl:with-param name="string" select="$image"/>
					<xsl:with-param name="forspace">-</xsl:with-param>
				</xsl:call-template>-88x31.png</xsl:attribute>
			<xsl:attribute name="title"><xsl:value-of select="$license"/></xsl:attribute>
			<xsl:attribute name="alt"><xsl:value-of select="$license"/></xsl:attribute>
			<xsl:attribute name="class"><xsl:value-of select="concat('line-height-size ',@class)"/></xsl:attribute>
			<xsl:attribute name="loading">lazy</xsl:attribute>
		</img>
	</xsl:template>

  <!-- Bilder in News-Einträgen haben Pfad immer relativ zu /images/news/ -->
  <xsl:template match="image">
    <xsl:variable name="path">
      <xsl:call-template name="normalize-path">
        <xsl:with-param name="path">
          <xsl:choose>
            <!-- use it like this: <image thumb="no">http://upload.wikimedia.org/commons/h/ha/picture.img</image> -->
            <xsl:when test="not(@thumb='no') and contains(string(.), '://') and contains(string(.), 'wikimedia.org')">
              <xsl:variable name="filename">
                <xsl:value-of select="string(str:tokenize(string(.),'/')[last()])"/>
              </xsl:variable>
              <xsl:variable name="hash">
                <xsl:value-of select="concat(str:tokenize(string(.),'/')[position()=last()-2],'/',str:tokenize(string(.),'/')[position()=last()-1],'/')"/>
              </xsl:variable>
              <xsl:variable name="validfilename">
                <xsl:choose>
                  <xsl:when test="contains($filename, '.svg') and string-length(substring-after($filename, '.svg'))=0">
                    <xsl:value-of select="concat($filename, '.png')"/>
                  </xsl:when>
          <xsl:when test="contains(., '://')">
            <xsl:value-of select="."/>
          </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="$filename"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:value-of select="concat(substring-before(string(.),'commons/'),'commons/thumb/',$hash,$filename,'/600px-',$validfilename)"/>
            </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat($baseurl, .)"/>
        </xsl:otherwise>
          </xsl:choose>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>

    <img
      src="{$path}"
      title="{@title}"
      alt="{@title}"
      class="{@class}"
      loading="lazy" />
  </xsl:template>

  <xsl:template match="item/image[1]">
    <xsl:variable name="path">
      <xsl:call-template name="normalize-path">
    <xsl:with-param name="path">
      <xsl:value-of select="concat($baseurl, 'images/news/', .)"/>
    </xsl:with-param>
      </xsl:call-template>
    </xsl:variable>

    <img
      src="{$path}"
      title="{@title}"
      alt="{@title}"
      class="{@class}"
      loading="lazy"/>
  </xsl:template>

  <xsl:template match="link">
    <xsl:variable name="href">
      <xsl:call-template name="make-href">
        <xsl:with-param name="href" select="@href"/>
      </xsl:call-template>
    </xsl:variable>

    <a href="{$href}">
      <xsl:if test="@class">
        <xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@rel">
        <xsl:attribute name="rel"><xsl:value-of select="@rel"/></xsl:attribute>
      </xsl:if>

      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template name="make-href">
    <xsl:param name="href"/>

    <xsl:choose>
      <xsl:when test="$href and not(contains($href, ':'))">
        <xsl:value-of select="concat($baseurl,$href)"/>
      </xsl:when>
      <xsl:when test="$href">
        <xsl:value-of select="$href"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="code">
    <code>
      <xsl:apply-templates/>
    </code>
  </xsl:template>
  <xsl:template match="em">
    <em>
      <xsl:apply-templates/>
    </em>
  </xsl:template>
  <xsl:template match="strong">
    <strong>
      <xsl:apply-templates/>
    </strong>
  </xsl:template>
  <xsl:template match="sup">
    <sup>
      <xsl:apply-templates/>
    </sup>
  </xsl:template>
  <xsl:template match="figure">
    <figure>
      <xsl:apply-templates/>
    </figure>
  </xsl:template>
  <xsl:template match="figcaption">
    <figcaption>
		<xsl:attribute name="class">caption</xsl:attribute>
		<xsl:apply-templates/>
    </figcaption>
  </xsl:template>

  <xsl:template match="xhtml:*"
                xmlns:xhtml="http://www.w3.org/1999/xhtml">
    <xsl:element name="{local-name()}">
      <xsl:for-each select="@*">
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>

      <xsl:apply-templates />
    </xsl:element>
  </xsl:template>
  <xsl:template match="svg:*"
                xmlns:svg="http://www.w3.org/2000/svg">
    <xsl:element name="{local-name()}">
      <xsl:for-each select="@*">
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>

      <xsl:apply-templates />
    </xsl:element>
  </xsl:template>

  <xsl:template mode="navitem" match="file">
    <xsl:variable name="file" select="."/>
    <li>
      <a href="{$baseurl}{$file}.html">
        <xsl:choose>
          <xsl:when test="string-length(@title) = 0">
            <xsl:if test="document(concat('../content/pages/', $file, '.xml'))/page/@pagetitle">
              <xsl:attribute name="title"><xsl:value-of select="document(concat('../content/pages/', $file, '.xml'))/page/@pagetitle"/></xsl:attribute>
            </xsl:if>

            <xsl:value-of select="document(concat('../content/pages/', $file, '.xml'))/page/@title"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@title"/>
          </xsl:otherwise>
        </xsl:choose>
      </a>
    </li>
  </xsl:template>

  <xsl:template mode="navitem" match="link">
    <li>
      <xsl:apply-templates select="."/>
    </li>
  </xsl:template>

  <xsl:template name="navitems">
    <xsl:for-each select="document('../content/navigation.xml')/navigation/group">
      <ul>
        <xsl:apply-templates mode="navitem"/>
      </ul>
    </xsl:for-each>
  </xsl:template>

  <!-- Chaosupdates in tile -->

  <xsl:template name="chaosupdates">
    <xsl:param name="limit" select="5"/>

    <h3>
      <a href="{document('../chaosupdates.xml')/rdf:RDF/rss:channel/rss:link}">
        <xsl:value-of select="document('../chaosupdates.xml')/rdf:RDF/rss:channel/rss:title"/>
      </a>
    </h3>
    <ul>
      <xsl:for-each select="document('../chaosupdates.xml')/rdf:RDF/rss:item[position() &lt;= $limit]">
        <li>
          <a href="{rss:link}">
            <xsl:value-of select="rss:title"/>
          </a>
        </li>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <!-- Calendar in tile -->

  <xsl:template name="calendar-events">
    <xsl:param name="limit" select="5"/>

    <ul>
      <xsl:for-each select="document('../calendar-summary.xml')/page/calendar-summary/event[date:seconds(end) &gt;= date:seconds(date:date())]">
        <xsl:sort select="date:seconds(start)" data-type="number" order="ascending" />

        <xsl:if test="position() &lt;= $limit">
          <xsl:variable name="href">
            <xsl:call-template name="make-href">
              <xsl:with-param name="href" select="link"/>
            </xsl:call-template>
          </xsl:variable>

          <li>
            <xsl:call-template name="datespan-to-human">
                <xsl:with-param name="start" select="start"/>
                <xsl:with-param name="end" select="end"/>
            </xsl:call-template>:
            <a>
                <xsl:if test="string-length($href) &gt; 0">
                    <xsl:attribute name="href">
                        <xsl:value-of select="$href"/>
                    </xsl:attribute>
                </xsl:if>
                <!-- vv01f:
                    added link to a specific date in calndar
                    #eventday-* is to be handeled with css
                -->
                <xsl:if test="string-length($href) = 0">
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('calendar.html#eventday-',concat(date:month-in-year(start), '-'), date:day-in-month(start))"/>
                    </xsl:attribute>
                </xsl:if>

                <xsl:if test="string-length(location) &gt; 0">
                    <xsl:attribute name="title">
                        <xsl:value-of select="concat('Ort: ', location)"/>
                        <xsl:if test="string-length(date:time(start)) &gt; 0 or string-length(date:time(end)) &gt; 0">
                            <xsl:value-of select="concat('&#xA;', 'Zeit:')" disable-output-escaping="yes"/>
                            <xsl:if test="string-length(date:time(start)) &gt; 0 and substring(date:time(start),1,5) != ''">
                                <xsl:value-of select="concat(' ab ', substring(date:time(start),1,5))"/>
                            </xsl:if>
                            <xsl:if test="string-length(date:time(end)) &gt; 0 and substring(date:time(end),1,5) != '' and substring(date:time(end),1,5) !=  substring(date:time(start),1,5)">
                                <xsl:value-of select="concat('&#160;bis&#160;', substring(date:time(end),1,5))"/>
                            </xsl:if>
                        </xsl:if>
                    </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="title"/>
            </a>
          </li>

        </xsl:if>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <!-- Support für <event/> bzw. Microformats -->
  <xsl:include href="date.xsl" />
  <xsl:template match="item/event">
    <dl>
      <xsl:apply-templates/>
    </dl>
  </xsl:template>

  <xsl:template match="item/event/start">
    <xsl:variable name="start" select="."/>
    <xsl:variable name="end" select="../end"/>

    <dt>Datum</dt>
    <dd>
      <abbr class="dtstart">
    <xsl:attribute name="title">
      <xsl:call-template name="date-to-hevent">
        <xsl:with-param name="date" select="$start"/>
      </xsl:call-template>
    </xsl:attribute>

    <xsl:call-template name="get-datestring">
      <xsl:with-param name="date" select="$start"/>
    </xsl:call-template>
      </abbr>

      <xsl:if test="$end">
    bis
    <abbr class="dtend">
      <xsl:attribute name="title">
        <xsl:call-template name="date-to-hevent">
          <xsl:with-param name="date" select="$end"/>
        </xsl:call-template>
      </xsl:attribute>

      <xsl:choose>
        <!-- Ends on same day it starts? -->
        <xsl:when test="date:date($start) = date:date($end)">
          <xsl:call-template name="get-timestring">
        <xsl:with-param name="date" select="$end"/>
          </xsl:call-template>
        </xsl:when>
        <!-- Event spans days -->
        <xsl:otherwise>
          <xsl:call-template name="get-datestring">
        <xsl:with-param name="date" select="$end"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </abbr>
      </xsl:if>
    </dd>
  </xsl:template>
  <xsl:template match="item/event/end">
    <!-- Output already for item/event/start above -->
  </xsl:template>
  <xsl:template match="item/event/location">
    <dt>Ort</dt>
    <dd class="location"><xsl:apply-templates/></dd>
  </xsl:template>

  <!-- Resources/alternatives in HTML -->

  <xsl:template match="resource">
    <xsl:choose>
      <xsl:when test="not(starts-with(@type, 'video/')) or count(resource[not(starts-with(@type, 'video/'))]) &gt; 0">
    <!-- Is not only video/* -->
    <ul>
      <xsl:call-template name="resource-item"/>
      <xsl:apply-templates mode="resource"/>
    </ul>
      </xsl:when>
      <xsl:otherwise>
    <!-- Video only -->
    <xsl:call-template name="video-resource"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="resource/alternative" mode="resource">
    <xsl:call-template name="resource-item">
      <xsl:with-param name="title">
    <xsl:choose>
      <xsl:when test="@title"><xsl:value-of select="@title"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="../@title"/></xsl:otherwise>
    </xsl:choose>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="resource-item">
    <xsl:param name="title" select="@title"/>
    <xsl:variable name="href">
      <xsl:call-template name="make-href">
        <xsl:with-param name="href" select="@url"/>
      </xsl:call-template>
    </xsl:variable>

    <!-- @class = "resource" | "alternative" -->
    <li class="{name(.)}">
      <!-- HTML5 dataset hints for play-resources.js poster -->
      <xsl:if test="@poster">
    <xsl:attribute name="data-poster">
      <xsl:value-of select="@poster"/>
    </xsl:attribute>
      </xsl:if>
      <xsl:if test="@preview">
    <xsl:attribute name="data-preview">
      <xsl:value-of select="@preview"/>
    </xsl:attribute>
      </xsl:if>

      <!-- MIME type hinting works even in the browser -->
      <a href="{$href}" type="{@type}" class="mime" rel="enclosure">
        <xsl:choose>
          <xsl:when test="string-length(string($title))"><xsl:value-of select="$title"/></xsl:when>
          <xsl:otherwise><xsl:value-of select="$href"/></xsl:otherwise>
        </xsl:choose>
      </a>

      <span class="resource-meta">
        (<xsl:call-template name="mime-for-human">
           <xsl:with-param name="mime" select="@type"/>
         </xsl:call-template>, <xsl:call-template name="format-filesize">
           <xsl:with-param name="value" select="@size"/>
         </xsl:call-template>)
      </span>

      <xsl:if test="$baseurl = '' or $baseurl = '../'">
    <!-- Include chapters only in XHTML for JS player. ATOM feeds
         copy chapters outside <content type="xhtml"/>
    -->
    <xsl:copy-of select="sc:chapters"
             xmlns:sc="http://podlove.de/simple-chapters"/>
      </xsl:if>
    </li>
  </xsl:template>

  <xsl:template name="video-resource">
    <section class="video-resource">
      <h4>
        <xsl:call-template name="auto-id"/>
    <xsl:choose>
      <xsl:when test="@details-link">
        <a href="{@details-link}">
          <xsl:value-of select="@title"/>
        </a>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="@title"/>
      </xsl:otherwise>
    </xsl:choose>
      </h4>
      <ul>
    <xsl:call-template name="video-resource-item"/>
    <xsl:apply-templates mode="video-resource"/>

    <xsl:if test="@feedback-link">
      <li class="feedback"><a href="{@feedback-link}">Feedback</a></li>
    </xsl:if>
      </ul>
    </section>
  </xsl:template>

  <xsl:template match="resource/alternative" mode="video-resource">
    <xsl:call-template name="video-resource-item">
      <xsl:with-param name="title">
    <xsl:choose>
      <xsl:when test="@title"><xsl:value-of select="@title"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="../@title"/></xsl:otherwise>
    </xsl:choose>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="video-resource-item">
    <xsl:variable name="href">
      <xsl:call-template name="make-href">
        <xsl:with-param name="href" select="@url"/>
      </xsl:call-template>
    </xsl:variable>

    <li class="{name(.)}">
      <!-- HTML5 dataset hints for play-resources.js poster -->
      <xsl:if test="@poster">
    <xsl:attribute name="data-poster">
      <xsl:call-template name="make-href">
        <xsl:with-param name="href" select="@poster"/>
      </xsl:call-template>
    </xsl:attribute>
      </xsl:if>
      <xsl:if test="@preview">
    <xsl:attribute name="data-preview">
      <xsl:call-template name="make-href">
        <xsl:with-param name="href" select="@preview"/>
      </xsl:call-template>
    </xsl:attribute>
      </xsl:if>

      <!-- MIME type hinting works even in the browser -->
      <a href="{$href}" type="{@type}" class="mime" rel="enclosure">
    <xsl:call-template name="mime-for-human-short">
      <xsl:with-param name="mime" select="@type"/>
    </xsl:call-template>
      </a>

      <span class="size">
    <xsl:call-template name="format-filesize">
      <xsl:with-param name="value" select="@size"/>
    </xsl:call-template>
      </span>

      <xsl:if test="$baseurl = '' or $baseurl = '../'">
    <!-- Include chapters only in XHTML for JS player. ATOM feeds
         copy chapters outside <content type="xhtml"/>
    -->
    <xsl:copy-of select="sc:chapters"
             xmlns:sc="http://podlove.de/simple-chapters"/>
      </xsl:if>
    </li>
  </xsl:template>

  <!-- Helpers -->

  <xsl:template name="auto-id">
    <xsl:param name="tagname"/>

    <xsl:choose>
      <xsl:when test="@id">
        <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
      </xsl:when>
      <xsl:when test="@title">
        <xsl:attribute name="id">
          <xsl:call-template name="string-to-lower">
            <xsl:with-param name="string" select="@title"/>
            <xsl:with-param name="forspace">-</xsl:with-param>
          </xsl:call-template>
        </xsl:attribute>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="$tagname='ol' and @start">
      <xsl:attribute name="start"><xsl:value-of select="@start"/></xsl:attribute>
    </xsl:if>
  </xsl:template>

  <xsl:template name="format-filesize">
    <xsl:param name="value"/>

    <xsl:choose>
      <xsl:when test="$value &gt; 1024 * 1024 * 1024">
        <xsl:value-of select="format-number($value div (1024 * 1024 * 1024), '0.0')"/>
        <xsl:text> GiB</xsl:text>
      </xsl:when>
      <xsl:when test="$value &gt; 1024 * 1024">
        <xsl:value-of select="format-number($value div (1024 * 1024), '0.0')"/>
        <xsl:text> MiB</xsl:text>
      </xsl:when>
      <xsl:when test="$value &gt; 1024">
        <xsl:value-of select="format-number($value div (1024), '0.0')"/>
        <xsl:text> KiB</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$value"/>
        <xsl:text> Byte</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

	<xsl:template name="mime-by-extension">
		<xsl:param name="extension"/>
		<xsl:param name="medium">media</xsl:param>
		
		<xsl:variable name="e">
			<xsl:call-template name="string-to-lower">
				<xsl:with-param name="string"><xsl:value-of select="$extension"/></xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="m">
			<xsl:call-template name="string-to-lower">
				<xsl:with-param name="string">
					<xsl:choose>
						<xsl:when test="$medium='picture'">image</xsl:when>
						<xsl:when test="$medium='video' or $medium='audio'"><xsl:value-of select="$medium"/></xsl:when>
					</xsl:choose>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$e='webm' or $e='mp4' or $e='ogg'"><xsl:value-of select="$m"/>/<xsl:value-of select="$e"/></xsl:when>
		</xsl:choose>
	</xsl:template>


  <xsl:template name="mime-for-human">
    <xsl:param name="mime"/>

    <xsl:choose>
      <xsl:when test="$mime = 'application/ogg'">Ogg Vorbis</xsl:when>
      <xsl:when test="$mime = 'audio/ogg'">Ogg Vorbis</xsl:when>
      <xsl:when test="$mime = 'audio/mpeg'">MPEG-Audio</xsl:when>
      <xsl:when test="$mime = 'audio/mp4'">MPEG4-Audio</xsl:when>
      <xsl:when test="$mime = 'application/octet-stream'">Binärdatei</xsl:when>
      <xsl:when test="$mime = 'application/xhtml+xml'">XHTML-Dokument</xsl:when>
      <xsl:when test="$mime = 'text/html'">HTML-Dokument</xsl:when>
      <xsl:when test="$mime = 'text/plain'">Text-Dokument</xsl:when>
      <xsl:when test="$mime = 'application/pdf'">Portable Document Format</xsl:when>
      <xsl:when test="$mime = 'image/svg+xml'">Scalable Vector Graphics</xsl:when>
      <xsl:when test="$mime = 'image/gif'">GIF-Bild</xsl:when>
      <xsl:when test="$mime = 'image/jpeg'">JPEG-Bild</xsl:when>
      <xsl:when test="$mime = 'image/png'">PNG-Bild</xsl:when>
      <xsl:when test="$mime = 'video/mpeg'">MPEG-Video</xsl:when>
      <xsl:when test="$mime = 'video/mp4'">MP4-Video</xsl:when>
      <xsl:when test="$mime = 'video/quicktime'">QuickTime-Video</xsl:when>
      <xsl:when test="$mime = 'video/x-ms-wmv'">Windows Media-Video</xsl:when>
      <xsl:when test="$mime = 'video/x-msvideo'">Video for Windows</xsl:when>
      <xsl:when test="$mime = 'video/avi'">Audio Video Interleave</xsl:when>
      <xsl:when test="$mime = 'video/ogg'">Ogg Media Video</xsl:when>
      <xsl:when test="$mime = 'video/x-matroska'">Matroska Video</xsl:when>
      <xsl:when test="$mime = 'video/x-m4v'">iTunes Video File</xsl:when>
      <xsl:when test="$mime = 'video/webm'">WebM Video</xsl:when>
      <xsl:when test="$mime = 'video/x-flv'">Flash Video</xsl:when>
      <xsl:when test="$mime = 'application/x-gtar'">Komprimierter Tarball</xsl:when>
      <xsl:when test="$mime = 'application/x-tar'">Tarball</xsl:when>
      <xsl:when test="$mime = 'application/x-bittorrent'">BitTorrent-Metainformationen</xsl:when>
      <xsl:when test="$mime = 'application/zip'">ZIP-Archiv</xsl:when>
      <xsl:when test="$mime = 'multipart/x-zip'">ZIP-Archiv</xsl:when>
      <xsl:when test="$mime = 'audio/opus'">Opus</xsl:when>
      <xsl:otherwise>Datei</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="mime-for-human-short">
    <xsl:param name="mime"/>

    <xsl:choose>
      <xsl:when test="$mime='audio/mpeg'">MP3</xsl:when>
      <xsl:when test="$mime='video/mp4'">MP4</xsl:when>
      <xsl:when test="$mime='audio/mp4'">M4A</xsl:when>
      <xsl:when test="$mime='video/webm'">WebM</xsl:when>
      <xsl:when test="$mime='video/ogg'">Ogg</xsl:when>
      <xsl:when test="$mime='video/avi'">AVI</xsl:when>
      <xsl:when test="$mime='video/x-flv'">FLV</xsl:when>
      <xsl:when test="$mime = 'audio/opus'">Opus</xsl:when>
      <xsl:when test="$mime='application/x-bittorrent'">BitTorrent</xsl:when>
      <xsl:otherwise>Datei</xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="normalize-path">
    <xsl:param name="path"/>

    <xsl:choose>
      <xsl:when test="contains($path, '/')">
    <xsl:variable name="dir1" select="substring-before($path, '/')"/>
    <xsl:variable name="dir2" select="substring-after($path, '/')"/>
    <xsl:choose>
      <xsl:when test="starts-with($dir2, '../')">
        <!-- recurse with xxx/../ removed -->
        <xsl:call-template name="normalize-path">
          <xsl:with-param name="path">
        <xsl:value-of select="substring-after($dir2, '../')"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <!-- spit out and recurse with tail -->
        <xsl:value-of select="$dir1"/><xsl:text>/</xsl:text><xsl:call-template name="normalize-path">
          <xsl:with-param name="path">
        <xsl:value-of select="$dir2"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
    <!-- just a filename with no / -->
    <xsl:value-of select="$path"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="string-to-lower">
    <xsl:param name="string"/>
    <xsl:param name="forspace"> </xsl:param>
    <xsl:value-of select="translate($string, ' ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞŸŽŠŒ', concat($forspace,'abcdefghijklmnopqrstuvwxyzàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿžšœ'))"/>
  </xsl:template>

  <xsl:template name="string-to-upper">
    <xsl:param name="string"/>
    <xsl:param name="forspace"> </xsl:param>
    <xsl:value-of select="translate($string,' abcdefghijklmnopqrstuvwxyzàáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿžšœ',concat($forspace, 'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞŸŽŠŒ'))"/>
  </xsl:template>

</xsl:stylesheet>
