<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:str="http://exslt.org/strings"
  xmlns:crypto="http://exslt.org/crypto"
  exclude-result-prefixes="xsl date str crypto">


<xsl:include href="common.xsl" />
<xsl:include href="header.xsl" />
<xsl:include href="footer.xsl" />
<xsl:include href="calendar.xsl" />
<xsl:include href="paypal.xsl" />

<xsl:output method="xml"
        version="1.0"
            encoding="utf-8"
            indent="yes"/>

  <xsl:template match="/page">
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;
</xsl:text>

    <xsl:variable name="title">
      <xsl:choose>
        <xsl:when test="/page/@pagetitle">
          <xsl:value-of select="/page/@pagetitle"/>
        </xsl:when>
        <xsl:when test="/page/@title">
          <xsl:value-of select="/page/@title"/>
        </xsl:when>
        <xsl:when test="/page/calendar-summary">
          <xsl:call-template name="calendar-title"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:comment><xsl:text> missing title </xsl:text></xsl:comment>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:call-template name="body">
      <xsl:with-param name="title" select="normalize-space($title)" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="/item">
    <xsl:variable name="title">
      <xsl:value-of select="/item/@title"/>
    </xsl:variable>

    <xsl:call-template name="body">
      <xsl:with-param name="title" select="$title" />
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="body">
    <xsl:param name="title" />

    <xsl:element name="html">
        <xsl:attribute name="lang">de</xsl:attribute>
        <xsl:attribute name="xml:lang">de</xsl:attribute>
        <xsl:call-template name="header">
            <xsl:with-param name="title" select="$title" />
        </xsl:call-template>

      <xsl:element name="body">
        <div id="page">
          <div id="logo">
              <a href="/"><h1>&lt;&lt;&lt;/&gt;&gt; Chaos Computer Club Dresden | c3d2</h1></a>

              <nav id="navigation">
        <div id="navgroups">
          <xsl:call-template name="navitems" />
        </div>
          </nav>
          </div>

      <section id="content">
        <xsl:if test="event">
          <xsl:attribute name="class">vevent</xsl:attribute>
        </xsl:if>
        <xsl:if test="/page/portal">
          <xsl:attribute name="class">portalcontainer</xsl:attribute>
        </xsl:if>

        <xsl:if test="$title != ''">
          <h2>
        <xsl:if test="name(.) = 'item'">
          <xsl:attribute name="class">news</xsl:attribute>
        </xsl:if>

        <xsl:value-of select="$title" />
          </h2>
        </xsl:if>

        <!-- Für Seiten, die nur ein Newsitem beinhalten,
         Newsmetadaten anzeigen
        -->
        <xsl:if test="name(.) = 'item'">
          <xsl:call-template name="news-meta"/>
        </xsl:if>

        <xsl:apply-templates/>

      </section> <!-- id="content" -->

      <xsl:call-template name="footer" />
        </div> <!-- id="page" -->
      </xsl:element> <!-- name="body" -->
    </xsl:element> <!-- name="html" -->
  </xsl:template>

  <xsl:template match="/page/news">

    <xsl:for-each select="newsfile">
      <xsl:sort select="date:seconds(document(concat('../', .))/item/@date)" data-type="number" order="descending" />
      <xsl:variable name="news" select="document(concat('../', .))"/>
      <xsl:if test="position() &lt;= 10 and
                    starts-with(
                      date:difference(
                        date:date-time(),
                        concat($news/item/@date, '+02:00')
                      ),
                      '-'
                    )">

        <xsl:variable name="newsfile"><xsl:value-of select="."/></xsl:variable>
        <xsl:variable name="itemposition" select="position()"/>

        <xsl:for-each select="$news/item">

            <article><div>
              <xsl:if test="event">
                <xsl:attribute name="class">vevent</xsl:attribute>
              </xsl:if>

          <header>
        <h3 class="news summary">
          <a href="{substring-before(substring-after($newsfile, 'content/'), '.xml')}.html" class="url">
            <xsl:value-of select="@title"/>
          </a>
        </h3>

        <xsl:call-template name="news-meta"/>
          </header>

          <div class="news">
        <xsl:apply-templates select="*[name() != 'addendum']">
          <xsl:with-param name="itemposition" select="$itemposition"/>
        </xsl:apply-templates>

        <xsl:if test="//addendum or //resource[@hide='yes']">
          <footer><p class="more">
            <a href="{substring-before(substring-after($newsfile, 'content/'), '.xml')}.html">
              Weiterlesen…
            </a>
          </p></footer>
        </xsl:if>
          </div>

          <hr class="news"/>

            </div></article>

        </xsl:for-each>
      </xsl:if>
    </xsl:for-each>

    <aside>
      <p>
    Die Neuigkeiten des C3D2 sind auch
    als <a href="{$baseurl}news-atom.xml">ATOM</a>
    und <a href="{$baseurl}news-rss.xml">RSS</a>, die
    Veranstaltungen
    im <a href="{$baseurl}ical.ics">iCal-</a>, <a href="{$baseurl}xcal.xml">XCal-</a>
    und <a href="{$baseurl}reminders">remind-Format</a> abrufbar.
    Mehr News gibts im <a href="{$baseurl}news-archiv.html" rel="archives">Archiv</a>.
      </p>
    </aside>
  </xsl:template>

  <xsl:template name="news-meta">
    <small class="news_author">
      <xsl:value-of select="@author"/>
    </small>

    <small class="news_date">
      @
      <time datetime="{@date}" pubdate="pubdate">
    <xsl:call-template name="get-datestring">
      <xsl:with-param name="date" select="@date" />
    </xsl:call-template>
      </time>
    </small>
  </xsl:template>

  <xsl:template match="item/image">
    <xsl:param name="itemposition"/>

    <img>
      <xsl:attribute name="src">
        <xsl:call-template name="normalize-path">
          <xsl:with-param name="path">
            <xsl:choose>
              <xsl:when test="contains(., '://')">
                <xsl:value-of select="."/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat($baseurl, 'images/news/', .)"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:attribute>

      <xsl:attribute name="title"><xsl:value-of select="@title"/></xsl:attribute>
      <xsl:attribute name="alt"><xsl:value-of select="@title"/></xsl:attribute>

      <xsl:if test="@height">
        <xsl:attribute name="height"><xsl:value-of select="@height"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@width">
        <xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
      </xsl:if>
      <xsl:if test="@style">
        <xsl:attribute name="style"><xsl:value-of select="@style"/></xsl:attribute>
      </xsl:if>
      <xsl:attribute name="loading">lazy</xsl:attribute>

      <!-- News-Items haben ihre Bilder abwechselnd rechts und links... -->
      <xsl:attribute name="class">
        <xsl:choose>
          <xsl:when test="$itemposition mod 2 = 1">
            <xsl:text>left</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>right</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
    </img>
  </xsl:template>

  <xsl:template match="news-list">
    <xsl:variable name="prefix" select="@prefix"/>
    <xsl:variable name="details" select="@details"/>


    <xsl:for-each select="document('../news.xml')/page/news/newsfile">
      <xsl:sort select="date:seconds(document(concat('../', .))/item/@date)" data-type="number" order="descending" />
      <xsl:variable name="newsfile" select="."/>
      <xsl:variable name="news" select="document(concat('../', $newsfile))"/>

      <xsl:for-each select="$news/item">
        <xsl:if test="((string-length($prefix) = 0) or
                       starts-with($newsfile, concat('content/news/', $prefix))) and
                      (($details != 'resources-only') or
                      (count(resource) &gt; 0)) and
                      starts-with(
                        date:difference(
                          date:date-time(),
                          concat($news/item/@date, '+02:00')
                        ),
                        '-'
                      )">

          <div>
            <xsl:if test="($details = 'event') and event">
              <xsl:attribute name="class">vevent</xsl:attribute>
            </xsl:if>

            <h3 class="summary">
              <a href="{substring-before(substring-after($newsfile, 'content/'), '.xml')}.html" class="url">
                <xsl:value-of select="@title"/>
              </a>
            </h3>

            <xsl:choose>
              <xsl:when test="$details = 'news'">
                <xsl:call-template name="news-meta"/>
              </xsl:when>
              <xsl:when test="$details = 'resources-only'">
        <xsl:apply-templates select=".//resource"/>
              </xsl:when>
              <xsl:when test="($details = 'event') and event">
                <ul class="details_event">
                  <li>
                    <abbr class="dtstart">
                      <xsl:attribute name="title">
                        <xsl:call-template name="date-to-hevent">
                          <xsl:with-param name="date" select="event/start"/>
                        </xsl:call-template>
                      </xsl:attribute>

                      <xsl:call-template name="get-datestring">
                        <xsl:with-param name="date" select="event/start"/>
                      </xsl:call-template>
                    </abbr>
                  </li>

                  <li class="location">
                    <xsl:value-of select="event/location"/>
                  </li>
                </ul>
              </xsl:when>
            </xsl:choose>

          </div>

        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="/page/portal">
    <!-- Portal tiles -->
    <section id="portal">
      <!-- Items 1..2 -->
      <xsl:apply-templates select="item[position() &lt;= 2]"/>

      <!-- Upcoming events -->
      <article class="upcoming">
        <h3>
          <a href="calendar.html">Kalender</a>
        </h3>
    <xsl:call-template name="calendar-events">
      <xsl:with-param name="limit" select="8"/>
    </xsl:call-template>
      </article>

      <!-- Item 3 -->
      <xsl:apply-templates select="item[position() = 3]"/>

      <!-- Advertisement -->
      <!--article>
      </article-->

      <!-- Items 4..5 -->
      <xsl:apply-templates select="item[position() &gt;= 4 and position() &lt;= 5]"/>

      <!-- Hackspace -->
      <article class="space">
    <a href="space.html">
      <h3>Hackspace</h3>
      <p class="footer">Current status</p>
    </a>
      </article>

      <!-- Items 6..7 -->
      <xsl:apply-templates select="item[position() &gt;= 6 and position() &lt;= 7]"/>

      <!-- Chaosupdates -->
      <!--article class="chaosupdates">
    <xsl:call-template name="chaosupdates">
      <xsl:with-param name="limit" select="3"/>
    </xsl:call-template>
    <p class="footer">ccc.de</p>
      </article-->

      <!-- 8..42 -->
      <xsl:apply-templates select="item[position() &gt;= 8 and position() &lt;= 42]"/>

    </section>
  </xsl:template>

  <xsl:template match="/page/portal/item">
    <!-- 1 (not really) random character 1-9 and blank
        crypto:sha1 depends on crypto being built with xsltproc
     -->
    <xsl:variable name="diceroll"><!--
        --><xsl:choose><!--
            --><xsl:when test="not(function-available('crypto:sha1'))">4</xsl:when><!--
            --><xsl:otherwise><xsl:text select="translate(substring(crypto:sha1(.),1,1),'0abcdef','')"/></xsl:otherwise><!--
        --></xsl:choose><!--
    --></xsl:variable>
    <article>
      <xsl:if test="@class">
    <xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
      </xsl:if>

      <a href="{link}">
    <xsl:choose>
        <xsl:when test="content-image">
            <xsl:attribute name="style">
                background-image: url('<xsl:value-of select="content-image"/>');
            </xsl:attribute>
        </xsl:when>
        <xsl:otherwise><!-- Fallback image -->
            <xsl:choose><!-- depending on class -->
                <!-- "random" image from list (filenames should be links) -->
                <xsl:when test="@class = 'news'"><xsl:attribute name="style">background-image: url('images/news/rnd<xsl:value-of select="$diceroll"/>.jpg');</xsl:attribute></xsl:when>
<!--
                <xsl:when test="@class = 'news'"><xsl:attribute name="style">background-image: url('images/news/gchq-hdr.jpg');</xsl:attribute></xsl:when>
-->
            </xsl:choose>
        </xsl:otherwise>
    </xsl:choose>

    <xsl:apply-templates select="header"/>
    <xsl:apply-templates select="footer"/>
    <xsl:apply-templates select="content-text"/>
      </a>
    </article>
  </xsl:template>

  <xsl:template match="portal/item/header">
    <h3><xsl:value-of select="string(.)"/></h3>
  </xsl:template>

  <xsl:template match="portal/item/footer">
    <p class="footer"><xsl:value-of select="string(.)"/></p>
  </xsl:template>

  <xsl:template match="portal/item/content-text">
    <h4><xsl:value-of select="string(.)"/></h4>
  </xsl:template>

  <xsl:template match="portal/item/content-image">
    <img src="{{.}}"/>
  </xsl:template>

  <xsl:template match="*[substring(name(), string-length(name()) - 6) = '-jsform']">
    <div class="noscript {name()}">
      <xsl:value-of select="."/><xsl:text>&#10;</xsl:text>
    </div>
  </xsl:template>

  <xsl:template match="chat">
    <div id="chat">
      <p>Chat im Browser benötigt JavaScript.</p>
    </div>
  </xsl:template>

</xsl:stylesheet>
