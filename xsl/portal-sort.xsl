<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:date="http://exslt.org/dates-and-times">
  <xsl:template match="/page/portal">
    <page>
      <portal>
	<xsl:for-each select="item">
	  <xsl:sort select="date:seconds(@date)" data-type="number" order="descending" />
	  <xsl:copy-of select="."/>
	</xsl:for-each>
      </portal>
    </page>
  </xsl:template>
</xsl:stylesheet>
