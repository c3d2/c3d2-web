<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rss="http://purl.org/rss/1.0/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:date="http://exslt.org/dates-and-times"
  exclude-result-prefixes="xsl rss rdf">

  <xsl:output method="html" indent="yes" />
  <xsl:template match="paypalbutton">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
      <input type="hidden" name="cmd" value="_s-xclick"/>
      <input type="hidden" name="hosted_button_id" value="G8P6T5HBWNY5Q"/>
      <!-- https://www.paypalobjects.com/de_DE/DE/i/btn/btn_donateCC_LG.gif --><input
        type="image"
        src="images/paypal-btn-donate-CC_LG.gif"
        border="0"
        name="submit"
        alt="Jetzt einfach, schnell und sicher online bezahlen – mit PayPal."/>
      <!--img
        alt=""
        border="0"
        src="https://www.paypalobjects.com/de_DE/i/scr/pixel.gif"
        width="1"
        height="1"/-->
    </form>
  </xsl:template>
</xsl:stylesheet>
