<?xml version="1.0" encoding="utf-8"?>
<!-- This code is distributed under the GPLv2 OR LATER -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times">

  <xsl:template name="ddate">
    <xsl:param name="date"/>

    <xsl:choose>
      <xsl:when test="date:month-in-year($date) = 2 and date:day-in-month($date) = 29">
        <xsl:text>St. Tib's Day</xsl:text>
      </xsl:when>
      <xsl:otherwise>

        <xsl:variable name="yday">
          <xsl:choose>
            <xsl:when test="date:leap-year($date) and date:month-in-year($date) &gt; 2">
              <xsl:value-of select="date:day-in-year($date) - 1"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="date:day-in-year($date)"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="season" select="floor(($yday - 1) div 73) + 1"/>
        <xsl:variable name="mday" select="(($yday - 1) mod 73) + 1"/>
        <xsl:variable name="wday" select="($yday - 1) mod 5"/>
        
        <xsl:choose>
          <xsl:when test="$wday = 0">Sweetmorn</xsl:when>
          <xsl:when test="$wday = 1">Boomtime</xsl:when>
          <xsl:when test="$wday = 2">Pungenday</xsl:when>
          <xsl:when test="$wday = 3">Prickle-Prickle</xsl:when>
          <xsl:when test="$wday = 4">Setting Orange</xsl:when>
          <xsl:otherwise>???</xsl:otherwise>
        </xsl:choose>
        <xsl:text>, the </xsl:text>
        <xsl:value-of select="$mday"/>
        <xsl:choose>
          <xsl:when test="$mday = 11">th</xsl:when>
          <xsl:when test="$mday = 12">th</xsl:when>
          <xsl:when test="$mday = 13">th</xsl:when>
          <xsl:when test="$mday mod 10 = 1">st</xsl:when>
          <xsl:when test="$mday mod 10 = 2">nd</xsl:when>
          <xsl:when test="$mday mod 10 = 3">rd</xsl:when>
          <xsl:otherwise>th</xsl:otherwise>
        </xsl:choose>
        <xsl:text> of </xsl:text>
        <xsl:choose>
          <xsl:when test="$season = 1">Chaos</xsl:when>
          <xsl:when test="$season = 2">Discord</xsl:when>
          <xsl:when test="$season = 3">Confusion</xsl:when>
          <xsl:when test="$season = 4">Bureaucracy</xsl:when>
          <xsl:when test="$season = 5">The Aftermath</xsl:when>
          <xsl:otherwise>???</xsl:otherwise>
        </xsl:choose>
        
      </xsl:otherwise>
    </xsl:choose>
        <xsl:text>, </xsl:text>
        <xsl:value-of select="date:year($date) + 1166"/> YOLD
  </xsl:template>

  <xsl:template name="ddate-holiday">
    <xsl:param name="date"/>

    <xsl:variable name="yday">
      <xsl:choose>
	<xsl:when test="date:leap-year($date) and date:month-in-year($date) &gt; 2">
	  <xsl:value-of select="date:day-in-year($date) - 1"/>
	</xsl:when>
	<xsl:otherwise>
	  <xsl:value-of select="date:day-in-year($date)"/>
	</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="mday" select="(($yday - 1) mod 73) + 1"/>
    <xsl:variable name="season" select="floor(($yday - 1) div 73) + 1"/>

    <xsl:choose>
      <xsl:when test="date:month-in-year($date) = 2 and
                      date:day-in-month($date) = 29">
        <xsl:text>St. Tib's Day</xsl:text>
      </xsl:when>
      <!-- APOSTLE HOLIDAYS -->
      <xsl:when test="$mday = 5">
        <xsl:choose>
          <xsl:when test="$season = 1">Mungday</xsl:when>
          <xsl:when test="$season = 2">Mojoday</xsl:when>
          <xsl:when test="$season = 3">Syaday</xsl:when>
          <xsl:when test="$season = 4">Zaraday</xsl:when>
          <xsl:when test="$season = 5">Maladay</xsl:when>
        </xsl:choose>
      </xsl:when>
      <!-- SEASON HOLIDAYS -->
      <xsl:when test="$mday = 50">
        <xsl:choose>
          <xsl:when test="$season = 1">Chaoflux</xsl:when>
          <xsl:when test="$season = 2">Discoflux</xsl:when>
          <xsl:when test="$season = 3">Confuflux</xsl:when>
          <xsl:when test="$season = 4">Bureflux</xsl:when>
          <xsl:when test="$season = 5">Afflux</xsl:when>
        </xsl:choose>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- Test support code -->
  <xsl:template match="dt:ddate-test" xmlns:dt="http://www.c3d2.de/ddate-test">
    <xsl:call-template name="ddate">
      <xsl:with-param name="date" select="."/>
    </xsl:call-template>
    <xsl:text> - </xsl:text>
    <xsl:call-template name="ddate-holiday">
      <xsl:with-param name="date" select="."/>
    </xsl:call-template>
  </xsl:template>

</xsl:stylesheet>
