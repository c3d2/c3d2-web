<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" encoding="utf-8"/>

<xsl:template match="/config">
  <xsl:text>fetch:</xsl:text>
  <xsl:for-each select="*[@id]">
    <xsl:text> portal-</xsl:text>
    <xsl:value-of select="@id"/>
    <xsl:text>.xml</xsl:text>
  </xsl:for-each>
  <xsl:text>&#10;</xsl:text>
  <xsl:apply-templates select="*[@id]"/>
</xsl:template>

<xsl:template match="/config/*">
  <xsl:text>portal-</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text>.xml:&#10;	wget -O $@ --tries=1 --no-check-certificate '</xsl:text>
  <xsl:value-of select="string(.)"/>
  <xsl:text>' 2&#62;&#62; wget.log || true &#10;</xsl:text>
</xsl:template>

</xsl:stylesheet>
