<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:crypto="http://exslt.org/crypto">

<xsl:output method="text" encoding="utf-8"/>

  <xsl:include href="common.xsl"/>

  <xsl:variable name="baseurl">https://www.c3d2.de/</xsl:variable>

  <xsl:template match="/page">
    <xsl:apply-templates select="calendar-summary"/>
  </xsl:template>

  <xsl:template match="calendar-summary">
    <xsl:for-each select="event">
      <xsl:sort select="date:seconds(@start)" data-type="number" order="descending" />

      <xsl:text>REM</xsl:text>
      <xsl:apply-templates select="start"/>
      <xsl:apply-templates select="end"/>
      <xsl:text> MSG </xsl:text>
      <xsl:apply-templates select="title"/>
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="location"/>
      <xsl:text>  </xsl:text>
      <xsl:apply-templates select="link"/>

      <xsl:text>&#10;</xsl:text>

    </xsl:for-each>
  </xsl:template>

  <xsl:template match="start">
    <xsl:text> </xsl:text>
    <xsl:call-template name="make-datespec"/>
  </xsl:template>

  <xsl:template match="end">
    <xsl:variable name="begin" select="string(../start[1])"/>
    <xsl:variable name="end">
      <xsl:value-of select="string(.)"/>
      <xsl:if test="not(contains(string(.), 'T'))">
        <xsl:text>T23:59:59</xsl:text>
      </xsl:if>
    </xsl:variable>
    <xsl:variable name="duration" select="date:seconds(date:difference($begin, $end))"/>

    <!-- TODO: Fix duration of multi-day events where end date has no
         time part
      -->
    <xsl:if test="$duration != 0">
      <xsl:text> DURATION </xsl:text>
      <xsl:value-of select="round($duration div 3600)"/>
      <xsl:text>:</xsl:text>
      <xsl:value-of select="format-number(round($duration div 60) mod 60, '00')"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="make-datespec">
    <xsl:param name="date" select="string(.)"/>

    <xsl:value-of select="date:month-abbreviation($date)"/>
      <xsl:text> </xsl:text>
    <xsl:value-of select="date:day-in-month($date)"/>
      <xsl:text> </xsl:text>
    <xsl:value-of select="date:year($date)"/>

    <xsl:if test="contains($date, 'T')">
      <xsl:text> AT </xsl:text>
      <xsl:value-of select="date:hour-in-day($date)"/>
      <xsl:text>:</xsl:text>
      <xsl:value-of select="format-number(date:minute-in-hour($date), '00')"/>
    </xsl:if>
  </xsl:template>

  <xsl:template match="title">
    <xsl:value-of select="string(.)"/>
  </xsl:template>

  <xsl:template match="link">
    <xsl:call-template name="make-href">
      <xsl:with-param name="href" select="string(.)"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="location">
    <xsl:value-of select="translate(string(), '&#10;&#13;', '  ')"/>
  </xsl:template>

</xsl:stylesheet>
