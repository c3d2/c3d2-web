<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  exclude-result-prefixes="xsl date">

<xsl:include href="date.xsl" />

  <xsl:output method="xml"
              version="1.0"
              encoding="utf-8"
              doctype-system="http://www.c3d2.de/dtd/c3d2web.dtd"
              indent="yes"/>

  <xsl:template match="/calendar">
    <page>
      <calendar-summary>
        <!-- Events aus events.xml ziehen -->
        <xsl:apply-templates select="event"/>

        <!-- Events aus den News ziehen -->
        <xsl:for-each select="document('../news.xml')/page/news/newsfile">
          <xsl:variable name="news" select="document(concat('../', .))"/>
          <xsl:if test="starts-with(date:difference(date:date-time(), concat($news/item/@date, '+02:00')), '-')">
            <xsl:apply-templates select="$news/item/event">
              <xsl:with-param name="link" select="concat(substring-before(substring-after(., 'content/'), '.xml'), '.html')"/>
            </xsl:apply-templates>
          </xsl:if>
        </xsl:for-each>

        <!-- Events aus Autotopia ziehen -->
        <xsl:for-each select="document('../autotopia-events.xml')/page/news/newsfile">
          <xsl:variable name="news" select="document(concat('../', .))"/>
          <xsl:apply-templates select="$news/page/event">
            <xsl:with-param name="link" select="concat('https://autotopia.c3d2.de/', substring-before(substring-after(., 'content/autotopia/'), '.xml'), '.html')"/>
          </xsl:apply-templates>
        </xsl:for-each>
      </calendar-summary>
    </page>
  </xsl:template>

  <xsl:template match="event">
    <xsl:param name="link"/>

<!-- vv01f: validate, terminate with message on error at date-time-groups -->
	<xsl:call-template name="validate-dtg">
		<xsl:with-param name="date" select="start" />
		<xsl:with-param name="label" select="name(start)" />
	</xsl:call-template>
	<xsl:call-template name="validate-dtg">
		<xsl:with-param name="date" select="end" />
		<xsl:with-param name="label" select="name(end)" />
	</xsl:call-template>

    <!-- Events must have started at least one year ago -->
    <xsl:variable name="minstart" select="date:add(date:date-time(), '-P366D')"/>
    <xsl:if test="starts-with(date:difference(start, $minstart), '-')">
      <event>
	<title>
	  <xsl:choose>
	    <xsl:when test="@title"><xsl:value-of select="@title"/></xsl:when>
	    <xsl:when test="../@title"><xsl:value-of select="../@title"/></xsl:when>
	    <xsl:otherwise>
	      <xsl:message terminate="yes">Event with no title!</xsl:message>
	    </xsl:otherwise>
	  </xsl:choose>
	</title>
	<start><xsl:value-of select="start"/></start>
	<end>
	  <xsl:choose>
	    <xsl:when test="end">
	      <xsl:value-of select="end"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:value-of select="start"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</end>
	<xsl:if test="location">
	  <location><xsl:value-of select="location"/></location>
	</xsl:if>
	<link>
	  <xsl:choose>
	    <xsl:when test="link"><xsl:value-of select="link"/></xsl:when>
	    <xsl:otherwise><xsl:value-of select="$link"/></xsl:otherwise>
	  </xsl:choose>
	</link>
	<!-- TODO: Link -->
      </event>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
