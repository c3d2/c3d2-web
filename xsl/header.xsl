<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rss="http://purl.org/rss/1.0/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:date="http://exslt.org/dates-and-times"
  exclude-result-prefixes="xsl rss rdf date">

  <xsl:template name="links">
    <link rel="icon" type="image/png" href="{$baseurl}favicon.ico" />
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="{$baseurl}news-rss.xml" />
    <link rel="alternate" type="application/atom+xml" title="Atom 1.0" href="{$baseurl}news-atom.xml" />
    <link rel="stylesheet" title="Default" type="text/css" href="{$baseurl}style/default.css" />
    <link rel="space-api" title="Hackerspace API Endpoint" type="application/json" href="{$baseurl}spaceapi.json"/>
    <link rel="me" href="https://c3d2.social/@c3d2"/>
    <link rel="me" href="https://c3d2.social/@datenspuren"/>
    <xsl:choose>
      <xsl:when test="@title = 'Kalender'">
        <link rel="stylesheet" type="text/css" href="{$baseurl}style/ico.css" />
      </xsl:when>
      <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="header">
  <xsl:param name="title"/>
  <xsl:element name="head">

  <title>
  <xsl:text>&lt;&lt;&lt;/&gt;&gt; Chaos Computer Club Dresden | C3D2</xsl:text>
  <xsl:if test="$title != ''">
    <xsl:value-of select="$title" />
  </xsl:if>
  </title>

  <meta charset="utf-8" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
  <meta name="age-de-meta-label" content="age=0 hash:2f543505f7d9014bc35c3bc405bb6739c7ee87bd kind=sl protocol=all" />
  <!-- Value 4 Value via Bitcoin Lightning, s. a. unterstuetzen.xml username Unterscheidung anhand Content -->
  <xsl:choose>
    <xsl:when test="@title = 'Pentaradio'">
      <meta name="lightning" content="lnurlp:pentaradio@c3d2.de"/>
      <meta property="og:image" content="https://c3d2.de/images/pentaradio.png" />
    </xsl:when>
    <xsl:when test="@title = 'Unterstützen'">
      <meta name="lightning" content="lnurlp:unterstuetzen@c3d2.de"/>
      <meta property="og:image" content="https://c3d2.de/images/logo.svg" />
    </xsl:when>
    <xsl:otherwise>
      <meta name="lightning" content="lnurlp:value4value@c3d2.de"/>
      <meta property="og:image" content="https://c3d2.de/images/logo.svg" />
    </xsl:otherwise>
  </xsl:choose>
  <xsl:choose>
    <!-- Chat aus Suchmaschine herauswünschen -->
    <xsl:when test="@title = 'Chat'">
      <meta name="robots" content="noindex" />
    </xsl:when>
    <xsl:otherwise>
      <meta name="robots" content="index,follow" />
    </xsl:otherwise>
  </xsl:choose>
  <meta name="description" content="&lt;&lt;&lt;/&gt;&gt; C3D2, der Erfahrungsaustauschkreis aus Dresden" />
  <meta name="keywords" content="c3d2 ccc dresden sachsen hacker" />
  <meta name="DC.Creator" content="Fnordic walker" />
  <meta name="DC.Description" content="&lt;&lt;&lt;/&gt;&gt; C3D2, der Erfahrungsaustauschkreis aus Dresden" />
  <meta name="DC.Publisher" content="Chaos Computer Club Dresden" />
  <meta name="DC.Date" content="May 23 1981" />
  <meta name="DC.Type" content="Text" />
  <meta name="DC.Format" content="text/html" />
  <meta name="DC.Language" content="de" />
  <meta name="DC.Coverage" content="Dresden" />
  <meta name="DC.Title" content="C3D2: Chaos Computer Club Dresden" />
  <meta name="generator" content="C3D2-Web" />
  <meta name="language" content="de" />
  <meta name="author" content="Fnordic walker" />
  <meta name="date" content="May 23 1981" />
  <meta name="fediverse:creator" content="@c3d2@c3d2.social" />

  <xsl:call-template name="links" />

  <xsl:comment>[if lt IE 9]&gt;
      &lt;script src="<xsl:value-of select="$baseurl"/>script/ie-html5.js" type="text/javascript"&gt;&lt;/script&gt;
  &lt;![endif]</xsl:comment>
  <xsl:call-template name="make-script-tag"><xsl:with-param name="name">jquery-1.8.3.min</xsl:with-param></xsl:call-template>
  <xsl:call-template name="make-script-tag"><xsl:with-param name="name">jquery.dataset</xsl:with-param></xsl:call-template>
  <xsl:call-template name="make-script-tag"><xsl:with-param name="name">play-resources</xsl:with-param></xsl:call-template>
  <xsl:if test="@title = 'Kalender'">
    <xsl:call-template name="make-script-tag"><xsl:with-param name="name">calendar</xsl:with-param></xsl:call-template>
    <xsl:call-template name="make-script-tag"><xsl:with-param name="name">eventform</xsl:with-param></xsl:call-template>
  </xsl:if>
  <xsl:if test="@title = 'Mitgliedschaft'">
    <xsl:call-template name="make-script-tag"><xsl:with-param name="name">membership</xsl:with-param></xsl:call-template>
  </xsl:if>
  <xsl:if test="@title = 'Chat'">
    <xsl:call-template name="make-script-tag"><xsl:with-param name="name">chat</xsl:with-param></xsl:call-template>
  </xsl:if>

  </xsl:element>
  </xsl:template>

  <xsl:template name="make-script-tag">
    <xsl:param name="name"/>
    <script src="{$baseurl}script/{$name}.js" type="text/javascript" defer="defer">
    <xsl:text>
    </xsl:text>
    </script>
  </xsl:template>

</xsl:stylesheet>
