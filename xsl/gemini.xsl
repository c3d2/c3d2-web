<?xml version="1.0" encoding="utf-8"?>
<stylesheet version="1.0"
  xmlns="http://www.w3.org/1999/XSL/Transform"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:crypto="http://exslt.org/crypto">

  <output method="text" encoding="utf-8"/>
  <strip-space elements="page item addendum image event div p ul ol li blockquote"/>
  <include href="date.xsl"/>

  <template match="/page[portal]">
    <text># c3d2.de&#10;</text>
    <text>## &lt;&lt;&lt;/&gt;&gt; Chaos Computer Club Dresden&#10;&#10;</text>

    <text>## Navigation&#10;</text>
    <for-each select="document('../content/navigation.xml')/navigation/group">
      <apply-templates mode="navitem" select="child::*"/>
      <text>&#10;</text>
    </for-each>

    <call-template name="body"/>
  </template>

  <template mode="navitem" match="file">
    <text>=&gt; </text>
    <value-of select="concat($baseurl, ., '.gmi')"/>
    <text> </text>
    <choose>
      <when test="string-length(@title) = 0">
        <value-of select="document(concat('../content/pages/', ., '.xml'))/page/@title"/>
      </when>
      <otherwise>
        <value-of select="@title"/>
      </otherwise>
    </choose>
    <text>&#10;</text>
  </template>

  <template mode="navitem" match="link">
    <text>=> </text>
    <call-template name="make-href">
      <with-param name="href">
        <choose>
          <when test="@href"><value-of select="@href"/></when>
          <otherwise><value-of select="."/></otherwise>
        </choose>
      </with-param>
    </call-template>
    <text> </text>
    <call-template name="normalize-paragraph">
      <with-param name="text" select="normalize-space(.)"/>
    </call-template>
    <text>&#10;</text>
  </template>

  <template match="/page[@pagetitle]">
    <text># </text><value-of select="@pagetitle"/><text> | c3d2.de&#10;</text>

    <call-template name="body"/>
  </template>

  <template match="/page[@title]">
    <text># </text><value-of select="@title"/><text> | c3d2.de&#10;</text>

    <call-template name="body"/>
  </template>

  <template name="body">
    <apply-templates/>

    <text>&#10;&#10;=> </text>
    <value-of select="$baseurl"/>
    <text>index.gmi Start&#10;</text>
  </template>

  <!-- news -->

  <template match="/item">
    <text># </text><value-of select="@title"/><text> | c3d2&#10;</text>

    <call-template name="get-datestring">
      <with-param name="date" select="@date"/>
    </call-template>
    <text> von </text>
    <value-of select="@author"/>
    <text>&#10;&#10;</text>

    <call-template name="body"/>
  </template>

  <template match="/page/news">
    <for-each select="newsfile">
      <sort select="date:seconds(document(concat('../', .))/item/@date)" data-type="number" order="descending" />
      <variable name="news" select="document(concat('../', .))"/>
      <if test="position() &lt;= 10 and
                    starts-with(
                    date:difference(
                    date:date-time(),
                    concat($news/item/@date, '+02:00')
                    ),
                    '-'
                    )">

        <variable name="newsfile"><value-of select="."/></variable>
        <variable name="itemposition" select="position()"/>

        <for-each select="$news/item">
          <text>&#10;## </text>
          <value-of select="@title"/>
          <text>&#10;</text>

          <call-template name="get-datestring">
            <with-param name="date" select="@date"/>
          </call-template>
          <text> von </text>
          <value-of select="@author"/>
          <text>&#10;&#10;</text>

          <apply-templates select="*[
            name() = 'p' or name() = 'blockquote' or name() = 'ul' or name() = 'ol' or
            name() = 'event' or name() = 'resource'
          ]"/>

          <text>=> </text>
          <value-of select="substring-before(substring-after($newsfile, 'content/'), '.xml')"/>
          <text>.gmi News lesen&#10;</text>
        </for-each>
      </if>
    </for-each>

    <text>&#10;&#10;# Ressourcen&#10;</text>
    <text>=> news-atom.xml ATOM&#10;</text>
    <text>=> news-rss.xml RSS&#10;</text>
    <text>=> news-archiv.gmi Archiv&#10;</text>
  </template>

  <template match="news-list">
    <variable name="prefix" select="@prefix"/>
    <variable name="details" select="@details"/>

    <for-each select="document('../news.xml')/page/news/newsfile">
      <sort select="date:seconds(document(concat('../', .))/item/@date)" data-type="number" order="descending" />
      <variable name="newsfile" select="."/>
      <variable name="news" select="document(concat('../', $newsfile))"/>

      <for-each select="$news/item">
        <if test="((string-length($prefix) = 0) or
                      starts-with($newsfile, concat('content/news/', $prefix))) and
                      (($details != 'resources-only') or
                      (count(resource) &gt; 0)) and
                      starts-with(
                      date:difference(
                      date:date-time(),
                      concat($news/item/@date, '+02:00')
                      ),
                      '-'
                      )">

          <text>&#10;## </text>
          <value-of select="@title"/>
          <text>&#10;</text>

          <call-template name="get-datestring">
            <with-param name="date" select="@date"/>
          </call-template>
          <text> von </text>
          <value-of select="@author"/>
          <text>&#10;</text>

          <text>=> </text>
          <value-of select="substring-before(substring-after($newsfile, 'content/'), '.xml')"/>
          <text>.gmi News lesen&#10;</text>

        </if>
      </for-each>
    </for-each>
  </template>

  <!-- portal -->

  <template match="/page/portal">
    <text>&#10;## Kalender&#10;</text>
    <call-template name="calendar-events"/>

    <text>&#10;## Changelog&#10;</text>
    <apply-templates select="item[position() &lt;= 42]"/>
  </template>

  <template match="calendar">
    <text>&#10;## Kalender&#10;</text>
    <call-template name="calendar-events">
      <with-param name="limit" select="23"/>
      <with-param name="src" select="@src"/>
    </call-template>
    <text>&#10;</text>
  </template>

  <template name="calendar-events">
    <param name="limit" select="8"/>
    <param name="src" select="'../calendar-summary.xml'"/>

    <for-each select="document($src)/page/calendar-summary/event[date:seconds(end) &gt;= date:seconds(date:date())]">
      <sort select="date:seconds(start)" data-type="number" order="ascending" />

      <if test="position() &lt;= $limit">
        <choose>
          <when test="string-length(link) &gt; 0">
            <text>=> </text>
            <call-template name="make-href">
              <with-param name="href" select="link"/>
            </call-template>
          </when>
          <otherwise>
            <text>*</text>
          </otherwise>
        </choose>

        <text> </text>
        <call-template name="datespan-to-human">
          <with-param name="start" select="start"/>
          <with-param name="end" select="end"/>
        </call-template>
        <text>: </text>
        <value-of select="title"/>
        <text>&#10;</text>
      </if>
    </for-each>
  </template>

  <template match="/page/portal/item">
    <text>=> </text>
    <call-template name="make-href">
      <with-param name="href" select="link"/>
    </call-template>
    <text> </text>
    <if test="@date">
      <value-of select="substring-before(@date, 'T')"/>
      <text> </text>
    </if>
    <if test="content-text">
      <call-template name="normalize-paragraph">
        <with-param name="text" select="normalize-space(content-text)"/>
      </call-template>
      <text> (</text>
    </if>
    <call-template name="normalize-paragraph">
      <with-param name="text" select="normalize-space(header)"/>
    </call-template>
    <text> </text>
    <call-template name="normalize-paragraph">
      <with-param name="text" select="normalize-space(footer)"/>
    </call-template>
    <if test="content-text">
      <text>)</text>
    </if>
    <text>&#10;</text>
  </template>

  <!-- content templates -->

  <template match="*[name() = 'resource' or name() = 'alternative']">
    <variable name="url">
      <call-template name="make-href">
        <with-param name="href" select="@url"/>
      </call-template>
    </variable>

    <text>=> </text>
    <value-of select="$url"/>
    <text> </text>
    <value-of select="@type"/>
    <text> (</text>
    <call-template name="format-filesize">
      <with-param name="value" select="@size"/>
    </call-template>
    <text>)&#10;</text>
  </template>

  <template match="event">
    <apply-templates/>
    <text>&#10;</text>
  </template>

  <template match="event/start">
    <text>* Beginn: </text>
    <call-template name="get-datestring">
      <with-param name="date" select="."/>
    </call-template>
    <text>&#10;</text>
  </template>

  <template match="event/end">
    <text>* Ende: </text>
    <call-template name="get-datestring">
      <with-param name="date" select="."/>
    </call-template>
    <text>&#10;</text>
  </template>

  <template match="event/location">
    <text>* Ort: </text>
    <call-template name="normalize-paragraph">
      <with-param name="text" select="normalize-space(.)"/>
    </call-template>
    <text>&#10;</text>
  </template>

  <template match="image"/>

  <template match="ol">
    <apply-templates/>
    <text>&#10;</text>
  </template>

  <template match="ul">
    <apply-templates/>
    <text>&#10;</text>
  </template>

  <template match="li[string(.) = string(link)]">
    <text>=> </text>
    <call-template name="make-href">
      <with-param name="href">
        <choose>
          <when test="link/@href"><value-of select="link/@href"/></when>
          <otherwise><value-of select="."/></otherwise>
        </choose>
      </with-param>
    </call-template>
    <text> </text>
    <call-template name="normalize-paragraph">
      <with-param name="text" select="normalize-space(link)"/>
    </call-template>
    <text>&#10;</text>
  </template>

  <template match="li">
    <text>* </text>
    <call-template name="normalize-paragraph">
      <with-param name="text" select="normalize-space(.)"/>
    </call-template>
    <text>&#10;</text>
  </template>

  <template match="p">
    <if test="@title">
      <text>## </text>
      <value-of select="@title"/>
      <text>&#10;</text>
    </if>

    <call-template name="normalize-paragraph">
      <with-param name="text" select="normalize-space(.)"/>
    </call-template>
    <text>&#10;</text>

    <call-template name="link-list"/>
    <text>&#10;</text>
  </template>

  <template match="blockquote">
    <text>&#10;```&#10;</text>
    <apply-templates/>
    <text>&#10;```&#10;</text>
  </template>

  <template name="normalize-paragraph">
    <param name="text"/>

    <choose>
      <when test="contains($text, '&#10;')">
        <value-of select="substring-before($text, '&#10;')"/>
        <call-template name="normalize-paragraph">
          <with-param name="text" select="substring-after($text, '&#10;')"/>
        </call-template>
      </when>
      <otherwise>
        <value-of select="$text"/>
      </otherwise>
    </choose>
  </template>

  <template name="link-list">
    <for-each select=".//link">
      <text>=> </text>
      <call-template name="make-href">
        <with-param name="href">
          <choose>
            <when test="@href"><value-of select="@href"/></when>
            <otherwise><value-of select="."/></otherwise>
          </choose>
        </with-param>
      </call-template>
      <text> </text>
      <choose>
        <when test="image/@title">
          <call-template name="normalize-paragraph">
            <with-param name="text" select="normalize-space(image/@title)"/>
          </call-template>
        </when>
        <otherwise>
          <call-template name="normalize-paragraph">
            <with-param name="text" select="normalize-space(.)"/>
          </call-template>
        </otherwise>
      </choose>
      <text>&#10;</text>
    </for-each>
  </template>

  <template match="xhtml:*">
  </template>

  <!-- helpers -->

  <template name="make-href">
    <param name="href"/>

    <choose>
      <when test="$href and not(contains($href, ':')) and substring($href, string-length($href) - 4) = '.html'">
        <value-of select="concat($baseurl, substring-before($href, '.html'), '.gmi')"/>
      </when>
      <when test="$href and not(contains($href, ':'))">
        <value-of select="concat($baseurl, $href)"/>
      </when>
      <when test="$href">
        <value-of select="$href"/>
      </when>
      <otherwise>
        <value-of select="."/>
      </otherwise>
    </choose>
  </template>

  <template name="format-filesize">
    <param name="value"/>

    <choose>
      <when test="$value &gt; 1024 * 1024 * 1024">
        <value-of select="format-number($value div (1024 * 1024 * 1024), '0.0')"/>
        <text> GiB</text>
      </when>
      <when test="$value &gt; 1024 * 1024">
        <value-of select="format-number($value div (1024 * 1024), '0.0')"/>
        <text> MiB</text>
      </when>
      <when test="$value &gt; 1024">
        <value-of select="format-number($value div (1024), '0.0')"/>
        <text> KiB</text>
      </when>
      <otherwise>
        <value-of select="$value"/>
        <text> Byte</text>
      </otherwise>
    </choose>
  </template>
</stylesheet>
