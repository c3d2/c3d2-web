<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="xsl">

  <xsl:output method="xml"
              version="1.0"
              encoding="utf-8"
              indent="yes"/>

  <xsl:include href="date.xsl" />

  <xsl:variable name="baseurl">http://www.c3d2.de/</xsl:variable>

  <xsl:template match="item">
    <rss version="2.0">

      <channel>
	<title><xsl:value-of select="@title"/></title>
	<link><xsl:value-of select="$baseurl"/></link>
	<description><xsl:value-of select="@title"/></description>
	<language>de</language>
	<copyright>http://creativecommons.org/licenses/by-sa/2.0/de/</copyright>
	<xsl:apply-templates select="image"/>

        <itunes:author>Chaos Computer Club Dresden</itunes:author>
        <itunes:owner>
          <itunes:name>C3D2</itunes:name>
          <itunes:email>pentaradio@c3d2.de</itunes:email>
        </itunes:owner>
	<xsl:if test="image">
	  <itunes:image href="{concat($baseurl, 'images/news/', string(image))}"/>
	</xsl:if>

        <xsl:apply-templates select="*[name() = 'resource' or name() = 'addendum']"/>
      </channel>

    </rss>
  </xsl:template>

  <xsl:template match="image">
    <image>
      <title><xsl:value-of select="@title"/></title>
      <url><xsl:value-of select="concat($baseurl, 'images/news/', string(.))"/></url>
      <link><xsl:value-of select="$baseurl"/></link>
    </image>
  </xsl:template>

  <xsl:template match="addendum">
    <xsl:apply-templates select="resource"/>
  </xsl:template>

  <xsl:template match="resource">
    <item>
      <title><xsl:value-of select="@title"/></title>
      <xsl:if test="@feedback-link">
	<link><xsl:value-of select="@feedback-link"/></link>
      </xsl:if>

      <xsl:apply-templates select="." mode="generate-enclosures"/>

      <guid isPermaLink="false"><xsl:value-of select="concat(@feedback-link, '+++', @url, alternative/@url)"/></guid>

      <pubDate>
        <xsl:call-template name="date-to-rfc822">
          <xsl:with-param name="date" select="/item/@date"/>
        </xsl:call-template>
      </pubDate>

      <xsl:if test="@poster">
	<itunes:image href="{@poster}"/>
      </xsl:if>
    </item>
  </xsl:template>

  <xsl:template match="*[name() = 'resource' or name() = 'alternative']"
		mode="generate-enclosures">
    <enclosure
        url="{@url}"
        length="{@size}"
        type="{@type}"
        />
    <xsl:apply-templates mode="generate-enclosures"/>
  </xsl:template>
  <xsl:template match="*"
		mode="generate-enclosures">
  </xsl:template>

</xsl:stylesheet>
