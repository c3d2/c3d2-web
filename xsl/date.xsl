<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  exclude-result-prefixes="xsl">

<!--xsl:template name="isleap">
  <xsl:param name="year" />

  <xsl:value-of select="$year mod 4 = 0 and ($year mod 100 != 0 or $year mod 400 = 0)" />
</xsl:template>

<xsl:template name="leapdays">
  <- Berechnet die Schalttage, die bis $year seit 1970 vergangen sind ->
  <xsl:param name="year" />

  <xsl:variable name="four">
    <xsl:value-of select="floor($year div 4)" />
  </xsl:variable>

  <xsl:variable name="hundred">
    <xsl:value-of select="floor($year div 100)" />
  </xsl:variable>

  <xsl:variable name="fourhundred">
    <xsl:value-of select="floor($year div 400)" />
  </xsl:variable>

  <xsl:value-of select="$four - $hundred + $fourhundred - 477" />
</xsl:template>

<xsl:template name="correct-seconds">
  <- Berechnet die Sekunden die vergangen wären, wenn es keine Schaltjahre gäbe ->
  <xsl:param name="seconds" />

  <xsl:variable name="yearguessed">
    <xsl:value-of select="floor($seconds div (86400 * 365)) + 1970" />
  </xsl:variable>

  <xsl:variable name="leapdays">
    <xsl:call-template name="leapdays">
      <xsl:with-param name="year" select="$yearguessed" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="secondscorrected">
    <xsl:value-of select="$seconds - $leapdays * 86400" />
  </xsl:variable>

  <xsl:variable name="year">
    <xsl:value-of select="floor($secondscorrected div (86400 * 365)) + 1970" />
  </xsl:variable>

  <xsl:variable name="isleap">
    <xsl:call-template name="isleap">
      <xsl:with-param name="year" select="$year" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="days">
    <xsl:value-of select="floor(($secondscorrected div 86400) mod 365) + 1" />
  </xsl:variable>


  <xsl:choose>
    <xsl:when test="$isleap">
      <xsl:value-of select="$secondscorrected + 86400" />
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$secondscorrected" />
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>

<xsl:template name="get-days">
  <xsl:param name="seconds" />

  <xsl:variable name="secondscorrected">
    <xsl:call-template name="correct-seconds">
      <xsl:with-param name="seconds" select="$seconds"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:value-of select="floor(($secondscorrected mod (365 * 86400)) div 86400) + 1" />
</xsl:template>

<xsl:template name="get-year">
  <xsl:param name="seconds" />

  <xsl:variable name="secondscorrected">
    <xsl:call-template name="correct-seconds">
      <xsl:with-param name="seconds" select="$seconds"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:value-of select="1970 + floor($secondscorrected div (86400 * 365))" />

</xsl:template>

<xsl:template name="get-month">
  <xsl:param name="seconds" />

  <xsl:variable name="year">
    <xsl:call-template name="get-year">
      <xsl:with-param name="seconds" select="$seconds"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="isleap">
    <xsl:call-template name="isleap">
      <xsl:with-param name="year" select="$year" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="febdays">
    <xsl:choose>
      <xsl:when test="$isleap = 'true'">
        <xsl:value-of select="29" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="28" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="days">
    <xsl:call-template name="get-days">
      <xsl:with-param name="seconds" select="$seconds" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:choose>
    <xsl:when test="$days &lt;= 31">
      <xsl:value-of select="1" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays">
      <xsl:value-of select="2" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31">
      <xsl:value-of select="3" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30">
      <xsl:value-of select="4" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31">
      <xsl:value-of select="5" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30">
      <xsl:value-of select="6" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31">
      <xsl:value-of select="7" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31">
      <xsl:value-of select="8" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30">
      <xsl:value-of select="9" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31">
      <xsl:value-of select="10" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30">
      <xsl:value-of select="11" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31">
      <xsl:value-of select="12" />
    </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="get-dayofmonth">
  <xsl:param name="seconds" />

  <xsl:variable name="year">
    <xsl:call-template name="get-year">
      <xsl:with-param name="seconds" select="$seconds"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="isleap">
    <xsl:call-template name="isleap">
      <xsl:with-param name="year" select="$year" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="febdays">
    <xsl:choose>
      <xsl:when test="$isleap = 'true'">
        <xsl:value-of select="29" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="28" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="days">
    <xsl:call-template name="get-days">
      <xsl:with-param name="seconds" select="$seconds" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:choose>
    <xsl:when test="$days &lt;= 31">
      <xsl:value-of select="$days" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays">
      <xsl:value-of select="$days - 31" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31">
      <xsl:value-of select="$days - (31 + $febdays)" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30">
      <xsl:value-of select="$days - (31 + $febdays + 31)" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31">
      <xsl:value-of select="$days - (31 + $febdays + 31 + 30)" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30">
      <xsl:value-of select="$days - (31 + $febdays + 31 + 30 + 31)" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31">
      <xsl:value-of select="$days - (31 + $febdays + 31 + 30 + 31 + 30)" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31">
      <xsl:value-of select="$days - 31 + $febdays + 31 + 30 + 31 + 30 + 31" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30">
      <xsl:value-of select="$days - (31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31)" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31">
      <xsl:value-of select="$days - (31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30)" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30">
      <xsl:value-of select="$days - (31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31)" />
    </xsl:when>
    <xsl:when test="$days &lt;= 31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31">
      <xsl:value-of select="$days - (31 + $febdays + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30)" />
    </xsl:when>
  </xsl:choose>

</xsl:template>

<xsl:template name="get-dayofweek">
  <- 1 entspricht Montag ->
  <xsl:param name="seconds" />

  <- 1. Januar 1970 war ein Donnerstag ->
  <xsl:value-of select="(floor($seconds div 86400) + 3) mod 7 + 1" />
</xsl:template-->

<xsl:template name="get-dayofweekstring">
  <xsl:param name="date" />
  <!--xsl:param name="seconds" /-->

  <xsl:variable name="dayofweek">
    <xsl:value-of select="date:day-in-week($date)"/>
    <!--xsl:call-template name="get-dayofweek">
      <xsl:with-param name="seconds" select="$seconds" />
    </xsl:call-template-->
  </xsl:variable>

  <xsl:choose>
    <xsl:when test="$dayofweek = 2">Montag</xsl:when>
    <xsl:when test="$dayofweek = 3">Dienstag</xsl:when>
    <xsl:when test="$dayofweek = 4">Mittwoch</xsl:when>
    <xsl:when test="$dayofweek = 5">Donnerstag</xsl:when>
    <xsl:when test="$dayofweek = 6">Freitag</xsl:when>
    <xsl:when test="$dayofweek = 7">Sonnabend</xsl:when>
    <xsl:when test="$dayofweek = 1">Sonntag</xsl:when>
  </xsl:choose>
</xsl:template>

<!--xsl:template name="get-timestring">
  <xsl:param name="seconds" />

  <xsl:variable name="hour">
    <xsl:value-of select="format-number(floor($seconds mod 86400 div 3600), '00')" />
  </xsl:variable>
  <xsl:variable name="minute">
    <xsl:value-of select="format-number(floor($seconds mod 3600 div 60), '00')" />
  </xsl:variable>
  <xsl:variable name="second">
    <xsl:value-of select="format-number(floor($seconds mod 60), '00')" />
  </xsl:variable>

  <xsl:value-of select="concat($hour, ':', $minute, ':', $second)" />
</xsl:template-->

<xsl:template name="get-monthstring">
  <xsl:param name="date" />
  <!--xsl:param name="seconds" /-->

  <xsl:variable name="month">
    <xsl:value-of select="date:month-in-year($date)"/>
    <!--xsl:call-template name="get-month">
      <xsl:with-param name="seconds" select="$seconds" />
    </xsl:call-template-->
  </xsl:variable>

  <xsl:choose>
    <xsl:when test="$month = 1">Januar</xsl:when>
    <xsl:when test="$month = 2">Februar</xsl:when>
    <xsl:when test="$month = 3">März</xsl:when>
    <xsl:when test="$month = 4">April</xsl:when>
    <xsl:when test="$month = 5">Mai</xsl:when>
    <xsl:when test="$month = 6">Juni</xsl:when>
    <xsl:when test="$month = 7">Juli</xsl:when>
    <xsl:when test="$month = 8">August</xsl:when>
    <xsl:when test="$month = 9">September</xsl:when>
    <xsl:when test="$month = 10">Oktober</xsl:when>
    <xsl:when test="$month = 11">November</xsl:when>
    <xsl:when test="$month = 12">Dezember</xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="get-datestring">
   <xsl:param name="date" />

    <!-- Dienstag, 24. Mai 2006 um 00:23:05 -->
    <xsl:call-template name="get-dayofweekstring">
      <xsl:with-param name="date" select="$date"/>
    </xsl:call-template>
    <xsl:text>, </xsl:text>

    <xsl:value-of select="date:day-in-month($date)"/>
    <xsl:text>. </xsl:text>

    <xsl:call-template name="get-monthstring">
      <xsl:with-param name="date" select="$date"/>
    </xsl:call-template>
    <xsl:text> </xsl:text>

    <xsl:value-of select="date:year($date)"/>


    <!-- Uhrzeit optional, im Timestamp einfach alles ab T... weglassen -->
    <xsl:if test="date:hour-in-day($date) or date:minute-in-hour($date)">
      <xsl:text> um </xsl:text>
      <xsl:call-template name="get-timestring">
	<xsl:with-param name="date" select="$date"/>
      </xsl:call-template>
    </xsl:if>

</xsl:template>

<xsl:template name="get-timestring">
  <xsl:param name="date"/>

  <xsl:variable name="hour_in_day">
    <xsl:if test="string-length(date:hour-in-day($date))=1">0</xsl:if>
    <xsl:value-of select="date:hour-in-day($date)"/>
  </xsl:variable>
  <xsl:variable name="minute_in_hour">
    <xsl:if test="string-length(date:minute-in-hour($date))=1">0</xsl:if>
    <xsl:value-of select="date:minute-in-hour($date)"/>
  </xsl:variable>
  <xsl:variable name="second_in_minute">
    <xsl:if test="string-length(date:second-in-minute($date))=1">0</xsl:if>
    <xsl:value-of select="date:second-in-minute($date)"/>
  </xsl:variable>

  <xsl:value-of select="$hour_in_day"/>:<xsl:value-of select="$minute_in_hour"/>
  <xsl:text> Uhr</xsl:text>
</xsl:template>

<xsl:template name="get-short-datestring">
  <xsl:param name="date"/>

  <!-- 26.12.2009 -->
  <xsl:value-of select="format-number(date:day-in-month($date), '00')"/>
  <xsl:text>.</xsl:text>

  <xsl:value-of select="format-number(date:month-in-year($date), '00')"/>
  <xsl:text>.</xsl:text>

  <xsl:value-of select="format-number(date:year($date), '00')"/>
</xsl:template>

<xsl:template name="datespan-to-human">
  <xsl:param name="start"/>
  <xsl:param name="end"/>
  <xsl:variable name="s" select="concat(date:day-in-month($start),'.',date:month-in-year($start),'.',date:year($start))"/>
  <xsl:variable name="e" select="concat(date:day-in-month($end),'.',date:month-in-year($end),'.',date:year($end))"/>
  <xsl:choose>
    <!-- One day only, no span: s.s.ssss -->
    <xsl:when test="$s = $e"><xsl:value-of select="$s"/></xsl:when>
    <!-- Same year, same month: s.-e.e.eeee. -->
    <xsl:when test="(date:month-in-year($start) = date:month-in-year($end)) and (date:year($start) = date:year($end))"><xsl:value-of select="concat(date:day-in-month($start),'.-',$e)"/></xsl:when>
    <!-- Same year, different months: s.s.-e.e.eeee -->
    <xsl:when test="(date:year($start) = date:year($end))"><xsl:value-of select="concat(date:day-in-month($start),'.',date:month-in-year($start),'.-',$e)"/></xsl:when>
    <!-- Across years: s.s.ssss-e.e.eeee -->
    <xsl:otherwise><xsl:value-of select="concat($s,'-',$e)"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="date-to-hevent">
  <xsl:param name="date"/>

  <xsl:variable name="month_in_year">
    <xsl:if test="string-length(date:month-in-year($date))=1">0</xsl:if>
    <xsl:value-of select="date:month-in-year($date)"/>
  </xsl:variable>
  <xsl:variable name="day_in_month">
    <xsl:if test="string-length(date:day-in-month($date))=1">0</xsl:if>
    <xsl:value-of select="date:day-in-month($date)"/>
  </xsl:variable>
  <xsl:variable name="hour_in_day">
    <xsl:if test="string-length(date:hour-in-day($date))=1">0</xsl:if>
    <xsl:value-of select="date:hour-in-day($date)"/>
  </xsl:variable>
  <xsl:variable name="minute_in_hour">
    <xsl:if test="string-length(date:minute-in-hour($date))=1">0</xsl:if>
    <xsl:value-of select="date:minute-in-hour($date)"/>
  </xsl:variable>
  <xsl:variable name="second_in_minute">
    <xsl:if test="string-length(date:second-in-minute($date))=1">0</xsl:if>
    <xsl:value-of select="date:second-in-minute($date)"/>
  </xsl:variable>

  <xsl:value-of select="date:year($date)"/><xsl:value-of select="$month_in_year"/><xsl:value-of select="$day_in_month"/><xsl:if test="date:hour-in-day($date)">T<xsl:value-of select="$hour_in_day"/><xsl:value-of select="$minute_in_hour"/><xsl:value-of select="$second_in_minute"/></xsl:if>
</xsl:template>

<!-- Geklaut von:
      http://www.trachtenberg.com/blog/2005/03/03/xslt-cookbook-generating-an-rfc-822-date/
  -->
<xsl:template name="date-to-rfc822">
  <xsl:param name="date"/>
  <xsl:param name="tz"/>
	<xsl:variable name="timezone">
		<xsl:choose>
			<xsl:when test="$tz != ''"><xsl:copy-of select="$tz"/></xsl:when>
			<xsl:otherwise>GMT</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

  <xsl:value-of select="concat(date:day-abbreviation($date), ', ',
  format-number(date:day-in-month($date), '00'), ' ',
  date:month-abbreviation($date), ' ', date:year($date), ' ',
  format-number(date:hour-in-day($date), '00'), ':',
  format-number(date:minute-in-hour($date), '00'), ':',
  format-number(date:second-in-minute($date), '00'), ' ', $timezone)"/>
</xsl:template>

<xsl:template name="strip-time-from-date">
  <xsl:param name="date"/>

  <xsl:choose>
    <xsl:when test="contains($date,'T')"><xsl:value-of select="substring-before($date,'T')"/></xsl:when>
    <xsl:otherwise test="contains($date,'T')"><xsl:value-of select="$date"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="validate-dtg">
  <xsl:param name="date" />
  <xsl:param name="label" />

  <xsl:if test="string-length($date) &gt; 0 and ((string-length($date) &gt; 10 and string-length($date) &lt; 19) or string(date:year($date)) = 'NaN' or string(date:month-in-year($date)) = 'NaN' or string(date:day-in-month($date)) = 'NaN' or string(date:time($date)) = 'NaN')">
    <xsl:message terminate="yes">
  Error: at date-time-group "<xsl:value-of select="$label"/>": 
  Value: "<xsl:value-of select="$date"/>"
  in Node: "<xsl:value-of select="local-name()"/>"

  Parent: <xsl:value-of select="name(..)"/>
  Title: <xsl:value-of select="@title"/>
  Year: <xsl:value-of select="date:year($date)"/>
  Month: <xsl:value-of select="date:month-in-year($date)"/>
  Day: <xsl:value-of select="date:day-in-month($date)"/>
  Time: <xsl:value-of select="date:time($date)"/>
  
  Hint: Compare format to output of command: `date +"%Y-%m-%dT%H:%M:%S"`
        The culprit likely is a typo.
    </xsl:message>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
