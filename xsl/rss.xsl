<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:content="http://purl.org/rss/1.0/modules/content/" 
  xmlns:atom="http://www.w3.org/2005/Atom"
  exclude-result-prefixes="xsl date">
  
  <xsl:include href="common.xsl" />

  <xsl:output method="xml"
              version="1.0"
              encoding="utf-8"
              indent="yes"/>

  <xsl:variable name="baseurl">http://www.c3d2.de/</xsl:variable>
  <xsl:param name="archivefeed"/>

  <xsl:template match="news">
    <xsl:element name="rss">
      <xsl:attribute name="version">2.0</xsl:attribute>
      <!--xsl:attribute name="xml:lang">de</xsl:attribute-->

      <channel>
        <title>www.c3d2.de Newsfeed</title>
        <link>http://www.c3d2.de/</link>
        <description>Neues aus dem Chaos Computer Club Dresden</description>
        <language>de-de</language>
	<image>
	  <title>Chaosknoten</title>
	  <url><xsl:value-of select="$baseurl"/>images/ck.png</url>
	  <link>http://www.c3d2.de/</link>
	</image>
	<xsl:if test="$archivefeed">
	  <atom:link rel="prev-archive" href="{$archivefeed}"/>
	</xsl:if>
        <!-- TODO: Insert "now": <pubDate><xsl:value-of select="news/item/@date"/></pubDate> -->
        <xsl:for-each select="newsfile">
          <xsl:sort select="date:seconds(document(.)/item/@date)" data-type="number" order="descending" />
          <xsl:variable name="newsfile" select="."/>
          <xsl:if test="(position() &lt;= $maxitems) and
                        starts-with(substring-after(substring-after($newsfile, '/'), '/'), $prefix) and
                        starts-with(date:difference(date:date-time(), concat(document(.)/item/@date, '+02:00')), '-')">
            <xsl:apply-templates select="document(.)/item">
              <xsl:with-param name="newsfile" select="$newsfile"/>
            </xsl:apply-templates>
            
          </xsl:if>
        </xsl:for-each>
      </channel>

    </xsl:element>
  </xsl:template>

  <xsl:template match="item">
    <xsl:param name="newsfile"/>
    <item>
      <xsl:variable name="href">
        <xsl:value-of select="$baseurl"/>
        <xsl:value-of select="substring-before(substring-after($newsfile, 'content/'), '.xml')"/>
        <xsl:text>.html</xsl:text>
      </xsl:variable>

      <title><xsl:value-of select="@title"/></title>
      <description>
	<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
	<div xmlns="http://www.w3.org/1999/xhtml">
	  <xsl:apply-templates select="document($newsfile)/item/*"/>
	</div>
	<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
      </description>
      <link><xsl:value-of select="$href"/></link>

      <author>mail@c3d2.de</author>

      <xsl:choose>
        <xsl:when test="starts-with($newsfile, 'content/news/ta-')">
          <category>Themenabend</category>
        </xsl:when>
        <xsl:when test="starts-with($newsfile, 'content/news/event-')">
          <category>Event</category>
        </xsl:when>
        <xsl:otherwise>
          <category>Misc</category>
        </xsl:otherwise>
      </xsl:choose>

      <!--
        TODO:
        Jemand mit iTunes (erhältlich für OSX und Windows)
        muss mal die itunes-extensions einbauen:
        http://www.apple.com/itunes/podcasts/techspecs.html
        -->
      <xsl:for-each select=".//*[name() = 'resource' or name() = 'alternative']">
        <xsl:variable name="resource_href">
          <xsl:call-template name="make-href">
            <xsl:with-param name="href" select="@url"/>
          </xsl:call-template>
        </xsl:variable>

        <enclosure url="{$resource_href}"
                   length="{@size}"
                   type="{@type}" />
      </xsl:for-each>

      <guid isPermaLink="true"><xsl:value-of select="$href"/></guid>

      <pubDate>
        <xsl:call-template name="date-to-rfc822">
          <xsl:with-param name="date" select="@date"/>
        </xsl:call-template>
      </pubDate>
      <content:encoded>
	<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
	<div xmlns="http://www.w3.org/1999/xhtml">
	  <xsl:apply-templates select="document($newsfile)/item/*"/>
	</div>
	<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
      </content:encoded>

      <xsl:copy-of select="resource/sc:chapters"
		   xmlns:sc="http://podlove.de/simple-chapters"/>

    </item>
  </xsl:template>

</xsl:stylesheet>
