<?xml version="1.0" encoding="utf-8"?>

<!--
    See: http://tools.ietf.org/html/draft-royer-calsch-xcal-03
  -->

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:str="http://exslt.org/strings"
                exclude-result-prefixes="xsl str date">

<xsl:output method="xml"
            version="1.0"
            encoding="utf-8"
            doctype-public="-//IETF//DTD XCAL//iCalendar XML//EN"
            doctype-system="DTD/xcal.dtd"
            indent="yes"/>

  <xsl:include href="common.xsl"/>

  <xsl:variable name="baseurl">https://www.c3d2.de/</xsl:variable>

  <!-- A week ago -->
  <xsl:variable name="minstart" select="date:add(date:date-time(), '-P7D')"/>

  <!--xsl:template match="/page">
    <xsl:apply-templates/>
  </xsl:template-->

  <xsl:template match="calendar-summary">
    <!-- xCal namespace specified in standard but nowhere used
         except for inclusion in other XML documents
      -->
    <iCalendar xmlns:xCal="urn:ietf:params:xml:ns:xcal">
      <vcalendar method="PUBLISH" version="2.0" prodid="-//C3D2-Web//event iCal-Export//DE">

        <xsl:for-each select="event[starts-with(date:difference(start, $minstart), '-')]">
          <xsl:sort select="date:seconds(start)" data-type="number" order="descending" />

          <vevent>
            <xsl:call-template name="uid"/>
            <xsl:apply-templates select="start"/>
            <xsl:apply-templates select="end"/>
            <xsl:apply-templates select="title"/>
            <xsl:apply-templates select="link"/>
            <xsl:apply-templates select="location"/>
            <xsl:call-template name="categories"/>
          </vevent>
        </xsl:for-each>

      </vcalendar>
    </iCalendar>
  </xsl:template>

  <xsl:template name="uid">
    <uid>https://www.c3d2.de/#<xsl:value-of select="str:encode-uri(concat(title,start),false())"/></uid>
  </xsl:template>
    
  <xsl:template match="start">
    <dtstart>
      <xsl:call-template name="date-to-hevent">
        <xsl:with-param name="date" select="string(.)"/>
      </xsl:call-template>
    </dtstart>
  </xsl:template>
    
  <xsl:template match="end">
    <!-- Hack? -->
    <xsl:if test="not(string(.) = string(../start))">
      <dtend>
        <xsl:call-template name="date-to-hevent">
	  <xsl:with-param name="date">
	    <xsl:choose>
	      <xsl:when test="contains(., 'T')">
		<xsl:value-of select="string(.)"/>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:value-of select="date:add(., 'P1D')"/>
	      </xsl:otherwise>
	    </xsl:choose>
	  </xsl:with-param>
        </xsl:call-template>
      </dtend>
    </xsl:if>
  </xsl:template>
    
  <xsl:template match="title">
    <summary><xsl:value-of select="."/></summary>
  </xsl:template>
    
  <xsl:template match="link">
    <url>
      <xsl:call-template name="make-href">
        <xsl:with-param name="href" select="string(.)"/>
      </xsl:call-template>
    </url>
  </xsl:template>

  <xsl:template match="location">
    <location><xsl:value-of select="."/></location>
  </xsl:template>
    
  <xsl:template name="categories">
    <categories>
      <xsl:choose>
        <xsl:when test="starts-with(link, 'news/ta-')">
          <item>Themenabend</item>
        </xsl:when>
        <xsl:when test="starts-with(link, 'news/event-')">
          <item>Event</item>
        </xsl:when>
      <xsl:when test="starts-with(link, 'news/pentaradio')">
        <item>Radio</item>
      </xsl:when>
      <xsl:when test="string(link) = 'radio.html'">
        <item>Radio</item>
      </xsl:when>
        <xsl:otherwise>
          <item>Misc</item>
        </xsl:otherwise>
      </xsl:choose>
    </categories>
  </xsl:template>
    
</xsl:stylesheet>
