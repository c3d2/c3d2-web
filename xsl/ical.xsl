<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:date="http://exslt.org/dates-and-times"
                xmlns:str="http://exslt.org/strings">

<xsl:output method="text" encoding="utf-8"/>

  <xsl:include href="common.xsl"/>

  <xsl:param name="location-filter"/>
  <xsl:variable name="baseurl">https://www.c3d2.de/</xsl:variable>

  <!-- A week ago -->
  <xsl:variable name="minstart" select="date:add(date:date-time(), '-P7D')"/>

  <xsl:template match="/page">
    <xsl:apply-templates select="calendar-summary"/>
  </xsl:template>

  <xsl:template match="calendar-summary">
    <xsl:text>BEGIN:VCALENDAR</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
    
    <xsl:text>VERSION:2.0</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>PRODID:-//C3D2-Web//event iCal-Export//DE</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>CALSCALE:GREGORIAN</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>METHOD:PUBLISH</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>X-WR-CALDESC;VALUE=TEXT:Events und Themenabende beim CCC Dresden</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>X-WR-CALNAME;VALUE=TEXT:C3D2-Events</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>X-WR-TIMEZONE;VALUE=TEXT:Europe/Prague</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:for-each select="event[starts-with(date:difference(start, $minstart), '-')]">
      <xsl:sort select="date:seconds(@start)" data-type="number" order="descending" />

      <xsl:if test="contains(location, $location-filter)">
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:text>BEGIN:VEVENT</xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:text>METHOD:PUBLISH</xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>

        <xsl:call-template name="uid"/>
        <xsl:apply-templates select="start"/>
        <xsl:apply-templates select="end"/>
        <xsl:apply-templates select="title"/>
        <xsl:text>CLASS:PUBLIC</xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
        <xsl:call-template name="category"/>
        <xsl:apply-templates select="link"/>
        <xsl:apply-templates select="location"/>

        <xsl:text>END:VEVENT</xsl:text>
        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:if>
    </xsl:for-each>

    <xsl:text>&#13;&#10;</xsl:text>
    <xsl:text>END:VCALENDAR</xsl:text>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <xsl:template name="uid">
    <xsl:text>UID:https://www.c3d2.de/#</xsl:text><xsl:value-of select="str:encode-uri(concat(title,start),false())"/>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="start">
    <xsl:choose>
      <xsl:when test="contains(., 'T')">
        <xsl:text>DTSTART:</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>DTSTART;VALUE=DATE:</xsl:text>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:call-template name="date-to-hevent">
      <xsl:with-param name="date" select="string(.)"/>
    </xsl:call-template>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="end">
    <xsl:choose>
      <xsl:when test="not(string(.) = string(../start))">

        <xsl:variable name="date">
          <xsl:call-template name="date-to-hevent">
            <xsl:with-param name="date">
	      <xsl:choose>
	        <xsl:when test="contains(., 'T')">
	          <xsl:value-of select="string(.)"/>
                </xsl:when>

                <xsl:otherwise>
	          <xsl:value-of select="date:add(., 'P1D')"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:variable>

	<xsl:choose>
	  <xsl:when test="contains($date, 'T')">
            <xsl:text>DTEND:</xsl:text>
	    <xsl:value-of select="$date"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>DTEND;VALUE=DATE:</xsl:text>
	    <xsl:value-of select="$date"/>
	  </xsl:otherwise>
        </xsl:choose>

        <xsl:text>&#13;&#10;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <!-- Fallback duration: two hours -->
        <xsl:text>DURATION:PT2H&#13;&#10;</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="title">
    <xsl:call-template name="wrap-text">
      <xsl:with-param name="text">SUMMARY:<xsl:value-of select="."/></xsl:with-param>
      <xsl:with-param name="length" select="70"/>
    </xsl:call-template>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <xsl:template name="category">
    <xsl:text>CATEGORIES:</xsl:text>
    <xsl:choose>
      <xsl:when test="starts-with(link, 'news/ta-')">
        <xsl:text>Themenabend</xsl:text>
      </xsl:when>
      <xsl:when test="starts-with(link, 'news/event-')">
        <xsl:text>Event</xsl:text>
      </xsl:when>
      <xsl:when test="starts-with(link, 'news/pentaradio')">
        <xsl:text>Radio</xsl:text>
      </xsl:when>
      <xsl:when test="link = 'radio.html'">
        <xsl:text>Radio</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>Misc</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="link">
    <xsl:variable name="url">
      <xsl:call-template name="make-href">
        <xsl:with-param name="href" select="string(.)"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:text>URL:</xsl:text>
    <xsl:value-of select="$url"/>
    <xsl:text>&#13;&#10;</xsl:text>

    <xsl:text>DESCRIPTION:&lt;</xsl:text>
    <xsl:value-of select="$url"/>
    <xsl:text>&gt;&#13;&#10;</xsl:text>
  </xsl:template>

  <xsl:template match="location">
    <xsl:call-template name="wrap-text">
      <xsl:with-param name="text">LOCATION:<xsl:value-of select="string(.)"/></xsl:with-param>
      <xsl:with-param name="length" select="70"/>
    </xsl:call-template>
    <xsl:text>&#13;&#10;</xsl:text>
  </xsl:template>

  <!-- Copy left by Tim Pritlove -->
  <xsl:template name="wrap-text">
    <xsl:param name="text" />
    <xsl:param name="length" />

    <xsl:choose>
      <xsl:when test="string-length($text) &gt; $length">
        <xsl:value-of select="substring($text, 0, $length)" />
        <xsl:text>&#13;&#10; </xsl:text>
        <xsl:call-template name="wrap-text">
          <xsl:with-param name="text" select="substring($text, $length)" />
          <xsl:with-param name="length" select="$length" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
