<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:rss="http://purl.org/rss/1.0/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:date="http://exslt.org/dates-and-times"
  exclude-result-prefixes="xsl rss rdf">

  <xsl:template name="footer">
    <footer id="footer">
      <!-- ### Footer ### -->
      <xsl:comment>
	<xsl:text> ### Footer ### </xsl:text>
      </xsl:comment>
      <ul>
	<li>
	  <!-- Creative Commons License -->
	  <xsl:comment>
	    <xsl:text> Creative Commons License </xsl:text>
	  </xsl:comment>
	  <a rel="license" href="https://creativecommons.org/licenses/by-sa/2.0/de/deed.de">
	    <img src="{$baseurl}images/footer/cc-somerights20.png"
		 width="88" height="31"
		 alt="Creative Commons License"
		 title="Creative Commons Attribution-ShareAlike 2.0 Germany License"
		 loading="lazy"/>
	  </a>
	  <xsl:comment>
	    <xsl:text>
	      &lt;rdf:RDF xmlns="http://web.resource.org/cc/"
	      xmlns:dc="http://purl.org/dc/elements/1.1/"
	      xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"&gt;
	      &lt;Work rdf:about=""&gt;
              &lt;dc:type rdf:resource="http://purl.org/dc/dcmitype/Interactive" /&gt;
              &lt;license rdf:resource="http://creativecommons.org/licenses/by-sa/2.0/de/" /&gt;
	      &lt;/Work&gt;

	      &lt;License rdf:about="http://creativecommons.org/licenses/by-sa/2.0/de/"&gt;
              &lt;permits rdf:resource="http://web.resource.org/cc/Reproduction" /&gt;
              &lt;permits rdf:resource="http://web.resource.org/cc/Distribution" /&gt;
              &lt;requires rdf:resource="http://web.resource.org/cc/Notice" /&gt;
              &lt;requires rdf:resource="http://web.resource.org/cc/Attribution" /&gt;
              &lt;permits rdf:resource="http://web.resource.org/cc/DerivativeWorks" /&gt;
              &lt;requires rdf:resource="http://web.resource.org/cc/ShareAlike" /&gt;
	      &lt;/License&gt;
	      &lt;/rdf:RDF&gt;
	    </xsl:text>
	  </xsl:comment>
	</li>
	<li>
          <p>
            <a href="https://www.ccc.de/de/updates/2005/unvereinbarkeitserklaerung">Unvereinbarkeitserklärung</a>
            <a href="{$baseurl}impressum.html">Impressum</a>
            <a href="{$baseurl}datenschutz.html">Datenschutz</a>
          </p>
	  <p id="last-built"><xsl:value-of select="concat('built ', translate(substring(date:date-time(), 0, 20), 'T', ' '))" /></p>
        </li>
	<li>
	  <a href="https://www.w3.org/html/logo/">
	    <img src="{$baseurl}images/footer/html5-badge-h-multimedia-semantics.png"
		 width="83" height="32"
		 alt="HTML5 Powered with Multimedia, and Semantics"
		 title="HTML5 Powered with Multimedia, and Semantics"
		 loading="lazy"/>
	    </a>
	</li>
      </ul>

      <small class="hidden">
	Cover, Concealment, Camouflage, Denial and Deception
      </small>
    </footer>
  </xsl:template>

</xsl:stylesheet>
