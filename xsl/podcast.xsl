<?xml version="1.0" encoding="utf-8"?>

<!-- Spezifikationen:
     http://www.apple.com/itunes/store/podcaststechspecs.html
     XMLNS podcast: https://github.com/Podcastindex-org/podcast-namespace/blob/main/docs/1.0.md
 -->

<xsl:stylesheet version="1.0"
  xmlns:itunes="https://www.itunes.com/dtds/podcast-1.0.dtd"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns:content="https://purl.org/rss/1.0/modules/content/"
  xmlns:atom="https://www.w3.org/2005/Atom"
  xmlns:podcast="https://podcastindex.org/namespace/1.0"
  exclude-result-prefixes="xsl date">

  <xsl:output method="xml"
              version="1.0"
              encoding="utf-8"
              indent="yes"/>

  <xsl:include href="common.xsl" />

  <xsl:variable name="baseurl">https://www.c3d2.de/</xsl:variable>
  <xsl:variable name="publisher">C3D2</xsl:variable>
  <xsl:variable name="mailto">pentaradio@c3d2.de</xsl:variable>
  <xsl:param name="selection" select="''"/>
  <xsl:param name="format"/>
  <xsl:param name="archivefeed"/>


  <xsl:template match="news">
  <!--xsl:text disable-output-escaping="yes">&lt;?xml-stylesheet type="text/css" href="https://c3d2.de/style/rss.css"?&gt;</xsl:text-->
    <rss version="2.0">

      <channel>

		<xsl:variable name="podtitle">
			<xsl:choose>
			  <xsl:when test="$selection = 'pentamedia'">Penta*</xsl:when>
			  <xsl:when test="$selection = 'pentaradio'">Pentaradio</xsl:when>
			  <xsl:when test="$selection = 'pentacast'">Pentacast</xsl:when>
			  <xsl:when test="$selection = 'pentamusic'">Pentamusic</xsl:when>
			  <xsl:otherwise>C3D2 Podcast</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="podurl">
			<xsl:choose>
			  <xsl:when test="$selection = 'pentamedia'"><xsl:value-of select="$baseurl"/></xsl:when>
			  <xsl:when test="$selection = 'pentaradio'"><xsl:value-of select="$baseurl"/>radio.html</xsl:when>
			  <xsl:when test="$selection = 'pentacast'"><xsl:value-of select="$baseurl"/>podcast.html</xsl:when>
			  <xsl:when test="$selection = 'pentamusic'"><xsl:value-of select="$baseurl"/>pentamusic.html</xsl:when>
			  <xsl:otherwise><xsl:value-of select="$baseurl"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="poddesc">
			<xsl:choose>
			  <xsl:when test="$selection = 'pentamedia'">Pentaradio, Pentacast, Pentamusic</xsl:when>
			  <xsl:when test="$selection = 'pentaradio'">C3D2 Pentaradio</xsl:when>
			  <xsl:when test="$selection = 'pentacast'">C3D2 Pentacast</xsl:when>
			  <xsl:when test="$selection = 'pentamusic'">C3D2 Pentamusic</xsl:when>
			  <xsl:otherwise>C3D2 Podcast</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="imgtitle">
			<xsl:choose>
			  <xsl:when test="$selection = 'pentamedia' or $selection = 'pentaradio'">Pentaradio</xsl:when>
			  <xsl:when test="$selection = 'pentacast'">Pentacast</xsl:when>
			  <xsl:when test="$selection = 'pentamusic'">Pentamusic</xsl:when>
			  <xsl:otherwise>C3D2</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="imgurl">
			<xsl:choose>
			  <xsl:when test="$selection = 'pentamedia' or $selection = 'pentaradio'"><xsl:value-of select="$baseurl"/>images/pentaradio-artwork.png</xsl:when>
			  <xsl:when test="$selection = 'pentacast'"><xsl:value-of select="$baseurl"/>images/pentacast-artwork.png</xsl:when>
			  <xsl:when test="$selection = 'pentamusic'"><xsl:value-of select="$baseurl"/>images/pentamusic-quadratic.png</xsl:when>
			  <xsl:otherwise><xsl:value-of select="$baseurl"/>images/ck.png</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="imglink">
			<xsl:choose>
			  <xsl:when test="$selection = 'pentamedia' or $selection = 'pentaradio'"><xsl:value-of select="$baseurl"/>radio.html</xsl:when>
			  <xsl:when test="$selection = 'pentacast'"><xsl:value-of select="$baseurl"/>podcast.html</xsl:when>
			  <xsl:when test="$selection = 'pentamusic'"><xsl:value-of select="$baseurl"/>pentamusic.html</xsl:when>
			  <xsl:otherwise><xsl:value-of select="$baseurl"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="podsubtitle">
			<xsl:choose>
			  <xsl:when test="$selection = 'pentamedia'">Die drei Podcasts vom CCC Dresden</xsl:when>
			  <xsl:when test="$selection = 'pentaradio'">Radio vom CCC Dresden</xsl:when>
			  <xsl:when test="$selection = 'pentacast'">Podcasts vom CCC Dresden</xsl:when>
			  <xsl:when test="$selection = 'pentamusic'">Musikvorstellungen vom CCC Dresden</xsl:when>
			  <xsl:otherwise>Radio &amp; Podcasts vom CCC Dresden</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="podsummary">
			<xsl:choose>
			  <xsl:when test="$selection = 'pentamedia'">Enthält Podcasts von Pentaradio, Pentacast und Pentamusic</xsl:when>
			  <xsl:when test="$selection = 'pentaradio'">Pentaradio sind die Sendungen des Chaos Computer Club Dresden auf coloRadio, bestehend aus einem informativen Rahmenprogramm und Beiträgen zu einem sendungsspezifischen Thema.</xsl:when>
			  <xsl:when test="$selection = 'pentacast'">Pentacast sind themenspezifische Sendungen des Chaos Computer Club Dresden.</xsl:when>
			  <xsl:when test="$selection = 'pentamusic'">Versuch, Künstler, Bands und Labels der CC-Szene vor's Mikro zu holen um diese einer breiteren Öffentlichkeit bekannt zu machen.</xsl:when>
			  <xsl:otherwise>Pentaradio, Pentacast &amp; Pentamusic vom Chaos Computer Club Dresden.</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="ctech">
			<xsl:choose>
			  <xsl:when test="$selection = 'pentamedia' or $selection = 'pentaradio'"><itunes:category text="Technology"><itunes:category text="Tech News"/></itunes:category></xsl:when>
			  <xsl:when test="$selection = 'pentacast'"><itunes:category text="Technology"/></xsl:when>
			  <xsl:when test="$selection = 'pentamusic'"><itunes:category text="Music"/></xsl:when>
			  <xsl:otherwise><itunes:category text="Technology"/></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<title><xsl:value-of select="$podtitle"/></title>
		<link><xsl:value-of select="$podurl"/></link>
		<description><xsl:value-of select="$poddesc"/></description>
		<language>de-DE</language>
		<copyright>https://creativecommons.org/licenses/by-sa/2.0/de/</copyright>
		<lastBuildDate><xsl:call-template name="date-to-rfc822"><xsl:with-param name="date" select="date:date-time()"/><!--xsl:with-param name="tz">+0200</xsl:with-param--></xsl:call-template></lastBuildDate>
	    <image>
	      <title><xsl:value-of select="$imgtitle"/></title>
	      <url><xsl:value-of select="$imgurl"/></url>
	      <link><xsl:value-of select="$imglink"/></link>
	    </image>
		<itunes:subtitle><xsl:value-of select="$podsubtitle"/></itunes:subtitle>
		<itunes:summary><xsl:value-of select="$podsummary"/></itunes:summary>
		<itunes:image>
			<xsl:attribute name="href"><xsl:value-of select="$imgurl"/></xsl:attribute>
		</itunes:image>

		<xsl:copy-of select="$ctech"/>
		<itunes:category text="Society &amp; Culture"/>

        <itunes:author>Chaos Computer Club Dresden</itunes:author>
        <itunes:owner>
          <itunes:name><xsl:value-of select="$publisher"/></itunes:name>
          <itunes:email><xsl:value-of select="$mailto"/></itunes:email>
        </itunes:owner>
        <itunes:explicit>no</itunes:explicit>

        <!--generator>C3D2 custom XSLT</generator-->
        <podcast:locked owner="{$mailto}">no</podcast:locked>
        <podcast:location geo="geo:51.08106,13.72867">C3D2, Dresden, Germany</podcast:location>
        <podcast:license url="https://creativecommons.org/licenses/by-sa/2.0/de/">CC BY-SA 2.0 DE</podcast:license>

		<xsl:choose>
		  <xsl:when test="not(substring($selection,1,4) = 'penta')"><!-- Für Dinge außerhalb der Vereins-"Marke" --></xsl:when>
		  <xsl:otherwise><!-- alles penta -->
			<podcast:funding url="https://c3d2.de/unterstuetzen.html">C3D2 Unterstützen!</podcast:funding>
			<!-- spec erlaubt mehrere tags, die catcher sind sich aber nicht einig welchen sie anzeigen und sie zeigen derzeit nur einen
			<podcast:funding url="https://c3d2.de/membership.html">Mitglied werden!</podcast:funding>
			-->
		  </xsl:otherwise>
		</xsl:choose>

		<!--podcast:value type="lightning" method="keysend" suggested="0.00000005000">
		<podcast:valueRecipient
		name="[name of recipient(string)]"
		type="[address type(string)]"
		address="[the receiving address(string)]"
		customKey="[optional key to pass(mixed)]"
		customValue="[optional value to pass(mixed)]"
		split="[share count(int)]"
		fee=[true|false]
		/>
		</podcast:value-->
		<xsl:choose>
		  <!-- https://c3d2.de/pentaradio.xml https://www.c3d2.de/pentaradio.xml -->
		  <xsl:when test="$selection = 'pentaradio'">
			<!-- not supported yet, see https://github.com/Podcastindex-org/podcast-namespace/issues/677
			<podcast:value type="lightning" method="lnaddress" suggested="0.00000005000"> 
				<podcast:valueRecipient name="Pentaradio LN Address" type="lnaddress" address="pentaradio@c3d2.de" /> 
			</podcast:value>
			-->
			<podcast:value type="lightning" method="keysend" suggested="0.00000005000"> 
				<!-- getalby disabled podcast:valueRecipient name="Pentaradio (on Alby)" type="node" address="030a58b8653d32b99200a2334cfe913e51dc7d155aa0116c176657a4f1722677a3" customKey="696969" customValue="sCxNM2WUe4R6GxCjLWKG" split="95" /--> 
				<podcast:valueRecipient name="pentaradio@fountain.fm" type="node" address="03b6f613e88bd874177c28c6ad83b3baba43c4c656f56be1f8df84669556054b79" split="95" fee="false" customKey="906608" customValue="01ML49KW10vNxbAfyOm2Li" />
				<podcast:valueRecipient name="Podcastindex.org (Donation)" type="node" address="03ae9f91a0cb8ff43840e3c322c4c61f019d8c1c3cea15a25cfc425ac605e61a4a" fee="true" split="5" /> 
			</podcast:value>
		  </xsl:when>

		  <!-- https://c3d2.de/pentamedia.xml -->
<!--
		  <xsl:when test="$selection = 'pentamedia'">
		  </xsl:when>
-->

		  <!-- https://c3d2.de/pentacast.xml -->
<!--
		  <xsl:when test="$selection = 'pentacast'">
		  </xsl:when>
-->

		  <!-- https://c3d2.de/pentamusic.xml -->
<!--
		  <xsl:when test="$selection = 'pentamusic'">
		  </xsl:when>
-->

		  <!-- Fallback für alle oberhalb nicht spezifisch angesprochenen, insb. auch https://c3d2.de/podcast.xml -->
		  <xsl:otherwise>
			<podcast:value type="lightning" method="keysend" suggested="0.00000005000"> 
				<!-- getalby disabled podcast:valueRecipient name="Podcaster (on Alby)" type="node" address="030a58b8653d32b99200a2334cfe913e51dc7d155aa0116c176657a4f1722677a3" customKey="696969" customValue="sCxNM2WUe4R6GxCjLWKG" split="100" /-->
				<!-- fountain --><podcast:valueRecipient name="pentaradio@fountain.fm" type="node" address="03b6f613e88bd874177c28c6ad83b3baba43c4c656f56be1f8df84669556054b79" split="95" fee="false" customKey="906608" customValue="01cfNSbo7dYMYRc4ApXZh1" />
			</podcast:value>
		  </xsl:otherwise>
		</xsl:choose>

		<!-- experimental part -->
		<podcast:value type="bitcoin" method="default" suggested="0.00000001">
			<podcast:valueRecipient name="C3D2" type="bech32" address="BC1QANU4755ULAU2JJDQSJLC3YW2XDJAWJ8WMVF5LA" fee="true"/>
			<podcast:valueRecipient name="C3D2" type="p2sh" address="3AhYpTzozbxFSmL1NqdZqsFgZhKc95BsCP" fee="true"/>
			<podcast:valueRecipient name="C3D2" type="p2pkh" address="195jSW9hXHdhu5QEeHkhyAN6Kf2oFUkmyK" fee="true"/>
		</podcast:value>
		<podcast:value type="monero" method="default" suggested="0.000001">
			<podcast:valueRecipient name="C3D2" type="monero" address="45c4E14LpNbeTdL8FSuBQA7M3wJ5nxGnYfnEzv3hwasfZUuKwP5zAaDCgJfD3s9kT4GP7y678gi6b1cGSVSgFpsTTm6fPtz" fee="true"/>
		</podcast:value>
		<podcast:value type="wire" method="sepa" suggested="5">
			<podcast:valueRecipient name="C3D2" type="bezahlcode" address="bank://singlepaymentsepa?name=NETZBIOTOP%20DRESDEN%20E.V.&amp;reason=Spende&amp;iban=DE34850900004655221005&amp;bic=GENODEF1DRS&amp;amount="/>
		</podcast:value>

	<xsl:if test="$archivefeed">
	  <atom:link rel="prev-archive" href="{$archivefeed}"/>
	</xsl:if>

        <xsl:for-each select="newsfile">
          <xsl:sort select="date:seconds(document(.)/item/@date)" data-type="number" order="descending" />
	  <xsl:if test="position() &lt;= $maxitems and
                    starts-with(
                      date:difference(
                        date:date-time(),
                        concat(document(.)/item/@date, '+01:00')
                      ),
                      '-'
                    )">
	    <xsl:choose>
	      <xsl:when test="($selection = 'pentaradio' or $selection = 'pentamedia') and
			      starts-with(., 'content/news/pentaradio') and
			      count(document(.)/item//resource) &gt; 0">
		<xsl:apply-templates select="document(.)/item//resource" mode="preselect">
		  <xsl:with-param name="newsfile" select="."/>
		</xsl:apply-templates>
	      </xsl:when>
	      <xsl:when test="($selection = 'pentacast' or $selection = 'pentamedia') and
			      starts-with(., 'content/news/pentacast') and
			      count(document(.)/item//resource) &gt; 0">
		<xsl:apply-templates select="document(.)/item//resource" mode="preselect">
		  <xsl:with-param name="newsfile" select="."/>
		</xsl:apply-templates>
	      </xsl:when>
	      <xsl:when test="($selection = 'pentamusic' or $selection = 'pentamedia') and
			      starts-with(., 'content/news/pentamusic') and
			      count(document(.)/item//resource) &gt; 0">
		<xsl:apply-templates select="document(.)/item//resource" mode="preselect">
		  <xsl:with-param name="newsfile" select="."/>
		</xsl:apply-templates>
	      </xsl:when>
	      <xsl:when test="$selection = '' and count(document(.)/item//resource) &gt; 0">
		<xsl:apply-templates select="document(.)/item//resource" mode="preselect">
		  <xsl:with-param name="newsfile" select="."/>
		</xsl:apply-templates>
	      </xsl:when>
	    </xsl:choose>
	  </xsl:if>
        </xsl:for-each>
      </channel>

    </rss>
  </xsl:template>


  <!-- Preselect heißt:
       * Gucken, ob es in der Resource nicht ein Alternative gibt, welches $format ist
       * Dann dieses nehmen
       * Ansonsten die Resource nehmen
    -->
  <xsl:template match="resource" mode="preselect">
    <xsl:param name="newsfile"/>

    <xsl:choose>
      <!-- Gibts ein $format? -->
      <xsl:when test="count(alternative[contains($format, @type)]) &gt; 0">
        <xsl:apply-templates select="alternative[contains($format, @type)]">
          <xsl:with-param name="newsfile" select="$newsfile"/>
        </xsl:apply-templates>
      </xsl:when>
      <!-- Nimm was da ist -->
      <xsl:otherwise>
        <xsl:apply-templates select=".">
          <xsl:with-param name="newsfile" select="$newsfile"/>
        </xsl:apply-templates>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <xsl:template match="*[name() = 'resource' or name() = 'alternative']">
    <xsl:param name="newsfile"/>

    <xsl:variable name="href">
      <xsl:call-template name="make-href">
        <xsl:with-param name="href" select="@url"/>
      </xsl:call-template>
    </xsl:variable>

    <xsl:call-template name="generate-item">
      <xsl:with-param name="newsfile" select="$newsfile"/>
      <xsl:with-param name="item_title">
	<xsl:value-of select="document($newsfile)/item/@title"/>
	<xsl:if test="count(document($newsfile)//resource) &gt; 1 and @title">
	  <!-- Multiple resources in this item? -->
	  <xsl:text>: </xsl:text>
	  <xsl:value-of select="@title"/>
	</xsl:if>
      </xsl:with-param>
      <xsl:with-param name="resource_title">
	<xsl:choose>
	  <xsl:when test="@title"><xsl:value-of select="@title"/></xsl:when>
	  <xsl:otherwise><xsl:value-of select="../@title"/></xsl:otherwise>
	</xsl:choose>
      </xsl:with-param>
      <xsl:with-param name="date" select="document($newsfile)/item/@date"/>
      <xsl:with-param name="resource_url" select="$href"/>
      <xsl:with-param name="resource_size" select="@size"/>
      <xsl:with-param name="resource_type" select="@type"/>
      <xsl:with-param name="chapters" select="sc:chapters"
		      xmlns:sc="https://podlove.de/simple-chapters"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="generate-item">
    <xsl:param name="newsfile"/>
    <xsl:param name="item_title"/>
    <xsl:param name="resource_title"/>
    <xsl:param name="date"/>
    <xsl:param name="resource_url"/>
    <xsl:param name="resource_size"/>
    <xsl:param name="resource_type"/>
    <xsl:param name="chapters"/>

    <item>
      <xsl:variable name="href">
        <xsl:value-of select="$baseurl"/>
        <xsl:value-of select="substring-before(substring-after($newsfile, 'content/'), '.xml')"/>
        <xsl:text>.html</xsl:text>
      </xsl:variable>

      <title><xsl:value-of select="$item_title"/></title>
      <description>
		<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
		<div xmlns="http://www.w3.org/1999/xhtml">
		<xsl:apply-templates select="document($newsfile)/item/*[name() != 'resource']"/>
		</div>
		<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
      </description>
      <link><xsl:value-of select="$href"/></link>

      <enclosure
        url="{$resource_url}"
        length="{$resource_size}"
        type="{$resource_type}"
        />

      <guid isPermaLink="false"><xsl:value-of select="concat($href, '+++', $resource_url)"/></guid>

      <pubDate>
        <xsl:call-template name="date-to-rfc822">
          <xsl:with-param name="date" select="$date"/>
        </xsl:call-template>
      </pubDate>

      <content:encoded>
	<xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
	<div xmlns="http://www.w3.org/1999/xhtml">
	  <xsl:apply-templates select="document($newsfile)/item/*[name() != 'resource']"/>
	</div>
	<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
      </content:encoded>

      <!-- iTunes-Extensions zum Schluss, sonst validiert es nicht -->

      <itunes:subtitle><xsl:value-of select="$resource_title"/></itunes:subtitle>
      <itunes:author>CCC Dresden</itunes:author>

      <xsl:copy-of select="$chapters"/>

      <!-- XMLNS podcast – https://podcastindex.org/ -->

      <!--podcast:transcript url="concat('',$baseurl,'transcripts/',$resource_url,'.html')" type="text/html"/--><!-- better check for variable and include when not empty -->
      <!--podcast:season name="Egyptology: The 19th Century">1</podcast:season-->
      <!--podcast:episode display="Ch.3">204</podcast:episode-->
      <!--podcast:chapters url="https://example.com/episode1/chapters.json" type="application/json+chapters" /-->
      <!--podcast:soundbite startTime="73.0" duration="60.0">Song</podcast:soundbite-->
      <!--podcast:person href="https://example.com/johnsmith/blog" img="https://example.com/images/johnsmith.jpg">John Smith</podcast:person-->
      <!--podcast:value-->
    </item>
  </xsl:template>

  <xsl:template match="addendum">
    <xsl:apply-templates select="*[name() != 'resource']"/>
  </xsl:template>

</xsl:stylesheet>
