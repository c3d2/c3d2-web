# eingebaute Variablen und Regeln werden nicht gebraucht
MAKEFLAGS += -rR

### Programme ###
# XSLT-Prozessor:
PROCESSOR=xsltproc
PFLAGS=--novalid --catalogs --stringparam baseurl $(BASEURL)
BASEURL=""
RM=rm -f
# XML-Validator
VALIDATOR=xmllint
VFLAGS=--noout --nonet
# VALIDATOR-Flags für Eingabe-XML (meist c3d2web)
VFLAGS_IN=$(VFLAGS) --schema xsd/c3d2web.xsd
# VALIDATOR-Flags für Ausgabe-XML (xhtml, rss, atom, ...)
VFLAGS_OUT=$(VFLAGS)
SGML_CATALOG_FILES=$(XML_CATALOG)
export SGML_CATALOG_FILES
# Downloadmanager (Alternativ: curl, fetch):
WGET=wget
# rsync
RSYNC=rsync
# -l, --links                 copy symlinks as symlinks
RSYNC_FLAGS=--links --verbose --recursive --delete --delete-after --delete-excluded --checksum --chmod=ugo=rX,u+w

# Verzeichnis, in das exportiert werden soll
DESTDIR?=www-export

### Ressourcen ###
# XSLT-Stylesheets:
STYLE=xsl/xhtml5.xsl
DS_STYLE=xsl/datenspuren/xhtml5.xsl
DS_YEAR=2025
AUTOTOPIA_STYLE=xsl/autotopia/xhtml5.xsl
# Document-Type-Definition:
DTD:=dtd/c3d2web.dtd
# XML-Katalog (Pfad zu den DTDs) - wird benötigt vom Validator
XML_CATALOG:=dtd/catalog.xml
# Navigationsleiste:
NAVIGATION=content/navigation.xml
# Chaosupdates:
#WWW_CCC_DE_UPDATES=chaosupdates.xml
# Calendar summary::
CALENDAR_SUMMARY=calendar-summary.xml
#DS_SCHEDULE=build/datenspuren/$(DS_YEAR)/schedule.xml
DS_SCHEDULE=/dev/null

### Ziele ###
# Mindestens 1x täglich neubauen wegen Chaosupdates & Kalender
DATESTAMP=build/.stamp-$(shell date +%Y-%m-%d)

# Dateinamen der zu erzeugenden html-Dateien:
CONTENT=$(PAGES) $(NEWS_PAGES) $(DS_PAGES) $(DS_FEEDS) $(AUTOTOPIA_PAGES) $(NEWSFILES) build/calendar.html

# 'normale' Seiten:
PAGES:=$(patsubst content/pages/%.xml, build/%.html, $(wildcard content/pages/*.xml))
# Seiten zu einzelnen News-Items:
NEWS_PAGES:=$(patsubst content/news/%.xml, build/news/%.html, $(wildcard content/news/*.xml))
# News-Index und Feeds:
NEWSFILES:=build/news.html build/news-rss.xml build/news-atom.xml \
	build/podcast.xml build/pentaradio.xml build/pentacast.xml build/pentamusic.xml build/pentamedia.xml \
	build/news-archiv-rss.xml build/news-archiv-atom.xml \
	build/podcast-archiv.xml build/pentaradio-archiv.xml build/pentacast-archiv.xml \
	build/pentamusic-archiv.xml build/pentamedia-archiv.xml \
	build/ical.ics build/hq.ics build/xcal.xml build/reminders \
	build/datenspuren/$(DS_YEAR)/news-rss.xml build/datenspuren/$(DS_YEAR)/news-atom.xml \
	build/pentacast-ogg.xml build/pentacast-ogg-archiv.xml \
	build/portal.html
PLAYLISTS:=$(patsubst content/news/pentaradio%.xml, build/news/pentaradio%.xspf, $(wildcard content/news/pentaradio*.xml)) \
	$(patsubst content/news/pentaradio%.xml, build/news/pentaradio%.pls, $(wildcard content/news/pentaradio*.xml)) \
	$(patsubst content/news/pentaradio%.xml, build/news/pentaradio%.m3u, $(wildcard content/news/pentaradio*.xml))
# Datenspuren-Seiten:
DS_PAGES:=$(patsubst content/datenspuren/%.xml, build/datenspuren/$(DS_YEAR)/%.html, $(wildcard content/datenspuren/*.xml))
DS_FEEDS:= \
	build/datenspuren/2009/mitschnitte-rss.xml \
	build/datenspuren/2010/mitschnitte-rss.xml \
	build/datenspuren/2011/mitschnitte-rss.xml \
	build/datenspuren/2012/mitschnitte-rss.xml \
	build/datenspuren/2013/mitschnitte-rss.xml \
	build/datenspuren/2014/mitschnitte-rss.xml

AUTOTOPIA_PAGES:=$(patsubst content/autotopia/%.xml, build/autotopia/%.html, $(wildcard content/autotopia/**/*.xml))

# Quellen
NEWSITEMS:=$(wildcard content/news/*.xml)
#CLEAN=$(CONTENT) $(NEWSFILES) $(WWW_CCC_DE_UPDATES) $(CALENDAR_SUMMARY) $(DS_PAGES) $(DS_FEEDS)
CLEAN=$(CONTENT) $(NEWSFILES) $(CALENDAR_SUMMARY) $(DS_PAGES) $(DS_FEEDS)

NOINPUTVALID:=false
NOVALID:=false

ALL_XSLS = $(wildcard xsl/*.xsl)

define xml_process
	$(PROCESSOR) $(PFLAGS) -o $@ $(STYLE) $<
	@$(NOVALID) || echo $(VALIDATOR) $(VFLAGS_OUT) $@ || \
		{ touch -m -t 197001010000 Makefile $@ ; exit 1 ; }
	@$(NOVALID) || $(VALIDATOR) $(VFLAGS_OUT) $@ || \
		{ touch -m -t 197001010000 Makefile $@ ; exit 1 ; }
endef

### Defaults ###

.PHONY: www
www: $(XML_CATALOG) $(CONTENT)
	mkdir -p $@
	$(RSYNC) $(RSYNC_FLAGS) --exclude=".stamp-*" content/static/ content/old-site/ build/ $@

.PHONY: export
export: www
	mkdir -p $(DESTDIR)
	$(RSYNC) $(RSYNC_FLAGS) --exclude=".git/" www/ $(DESTDIR)/

$(DATESTAMP):
	mkdir -p build
	mkdir -p build/datenspuren/$(DS_YEAR)/
	mkdir -p build/autotopia/
	$(RM) $(wildcard build/.stamp-*)
	touch $@

### Regeln für den XSLT-Prozess ###

build/%.html : NOVALID:=true
#build/news.html: $(WWW_CCC_DE_UPDATES) $(CALENDAR_SUMMARY) $(DATESTAMP)
build/news.html: $(CALENDAR_SUMMARY) $(DATESTAMP)

## Feedgrößen begrenzen!
build/%.xml : MAX_ITEMS=23
build/%.xml : PFLAGS+=--param maxitems $(MAX_ITEMS)
build/%-archiv.xml build/news-archiv-%.xml : MAX_ITEMS=9999999
## Erscheinen regelmäßig, aber guck durch mehr als 23 Items zurück
build/podcast.xml build/pentamedia.xml build/pentaradio.xml build/pentamusic.xml : MAX_ITEMS=100
## Erscheint leider nicht so oft
build/pentacast.xml build/pentacast-ogg.xml : MAX_ITEMS=200

build/news-rss.xml build/news-archiv-rss.xml : STYLE=xsl/rss.xsl
build/news-rss.xml build/news-archiv-rss.xml : PFLAGS+=--stringparam prefix ""
build/news-rss.xml build/news-archiv-rss.xml : VFLAGS_OUT=$(VFLAGS) --schema xsd/rss20.xsd
build/news-rss.xml : PFLAGS+=--stringparam archivefeed news-archiv-rss.xml
build/news-atom.xml build/news-archiv-atom.xml : STYLE=xsl/atom.xsl
build/news-atom.xml build/news-archiv-atom.xml : PFLAGS+=--stringparam prefix ""
build/news-atom.xml build/news-archiv-atom.xml : NOVALID=true
build/news-atom.xml build/news-archiv-atom.xml : VFLAGS_OUT=$(VFLAGS) --relaxng xsd/atom.rng
build/news-atom.xml : PFLAGS+=--stringparam archivefeed news-archiv-atom.xml
build/podcast.xml build/podcast-archiv.xml : STYLE=xsl/podcast.xsl
build/podcast.xml build/podcast-archiv.xml : VFLAGS_OUT=$(VFLAGS) --schema xsd/rss20.xsd
build/podcast.xml build/podcast-archiv.xml : PFLAGS+=--stringparam format audio/mpeg,video/mp4
build/podcast.xml build/podcast-archiv.xml: xsl/podcast.xsl # Does not work with $(STYLE) - why?
build/podcast.xml : PFLAGS+=--stringparam archivefeed podcast-archiv.xml
build/pentaradio.xml build/pentaradio-archiv.xml : STYLE=xsl/podcast.xsl
build/pentaradio.xml build/pentaradio-archiv.xml : VFLAGS_OUT=$(VFLAGS) --schema xsd/rss20.xsd
build/pentaradio.xml build/pentaradio-archiv.xml : PFLAGS+=--stringparam selection pentaradio --stringparam format audio/mpeg,video/mp4
build/pentaradio.xml build/pentaradio-archiv.xml: xsl/podcast.xsl # Does not work with $(STYLE) - why?
build/pentaradio.xml : PFLAGS+=--stringparam archivefeed pentaradio-archiv.xml
build/pentacast.xml build/pentacast-archiv.xml : STYLE=xsl/podcast.xsl
build/pentacast.xml build/pentacast-archiv.xml : VFLAGS_OUT=$(VFLAGS) --schema xsd/rss20.xsd
build/pentacast.xml build/pentacast-archiv.xml : PFLAGS+=--stringparam selection pentacast --stringparam format audio/mpeg,video/mp4
build/pentacast.xml build/pentacast-archiv.xml: xsl/podcast.xsl # Does not work with $(STYLE) - why?
build/pentacast.xml : PFLAGS+=--stringparam archivefeed pentacast-archiv.xml

build/pentacast-ogg.xml build/pentacast-ogg-archiv.xml : STYLE=xsl/podcast.xsl
build/pentacast-ogg.xml build/pentacast-ogg-archiv.xml : VFLAGS_OUT=$(VFLAGS) --schema xsd/rss20.xsd
build/pentacast-ogg.xml build/pentacast-ogg-archiv.xml : PFLAGS+=--stringparam selection pentacast --stringparam format audio/ogg,video/ogg,application/ogg,video/webm
build/pentacast-ogg.xml build/pentacast-ogg-archiv.xml : xsl/podcast.xsl # Does not work with $(STYLE) - why?
build/pentacast-ogg.xml : PFLAGS+=--stringparam archivefeed pentacast-ogg-archiv.xml

build/pentamusic.xml build/pentamusic-archiv.xml : STYLE=xsl/podcast.xsl
build/pentamusic.xml build/pentamusic-archiv.xml : VFLAGS_OUT=$(VFLAGS) --schema xsd/rss20.xsd
build/pentamusic.xml build/pentamusic-archiv.xml : PFLAGS+=--stringparam selection pentamusic --stringparam format audio/mpeg,video/mp4
build/pentamusic.xml build/pentamusic-archiv.xml: xsl/podcast.xsl # Does not work with $(STYLE) - why?
build/pentamusic.xml : PFLAGS+=--stringparam archivefeed pentamusic-archiv.xml

build/pentamedia.xml build/pentamedia-archiv.xml : STYLE=xsl/podcast.xsl
build/pentamedia.xml build/pentamedia-archiv.xml : VFLAGS_OUT=$(VFLAGS) --schema xsd/rss20.xsd
build/pentamedia.xml build/pentamedia-archiv.xml : PFLAGS+=--stringparam selection pentamedia --stringparam format audio/mpeg,video/mp4
build/pentamedia.xml build/pentamedia-archiv.xml: xsl/podcast.xsl # Does not work with $(STYLE) - why?
build/pentamedia.xml : PFLAGS+=--stringparam archivefeed pentamedia-archiv.xml

#.INTERMEDIATE: news.xml
news.xml: $(NEWSITEMS)
	sh scripts/gen_news.xml.sh $^ > $@

$(NEWSFILES): news.xml $(NAVIGATION) $(STYLE)
	$(call xml_process)

# Calendar
$(CALENDAR_SUMMARY) : STYLE=xsl/calendar-summary.xsl
$(CALENDAR_SUMMARY) : NOVALID:=true
$(CALENDAR_SUMMARY) : VFLAGS_IN=$(VFLAGS)
$(CALENDAR_SUMMARY): content/events.xml news.xml
	$(call xml_process)

# XCal
build/xcal.xml : STYLE=xsl/xcal.xsl
build/xcal.xml : VFLAGS_IN=$(VFLAGS)
build/xcal.xml: $(CALENDAR_SUMMARY) $(STYLE)
	$(call xml_process)

# iCal
build/ical.ics : STYLE=xsl/ical.xsl
build/ical.ics : NOVALID:=true
build/ical.ics : VFLAGS_IN=$(VFLAGS)
build/ical.ics : VFLAGS_OUT=$(VFLAGS)
build/ical.ics: $(CALENDAR_SUMMARY) $(STYLE)
	$(call xml_process)

# iCal with events at our hackerspace, for http://itedd.de/ + https://spaceapi.ccc.de/
build/hq.ics : STYLE=xsl/ical.xsl
build/hq.ics : PFLAGS+=--stringparam location-filter HQ
build/hq.ics : NOVALID:=true
build/hq.ics : VFLAGS_IN=$(VFLAGS)
build/hq.ics : VFLAGS_OUT=$(VFLAGS)
build/hq.ics: $(CALENDAR_SUMMARY) $(STYLE)
	$(call xml_process)

# Remind
build/reminders : STYLE=xsl/remind.xsl
build/reminders : NOVALID:=true
build/reminders : VFLAGS_IN=$(VFLAGS)
build/reminders : VFLAGS_OUT=$(VFLAGS)
build/reminders: $(CALENDAR_SUMMARY) $(STYLE)
	$(call xml_process)

# Playlists
build/%.xspf : VFLAGS_OUT=$(VFLAGS) --relaxng xsd/xspf-1_0.5.rng
build/%.m3u : NOVALID:=true
build/%.pls : NOVALID:=true

# Portal
portal:
	mkdir $@

portal/Makefile : STYLE=xsl/portal2fetch.xsl
portal/Makefile : NOINPUTVALID:=true
portal/Makefile : NOVALID:=true
portal/Makefile: content/portal-config.xml $(STYLE)
	$(call xml_process)

portal/items.xml : STYLE=xsl/portal2items.xsl
portal/items.xml : NOINPUTVALID:=true
portal/items.xml : NOVALID:=true
portal/items.xml: content/portal-config.xml $(STYLE) portal/Makefile news.xml
	$(MAKE) -C portal fetch
	$(call xml_process)

portal/items-sorted.xml : STYLE=xsl/portal-sort.xsl
portal/items-sorted.xml: portal/items.xml $(STYLE)
	$(call xml_process)

build/portal.html : NOINPUTVALID:=true
build/portal.html: portal/items-sorted.xml $(CALENDAR_SUMMARY) $(STYLE) $(CALENDAR_SUMMARY)
	$(call xml_process)

# Pages:
build/%.html: content/pages/%.xml $(STYLE) $(NAVIGATION) $(DATESTAMP) news.xml
	$(call xml_process)

build/calendar.html: content/pages/calendar.xml $(STYLE) $(NAVIGATION) $(DATESTAMP) $(CALENDAR_SUMMARY)
	$(call xml_process)

# News:
build/news/%.html : BASEURL="../"
build/news/%.html: content/news/%.xml $(STYLE) $(NAVIGATION) $(DATESTAMP)
	$(call xml_process)

# Datenspuren

DS_FEEDS=\
	datenspuren-2015-mp4.xml datenspuren-2015-webm.xml datenspuren-2015-opus.xml datenspuren-2015-mp3.xml \
	datenspuren-2016-mp4.xml datenspuren-2016-webm.xml datenspuren-2016-opus.xml datenspuren-2016-mp3.xml \
	datenspuren-2017-mp4.xml datenspuren-2017-webm.xml datenspuren-2017-opus.xml datenspuren-2017-mp3.xml \
	datenspuren-2018-mp4.xml datenspuren-2018-webm.xml datenspuren-2018-opus.xml datenspuren-2018-mp3.xml

datenspuren-2015-mp4.xml:
	$(WGET) -O $@ https://media.ccc.de/c/ds2015/podcast/mp4-hq.xml
datenspuren-2015-webm.xml:
	$(WGET) -O $@ https://media.ccc.de/c/ds2015/podcast/webm-hq.xml
datenspuren-2015-opus.xml:
	$(WGET) -O $@ https://media.ccc.de/c/ds2015/podcast/opus.xml
datenspuren-2015-mp3.xml:
	$(WGET) -O $@ https://media.ccc.de/c/ds2015/podcast/mp3.xml

datenspuren-2016-mp4.xml:
	$(WGET) -O $@ https://media.ccc.de/c/ds2016/podcast/mp4-hq.xml
datenspuren-2016-webm.xml:
	$(WGET) -O $@ https://media.ccc.de/c/ds2016/podcast/webm-hq.xml
datenspuren-2016-opus.xml:
	$(WGET) -O $@ https://media.ccc.de/c/ds2016/podcast/opus.xml
datenspuren-2016-mp3.xml:
	$(WGET) -O $@ https://media.ccc.de/c/ds2016/podcast/mp3.xml

datenspuren-2017-mp4.xml:
	$(WGET) -O $@ https://media.ccc.de/c/DS2017/podcast/mp4-hq.xml
datenspuren-2017-webm.xml:
	$(WGET) -O $@ https://media.ccc.de/c/DS2017/podcast/webm-hq.xml
datenspuren-2017-opus.xml:
	$(WGET) -O $@ https://media.ccc.de/c/DS2017/podcast/opus.xml
datenspuren-2017-mp3.xml:
	$(WGET) -O $@ https://media.ccc.de/c/DS2017/podcast/mp3.xml

datenspuren-2018-mp4.xml:
	$(WGET) -O $@ https://media.ccc.de/c/DS2018/podcast/mp4-hq.xml
datenspuren-2018-webm.xml:
	$(WGET) -O $@ https://media.ccc.de/c/DS2018/podcast/webm-hq.xml
datenspuren-2018-opus.xml:
	$(WGET) -O $@ https://media.ccc.de/c/DS2018/podcast/opus.xml
datenspuren-2018-mp3.xml:
	$(WGET) -O $@ https://media.ccc.de/c/DS2018/podcast/mp3.xml

build/datenspuren/$(DS_YEAR)/schedule.xml:
	$(WGET) -O $@ https://talks.datenspuren.de/ds24/schedule/export/schedule.xml

build/datenspuren/$(DS_YEAR)/%html : STYLE=$(DS_STYLE)
build/datenspuren/$(DS_YEAR)/%.html: content/datenspuren/%.xml $(DS_STYLE) news.xml $(DS_SCHEDULE) $(DS_FEEDS)
	$(call xml_process)

build/datenspuren/$(DS_YEAR)/news-rss.xml : STYLE=xsl/rss.xsl
build/datenspuren/$(DS_YEAR)/news-rss.xml : NOVALID:=true
build/datenspuren/$(DS_YEAR)/news-rss.xml : PFLAGS+=--stringparam prefix ds13-
build/datenspuren/$(DS_YEAR)/news-rss.xml : VFLAGS_OUT=$(VFLAGS) --schema xsd/rss20.xsd
build/datenspuren/$(DS_YEAR)/news-atom.xml : STYLE=xsl/atom.xsl
build/datenspuren/$(DS_YEAR)/news-atom.xml : NOVALID:=true
build/datenspuren/$(DS_YEAR)/news-atom.xml : PFLAGS+=--stringparam prefix ds13-
build/datenspuren/$(DS_YEAR)/news-atom.xml : VFLAGS_OUT=$(VFLAGS) --relaxng xsd/atom.rng

build/datenspuren/%/mitschnitte-rss.xml : STYLE=xsl/newsfile-to-podcast.xsl
build/datenspuren/%/mitschnitte-rss.xml : VFLAGS_OUT=$(VFLAGS) --schema xsd/rss20.xsd
build/datenspuren/2009/mitschnitte-rss.xml: content/news/ds09-videomitschnitte.xml $(STYLE)
	$(call xml_process)
build/datenspuren/2010/mitschnitte-rss.xml: content/news/ds10-videomitschnitte-komplett.xml $(STYLE)
	$(call xml_process)
build/datenspuren/2011/mitschnitte-rss.xml: content/news/ds11-mitschnitte-online.xml $(STYLE)
	$(call xml_process)
build/datenspuren/2012/mitschnitte-rss.xml: content/news/ds12-videomitschnitte-komplett.xml $(STYLE)
	$(call xml_process)
build/datenspuren/2013/mitschnitte-rss.xml: content/news/ds13-videomitschnitte-komplett.xml $(STYLE)
	$(call xml_process)
build/datenspuren/2014/mitschnitte-rss.xml: content/news/ds14-mitschnitte-online.xml $(STYLE)
	$(call xml_process)

build/datenspuren/$(DS_YEAR)/pois.json:
	wget -O $@ --post-data="data=[out:json];($(foreach a, 'amenity'='restaurant' 'amenity'='fast_food' 'amenity'='cafe' 'amenity'='ice_cream' 'amenity'='bakery' 'shop'='convenience' 'shop'='supermarket' 'shop'='tobacco' 'amenity'='pub' 'amenity'='biergarten' 'amenity'='drinking_water', $(foreach t, node way relation, $t(51.06,13.70,51.10,13.74)[${a}];)));out center qt;" http://overpass-api.de/api/interpreter

autotopia-events.xml: $(wildcard content/autotopia/events/*.xml)
	sh scripts/gen_news.xml.sh $^ > $@

build/autotopia/%html : STYLE=$(AUTOTOPIA_STYLE)
build/autotopia/%.html : BASEURL=""
build/autotopia/events/%.html : BASEURL="../"
build/autotopia/%.html: content/autotopia/%.xml $(AUTOTOPIA_STYLE) autotopia-events.xml
	$(call xml_process)

### Die neuesten Chaosupdates holen ###
#$(WWW_CCC_DE_UPDATES).orig: $(DATESTAMP)
#	$(RM) $@
#	$(WGET) -O $@ -4 http://www.ccc.de/de/rss/updates.rdf

#$(WWW_CCC_DE_UPDATES): $(WWW_CCC_DE_UPDATES).orig
#	$(VALIDATOR) --recover $< > $@

### Mr. Propper ###
.PHONY: clean
clean:
	$(RM) $(CLEAN) news.xml
	$(RM) -r portal
