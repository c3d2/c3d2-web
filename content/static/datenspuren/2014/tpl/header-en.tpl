<!doctype html>
<html lang="en-us">
	<head>
		<title>Datenspuren 2014 - surveilled and sewed, reboot the nets - 11. Symposium Datenspuren</title>
		<meta charset="utf-8">
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
		<meta content="Symposium Datenspuren - Rebuild the Nets, 2014, Kulturzentrum Scheune, organized by Chaos Computer Club Dresden" name="description">
		<meta content="Symposium Datenspuren,c3d2,Chaos Computer Club,CCC,Dresden,Datenschutz,Privacy,IPv6,Cloud,ePostbrief,de-Mail,Anonymity,informationelle Selbstbestimmung,Surveillance,Data Mining,ETSI,RFID,Mautsystem,Remailer,TCPA,TCG,Bundestrojaner,Vorratsdatenspeicherung,Biometrie,BigData,Freifunk,NSA,Own Infrastructure" name="keywords">
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" name="viewport">
		<meta content="index,follow" name="robots">
		<meta content="English" name="language">
		<link type="image/ico" href="img/favicon.ico" rel="icon">
		<link rel="stylesheet" type="text/css" href="css/fonts.css">
		<link rel="stylesheet" type="text/css" href="css/screen.css">
		<link rel="stylesheet" type="text/css" href="css/all.css">
		<link rel="stylesheet" type="text/css" href="css/mob.css">
	</head>
	<body id="top"><div>
