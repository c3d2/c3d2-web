 <footer>
 <div style="float:left;display:inline-block;">Eine Veranstaltung des:</div><div class="vcard">
 <div class="org" style="float: left;"><a href="https://c3d2.de" target="_blank">&lt;&lt;&lt;/&gt;&gt;</a></div>
 <div class="email"><a href="https://lists.c3d2.de/cgi-bin/mailman/listinfo/datenspuren" target="_blank">EMail</a></div>
 <div class="url"><a href="https://wiki.c3d2.de/Datenspuren_2014" target="_blank">DS14 im Wiki</a></div>
<div class="url"><a href="https://wiki.c3d2.de/Datenspuren" target="_blank">Ältere Datenspuren</a></div>
 <div class="adr hidden">
 <div class="street-address">Lingnerallee 3</div>
 <div class="locality">Dresden</div>
 <div class="region">Sachsen</div>
 <div class="postal-code">01069</div>
 <div class="country-name">Germany</div>
 </div>
<!-- <div class=""></div>
 <div class=""></div>
 <div class=""></div>-->
 </div>
 </footer>
</div></body>

</html>
