<!--
edited: Sept 02 14:17:00 CEST 2014
-->

= Samstag =

Samstag:GroßerSaal
| time  | speaker            | title
|:-----:|:-------------------|:----------------------------------------------------------------------
| 10:00 | Sven Guckes?       | Eröffnung
| 10:30 | Constanze Kurz     | KEYNOTE: Five Eyes
| 11:30 | Chaostreff Chemnitz, Eva Olivin und Robert Verch  | Ich sehe was, was du nicht siehst
| 12:30 | PAUSE              |
| 13:00 | Klaus Kusanowsky   | Wie kannst du Wissen, wer ich bin?
| 14:00 | Jens Kubieziel     | Unsichtbar durch Netz mit I2P
| 15:00 | PAUSE              |
| 15:30 | Cornelius Koelbel  | Identität gewährleisten private Daten schützen
| 17:00 | Paul Balzer        | Hack the (Gesundheits) System, wenigstens 'n bisschen
| 18:15 | Vater              | Was bringt der neue Stadtrat an Daten?
| 20:00 | ???                | Pentanews Gameshow


Samstag:KleinerSaal
| time  | speaker            | title
|:-----:|:-------------------|-----------------------------------------------------------------------
| 11:30 | Leslie Polzer      | kinko.me
| 12:30 | PAUSE              |
| 13:00 | Stefan Boecker     | Digitale Selbstverteidigung, Wie schütze ich mich vor Überwachung
| 14:00 | Dennis Romberg     | Klick here to sell your Soul!
| 15:00 | PAUSE              |
| 15:30 | Emploi             | Sachstand Freifunk Dresden
| 16:30 | Hagen Sankowski    | Lücken in der technischen Selbstverteidigung
| 18:00 | datenkollektiv (WS)| Digitale Verbrauchergemeinschaft, ein Machbarkeitsworkshop

= Sonntag =

Sonntag:GroßerSaal
| time  | speaker            | title
|:-----:|:-------------------|:---------------------------------------------------------------------
| 10:00 | Aschenbrenner      |	Open Government ist doch toll - warum versteht das die Politik nicht?
| 11:00 | herr flupke        | Walden oder Leben am Rand des Deep Web
| 11:45 | PAUSE              |
| 12:00 | Lightning Talks    |
| 14:00 | PAUSE              |
| 14:30 | Matthias Monroy    | Virtuelle Grenzen und immer neue Vorratsdatenspeicherung
| 16:00 | Martin, Christian  | Yes we can Monitor you
| 17:00 | Closing Event      |

Sonntag:KleinerSaal
| time  | speaker            | title
|:-----:|:-------------------|:---------------------------------------------------------------------
| 10:00 | Volker Birk        | p! p
| 11:00 | Sango              | End-to-Display Verschlüsslung zur Verschleierung von Kontakten
| 11:45 | PAUSE              |
| 12:00 | Christian Perle    | Fake Hotspot im Eigenbau
| 14:00 | PAUSE              |
| 14:30 | Nos                | Freie Lehrmaterialien mit freier Software
| 15:30 | Markus Suesz       | Aurora

