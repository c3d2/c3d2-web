#!/bin/sh
# NOTE: tested on debian wheezy (7.0)
set -e

rm -rf fake-ap
mkdir fake-ap
cd fake-ap

lb config \
	--backports false \
	--updates false \
	--security true \
	--architectures i386 \
	--linux-flavours 486 \
	--apt-recommends false \
	--mirror-bootstrap http://ftp.de.debian.org/debian \
	--mirror-chroot http://ftp.de.debian.org/debian \
	--mirror-binary http://ftp.de.debian.org/debian \
	--mirror-chroot-security http://security.debian.org \
	--mirror-binary-security http://security.debian.org \
	--bootappend-live "boot=live config locales=en_US.UTF-8 keyboard-layouts=de"

# we set a password for root instead of using sudo
# echo root:fake | chpasswd
echo "user-setup" > config/package-lists/recommends.list.chroot
echo "console-setup screen vim less mc hostapd udhcpd netwox tcpdump wireless-tools charybdis shellinabox irssi w3m openssh-client stunnel" > config/package-lists/my.list.chroot

tar xzf ../sysconf.tgz -C ./config/includes.chroot/

# NOTE: live-boot understands the "toram" boot option
echo "# lb build 2>&1 | tee build.log"
