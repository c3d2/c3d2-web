alles Freie Informationen und Freie Software!

# Doku für Datenspuren 2014 Website

Die Website wird in Ermangelung der Kompetenz bez. XSLT manuell statisch gebaut, das Makefile wird nicht vom c3d2-web-Makefile aufgerufen.
Noch zu postende Text für c3d2-web / News: https://pentapad.c3d2.de/p/ds14


## Editieren

Änderungen werden bislang in HTML an den Templates im Verzeichnis *tpl* vorgenommen. Links werden relativ gesetzt.
Header, Navigation und Footer (derzeit in Englisch und Deutsch) werden mit <code>make</code> beim Bauen zusammengefügt.
Eine Konvertierung aus anderen Fromaten in HTML oder autom. Datenübernahme aus dem Frab ist im Makefile <!-- für folgende Dateien --> bisher nicht vorgesehen!

## Neue Dateien

einfach in das Verzeichnis *tpl* ablegen und neu bauen

## Bauen

Das Makefile baut in das eigene Verzeichnis *2014* und zieht dafür die Templates und *tpl* heran.

``` bash
    cd content/static/datenspuren/2014/
    make
```

Hierbei sollten keine Fehler auftreten.
Anschließend ist es empfehlenswert die zusammengefügte Änderung noch lokal zu überprüfen bevor das ganze publiziert wird.

## Hinweise zu Links und Dateibenennung

tl;dr: s.a. Beispiel für den Vortrag von Nos in *tpl/speaker.html* und das Warmup *tpl/12r1900.html*

Im Programm sollen ggf. lange Texte für Vorträge verlinkt werden.
Dazu folgendes Schema für die ID und die Dateinamen der Vortragsbeschreibungen:

### Vortäge/Teilevents:

    dd    2 Ziffern für das Datum des Tages
    c     ein Zeichen für den Raum:
              r -> Restaurant,
              g -> großer Saal, 
              k -> kleiner Saal usw.
    hhmm  Uhrzeit laut Fahrplan
    .html Dateiendung

### Speaker:

* Quer-Verlinkung in Programm beachten
* Kontaktdaten überprüfen, z.B. PGP-Schlüssel bereitstellen falls gewünscht
* Personenbezogene Daten schohnen
* Vortragsfolien-Verlinkung (ggf. auskommentiert) vorbereiten

### Dokumente / Dateien

* Benennung der Dateien wie die Beschreibungstexte der Vorträge beginnen
* Freie Formate erzwingen, im Zweifel vor Veröffentlichung konvertieren
* Lizenzen (im Verzeichnis *docs*) verlinken und benennen sofern von den Urhebern genannt,
  * Standard:
    - CC-BY-SA für Videos / Vorträge / Doku / Kunst
    - GPL-V3 bei Code
* Dokumente von externe Links ggf. spiegeln

