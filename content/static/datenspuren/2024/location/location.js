const hasTouchCapability = () => {
  return 'ontouchstart' in window || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
};

const isSmallScreen = () => {
  const smallScreenWidth = 768;
  return window.innerWidth <= smallScreenWidth;
};


document.addEventListener('DOMContentLoaded', async () => {
  const isTouchDevice = hasTouchCapability();
  const isSmallScreenDevice = isSmallScreen();

  console.log('Touch capability:', isTouchDevice);
  console.log('Small screen:', isSmallScreenDevice);


  if (hasTouchCapability() && isSmallScreen()) {
    console.log('Small touch screen detected, disabling zoom');
    const viewportMeta = document.createElement('meta');
    viewportMeta.name = 'viewport';
    viewportMeta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no';
    document.head.appendChild(viewportMeta);
  }


  const scene = document.getElementById('location-scene');
  const levels = [
    scene.getElementsByClassName('level0')[0],
    scene.getElementsByClassName('level1')[0],
    scene.getElementsByClassName('level2')[0]
  ]
  let isDragging = false;
  let startX, startY;
  let currentRotation = 0;
  let animationStartTimer = null;

  scene.addEventListener('mousedown', startDrag);
  scene.addEventListener('touchstart', startDrag, { passive: false });
  document.addEventListener('mousemove', drag);
  document.addEventListener('touchmove', drag, { passive: false });
  document.addEventListener('mouseup', endDrag);
  document.addEventListener('touchend', endDrag);
  document.addEventListener('mouseleave', endDrag);

  function startDrag(e) {
    isDragging = true;
    if (animationStartTimer) {
      clearTimeout(animationStartTimer);
      animationStartTimer = null;
    }
    if (e.type === 'mousedown' && e.button !== 0) return; // Only respond to left mouse button
    const point = e.type.includes('touch') ? e.touches[0] : e;
    startX = point.clientX;
    startY = point.clientY;
    levels.forEach(level => {
      level.style.animationPlayState = 'paused';
      level.style.animation = 'none';
    });
    e.preventDefault(); // Prevent text selection and scrolling
  }

  const animationParams = {
    perspective: 600,
    rotateX: 50,
    duration: 23,
    levels: [
      {
        class: 'level0',
        translateZ: 8
      },
      {
        class: 'level1',
        translateZ: 9
      },
      {
        class: 'level2',
        translateZ: 10
      }
    ]
  };

  function drag(e) {
    if (!isDragging) return;

    const point = e.type.includes('touch') ? e.touches[0] : e;
    const deltaX = point.clientX - startX;
    const deltaY = point.clientY - startY;
    const sensitivity = 0.5;

    currentRotation -= (deltaX + deltaY) * sensitivity;

    animationParams.levels.forEach((levelParams, index) => {
      const level = levels[index];
      const transformStyle = `
          perspective(${animationParams.perspective}px)
          rotateX(${animationParams.rotateX}deg)
          rotateZ(${currentRotation}deg)
          translateZ(${levelParams.translateZ}em)
        `;
      level.style.setProperty('transform', transformStyle);
    });

    startX = point.clientX;
    startY = point.clientY;

    e.preventDefault(); // Prevent scrolling on touch devices
  }

  function endDrag() {
    if (!isDragging) return;
    isDragging = false;
    animationStartTimer = setTimeout(() => {
      levels.forEach(level => {
        level.style.animationPlayState = 'running';
        level.style.animation = `${animationParams.duration}s linear infinite ${level.className}`;
      });
    }, 3000);
  }

  // Add event listeners to SVG path elements
  const svgPaths = document.querySelectorAll('#location-scene svg path');


  //load the current schedule from the server
  const loadSchedule = async () => {
    const response = await fetch('https://talks.datenspuren.de/ds24/schedule/export/schedule.json');
    const schedule = await response.json();
    // now get days and rooms
    let rooms = {};
    schedule.schedule.conference.days.forEach(day => {
      Object.entries(day.rooms).forEach(([roomName, room]) => {
        const roomExisting = rooms[roomName] || [];
        rooms[roomName] = roomExisting.concat(room);
      });
    });
    console.log(rooms);
    return rooms
  };
  const allEvents = await loadSchedule();

  const getCurrentEvent = (roomName) => {
    if (allEvents[roomName]) {
      const now = dayjs();
      const currentEvent = allEvents[roomName].find(event => {
        const startTime = dayjs(event.date);
        const [hours, minutes] = event.duration.split(':').map(Number);
        const endTime = startTime.add(hours, 'hour').add(minutes, 'minute');
        return now.isAfter(startTime) && now.isBefore(endTime);
      });

      if (currentEvent) {
        return currentEvent;
      } else {
        // Find the closest future event
        const futureEvents = allEvents[roomName].filter(event =>
          dayjs(event.date).isAfter(now)
        );

        if (futureEvents.length > 0) {
          return futureEvents.reduce((closest, event) =>
            dayjs(event.date).isBefore(dayjs(closest.date)) ? event : closest
          );
        }

        return null;
      }
    }
  };


  const allRomms = [
    "Henny Brenner Saal",
    "Kleiner Saal",
    "Seminarraum",
    "IZ",
    "Bar",
    "Kabinett",
    "HQ",
    "Farbwerk kleiner Raum",
    "Hof 1",
    "Hof 2"
  ]
  const room2Id = {
    "kleiner_saal": "Kleiner Saal",
    "HQ": "HQ",
    "grosser_saal": "Henny Brenner Saal",
    "seminarraum": "Seminarraum",
    "IZ": "IZ"
  }


  function showEventPopup(event, x, y, triggerElement) {
    const popup = document.getElementById('eventpopup');

    const parentSvg = triggerElement.closest('svg');
    const svgRect = parentSvg.getBoundingClientRect();
    const triggerRect = triggerElement.getBoundingClientRect();

    // Calculate the center of the trigger element relative to the SVG
    const triggerCenterX = triggerRect.left + triggerRect.width / 2 - svgRect.left;

    // Determine if the trigger is more to the left or right of the SVG
    const isMoreLeft = triggerCenterX < svgRect.width / 2;

    // Set the reference point (left or right corner of the trigger element)
    let referenceX;
    if (isMoreLeft) {
      referenceX = triggerRect.left;
    } else {
      referenceX = triggerRect.right;
    }

    // Calculate the popup position
    let popupX;
    const popupWidth = 300; // Assuming the popup width is 300px as per CSS
    if (isMoreLeft) {
      popupX = referenceX;
    } else {
      popupX = referenceX - popupWidth;
    }

    // Ensure the popup stays within the viewport
    const viewportWidth = window.innerWidth;
    if (popupX < 0) {
      popupX = 0;
    } else if (popupX + popupWidth > viewportWidth) {
      popupX = viewportWidth - popupWidth;
    }

    // Set the calculated X position
    x = popupX;
    // Position the popup
    popup.style.left = `${x}px`;
    popup.style.top = `${y}px`;

    // Fill the details
    document.getElementById('event-title').textContent = event.title;
    document.getElementById('event-description').textContent = event.abstract;
    document.getElementById('event-author').textContent = event.persons[0]?.public_name;
    document.getElementById('event-url').href = event.url;
    const timeSlotElement = document.getElementById('time-slot');
    const now = new Date();
    const startTime = dayjs(event.date);
    const duration = event.duration.split(':');
    const endTime = startTime.add(Number(duration[0]), 'hour').add(Number(duration[1]), 'minute');

    if (now >= startTime && now < endTime) {
      // Event is currently running
      const remainingMinutes = Math.floor((endTime - now) / 60000);
      timeSlotElement.textContent = `Running now (${remainingMinutes} minutes remaining)`;
    } else {
      // Event is in the future or has ended
      const options = { hour: '2-digit', minute: '2-digit' };
      const formattedStart = startTime.format('HH:mm');
      const formattedEnd = endTime.format('HH:mm');
      timeSlotElement.textContent = `${formattedStart} to ${formattedEnd}`;
    }

    const avatarImg = document.getElementById('event-avatar');
    if (event.logo) {
      avatarImg.src = event.logo;
      avatarImg.alt = `${event.persons[0]?.public_name}'s avatar`;
      avatarImg.style.display = "block";
    } else {
      avatarImg.style.display = "none";
    }

    popup.style.display = 'block';
    function closePopup(e) {
      if (!popup.contains(e.target) && e.target !== triggerElement) {
        popup.style.display = 'none';
        document.removeEventListener('click', closePopup);
        document.removeEventListener('touchstart', closePopup);
      }
    }
    // Close popup when clicking outside
    document.addEventListener('click', closePopup);
    document.addEventListener('touchstart', closePopup);
  }



  svgPaths.forEach(path => {
    const originalFill = path.style.fill || path.getAttribute('fill') || 'none';
    const originalFillOpacity = path.style.fillOpacity || path.getAttribute('fill-opacity') || '1';

    path.addEventListener('mouseenter', () => {
      path.style.fill = 'red';
      path.style.fillOpacity = '1';
    });

    function handlePathClick(event, x, y) {
      const parentSVGClass = path.closest('svg').getAttribute('class');
      if (room2Id[path.id]) {
        const roomName = room2Id[path.id];
        const currentEvent = getCurrentEvent(roomName);
        showEventPopup(currentEvent, x, y, path);
      }
    }

    path.addEventListener('click', (event) => {
      const scrollX = window.pageXOffset || document.documentElement.scrollLeft;
      const scrollY = window.pageYOffset || document.documentElement.scrollTop;
      const x = event.clientX + scrollX;
      const y = event.clientY + scrollY;
      handlePathClick(event, x, y);
    });
    path.addEventListener('touchstart', (event) => {
      const touch = event.touches[0];
      const scrollX = window.pageXOffset || document.documentElement.scrollLeft;
      const scrollY = window.pageYOffset || document.documentElement.scrollTop;
      const x = touch.clientX + scrollX;
      const y = touch.clientY + scrollY;
      handlePathClick(event, x, y);
    });



    path.addEventListener('mouseleave', () => {
      path.style.fill = originalFill;
      path.style.fillOpacity = originalFillOpacity;
    });
  });
});
