$(document).ready(function(){
  $(window).on('scroll', function() {
  if ($(window).width() <= 768) {
    var docViewTop = parseInt($(window).scrollTop());
    var docViewBottom = parseInt(docViewTop + $(window).height());

    var elemTop = parseInt($('#idAbout').offset().top);
    var elemBottom = parseInt(elemTop + $('#idAbout').height());  
      if (docViewTop <= docViewBottom - docViewTop) {
        $('header nav').css({'top': '0', 'cursor': 'initial'});
        $('header nav').unbind();
        $('footer').css({'bottom': 0, 'cursor': 'initial', 'border-top': 'none'});
      } else {
        $('header nav').css({'top': '-2.5rem', 'cursor': 'pointer'});
        $('header nav').unbind().click(function() {
          $(".navbar-collapse").collapse('toggle');
        });
        $('footer').css({'bottom': -$('footer').height()+'px', 'cursor': 'pointer', 'border-top': '1.5rem solid #343a40'})
                   .unbind().click(function() {
          var bottom = parseInt($(this).css('bottom'));
          $(this).css('bottom', ((bottom == 0) ? -$(this).height() : 0) + 'px');
        });
      }
    }
  });
  
    
  $( "header .nav-link" ).click(function( event ) {
    event.preventDefault();
    $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, { duration: 1000, easing: 'easeInOutBack' });
    $(".navbar-collapse").collapse('hide');
  });
  
  $('#mapLayout').css('cursor', 'pointer ')
                 .click(function( event ) {
    $('#osmLayers div').remove();
    $('#myModalLabel').text($('#mapLayout').attr('alt'));
    $('#imagepreview').show();
    $('#imagepreview').attr('src', $('#mapLayout').attr('src'));
    $('.modal-content').css('height', 'auto');
    $('#osmLayers').css('height', 'auto');
    $('#imagemodal').modal('show');
  });
  
  $('#mapOsm').css('cursor', 'pointer ')
              .click(function( event ) {
    $('#osmLayers div').remove();
    
    if (confirm('Es werden Daten von Cloudflare und OpenStreetmaps nachgeladen. Dabei wird auf jeden Fall Deine IP Adressen an diese Dienste übermittelt. Willst du das wirklich?')) {
      var modalContentHeight = $(window).height()-$('.modal-dialog').height()-(2*parseInt($('.modal-dialog').css('margin-top'))),
          modalHeader = $('.modal-header'),
          modalHeaderHeight = parseInt(modalHeader.css('margin-top')) + parseInt(modalHeader.css('padding-top')) + Math.abs(modalHeader.height()) + parseInt(modalHeader.css('margin-bottom')) + parseInt(modalHeader.css('padding-bottom')),
          modalFooter = $('.modal-footer'),
          modalFooterHeight = parseInt(modalFooter.css('margin-top')) + parseInt(modalFooter.css('padding-top')) + Math.abs(modalFooter.height()) + parseInt(modalFooter.css('margin-bottom')) + parseInt(modalFooter.css('padding-bottom'));          

      $('.modal-content').height(modalContentHeight);
      $('#osmLayers').height(modalContentHeight-modalHeaderHeight-modalFooterHeight);
      loadOl('osmLayers');
      $('#myModalLabel').text($('#mapOsm').attr('alt'));
      $('#imagepreview').hide();
      $('#imagemodal').modal('show');
    }
  });
});

function loadOl(target, wait) {
  wait = wait || 1000;

  setTimeout(function() {
    if (typeof ol !== 'undefined') {
      loadMap(target);
    } else {
      var osmScript = document.getElementById('osmScript');
      if (osmScript === null) {
        var newScript = document.createElement("script");
        newScript.src = 'https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.1.1/ol.js';
        newScript.id = 'osmScript';
        document.body.appendChild(newScript);
      }
      var osmStyles = document.getElementById('osmStyles');
      if (osmStyles === null) {
        var newStyles = document.createElement("link");
        newStyles.href = 'https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.1.1/ol.css';
        newStyles.type = 'text/css';
        newStyles.rel = 'stylesheet';
        newStyles.id = 'osmStyles';
        document.head.appendChild(newStyles);
      }
      loadOl(target, 100);
    }   
  }, wait)
}

function loadMap(t) {
  t = t || 'poi';
  
  var layerCenter = new Array(),
      cityCenter = [13.7430, 51.0600],
      myCoords = [13.727849, 51.081209];
  
  var mousePositionControl = new ol.control.MousePosition({
    coordinateFormat: ol.coordinate.createStringXY(4),
    projection: 'EPSG:4326'
  });

  layerCenter = cityCenter;
  
  var map = new ol.Map({
    controls: ol.control.defaults({
      attributionOptions: {
        collapsible: false
      }
    }).extend([mousePositionControl]),    
    target: t,
    layers: [
      new ol.layer.Tile({
      source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat(layerCenter),
      zoom: 12
    })
  });

  var marker = document.createElement('img'),
      ratio = 152/49,
      h = 10;
 
  marker.src = 'assets/images/c3d2-logo.svg';
  marker.style.height = h+'px';

  map.addOverlay(new ol.Overlay({
    position: ol.proj.fromLonLat(myCoords),
    offset: [-(h*ratio/2), -(h/2)],
    element: marker
  }));
}
jQuery.easing.jswing=jQuery.easing.swing;jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,f,a,h,g){return jQuery.easing[jQuery.easing.def](e,f,a,h,g)},easeInQuad:function(e,f,a,h,g){return h*(f/=g)*f+a},easeOutQuad:function(e,f,a,h,g){return -h*(f/=g)*(f-2)+a},easeInOutQuad:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f+a}return -h/2*((--f)*(f-2)-1)+a},easeInCubic:function(e,f,a,h,g){return h*(f/=g)*f*f+a},easeOutCubic:function(e,f,a,h,g){return h*((f=f/g-1)*f*f+1)+a},easeInOutCubic:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f+a}return h/2*((f-=2)*f*f+2)+a},easeInQuart:function(e,f,a,h,g){return h*(f/=g)*f*f*f+a},easeOutQuart:function(e,f,a,h,g){return -h*((f=f/g-1)*f*f*f-1)+a},easeInOutQuart:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f+a}return -h/2*((f-=2)*f*f*f-2)+a},easeInQuint:function(e,f,a,h,g){return h*(f/=g)*f*f*f*f+a},easeOutQuint:function(e,f,a,h,g){return h*((f=f/g-1)*f*f*f*f+1)+a},easeInOutQuint:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f*f+a}return h/2*((f-=2)*f*f*f*f+2)+a},easeInSine:function(e,f,a,h,g){return -h*Math.cos(f/g*(Math.PI/2))+h+a},easeOutSine:function(e,f,a,h,g){return h*Math.sin(f/g*(Math.PI/2))+a},easeInOutSine:function(e,f,a,h,g){return -h/2*(Math.cos(Math.PI*f/g)-1)+a},easeInExpo:function(e,f,a,h,g){return(f==0)?a:h*Math.pow(2,10*(f/g-1))+a},easeOutExpo:function(e,f,a,h,g){return(f==g)?a+h:h*(-Math.pow(2,-10*f/g)+1)+a},easeInOutExpo:function(e,f,a,h,g){if(f==0){return a}if(f==g){return a+h}if((f/=g/2)<1){return h/2*Math.pow(2,10*(f-1))+a}return h/2*(-Math.pow(2,-10*--f)+2)+a},easeInCirc:function(e,f,a,h,g){return -h*(Math.sqrt(1-(f/=g)*f)-1)+a},easeOutCirc:function(e,f,a,h,g){return h*Math.sqrt(1-(f=f/g-1)*f)+a},easeInOutCirc:function(e,f,a,h,g){if((f/=g/2)<1){return -h/2*(Math.sqrt(1-f*f)-1)+a}return h/2*(Math.sqrt(1-(f-=2)*f)+1)+a},easeInElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return -(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e},easeOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return g*Math.pow(2,-10*h)*Math.sin((h*k-i)*(2*Math.PI)/j)+l+e},easeInOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k/2)==2){return e+l}if(!j){j=k*(0.3*1.5)}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}if(h<1){return -0.5*(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e}return g*Math.pow(2,-10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j)*0.5+l+e},easeInBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*(f/=h)*f*((g+1)*f-g)+a},easeOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*((f=f/h-1)*f*((g+1)*f+g)+1)+a},easeInOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}if((f/=h/2)<1){return i/2*(f*f*(((g*=(1.525))+1)*f-g))+a}return i/2*((f-=2)*f*(((g*=(1.525))+1)*f+g)+2)+a},easeInBounce:function(e,f,a,h,g){return h-jQuery.easing.easeOutBounce(e,g-f,0,h,g)+a},easeOutBounce:function(e,f,a,h,g){if((f/=g)<(1/2.75)){return h*(7.5625*f*f)+a}else{if(f<(2/2.75)){return h*(7.5625*(f-=(1.5/2.75))*f+0.75)+a}else{if(f<(2.5/2.75)){return h*(7.5625*(f-=(2.25/2.75))*f+0.9375)+a}else{return h*(7.5625*(f-=(2.625/2.75))*f+0.984375)+a}}}},easeInOutBounce:function(e,f,a,h,g){if(f<g/2){return jQuery.easing.easeInBounce(e,f*2,0,h,g)*0.5+a}return jQuery.easing.easeOutBounce(e,f*2-g,0,h,g)*0.5+h*0.5+a}});