/**
 * Partially from http://diveintohtml5.org/everything.html
 *
 * But we don't want audio/* as <video>
 */
function canPlayVideo(type) {
    var v = document.createElement('video');
    return !!(v.canPlayType &&
	      v.canPlayType(type).replace(/no/, '') &&
	      !(/^audio\//.test(type)));
}

$(document).ready(function() {
    /* Iterate over all resources in HTML output */
    $('.resource').each(function() {
	var that = $(this);
	var preview = that.data('preview');
	var sources = [];
	that.find('a').each(function() {
	    var a = $(this);
	    var type;
	    if ((type = a.attr('type')))
		sources.push({ href: a.attr('href'), type: type });
	});


	/* Check playability */
	if (sources.map(function(r) {
			    return r.type;
			}).some(canPlayVideo)) {
	    var img = that.find('img');
	    img.before('<p class="play">▶</p>');
	    var poster = img.attr('src');
	    var isPreview = false;
	    that.find("img, .play").mouseover(function() {
		img.attr('src', preview);
	    }).
	    mouseout(function() {
		img.attr('src', poster);
	    }).
	    click(function() {
		that.find(".play").remove();
		var video = $('<video class="video-js vjs-default-skin" controls="controls"></video>');
		video.attr('poster', poster);
		sources.forEach(function(source) {
		    var src = $('<source></source>');
		    src.attr('src', source.href);
		    src.attr('type', source.type);
		    video.append(src);
		});
		img.replaceWith(video);
		new VideoJS(video[0], { width: 680,
					height: Math.floor(3 * 680 / 4) }).play();
	    });
	}
    });
    $('head').append(
	'<script src="script/video-js/video.min.js" type="text/javascript" charset="utf-8"></script>' +
	'<link rel="stylesheet" href="script/video-js/video-js.min.css" type="text/css" media="screen" title="Video JS" charset="utf-8">'
    );
});
