(function() {
    function init() {
        var el = document.getElementById('chat');
        el.innerHTML = '<form id="chat-form">' +
            '<p><label for="chat-nick">Name: </label><input id="chat-nick" name="nick" size="20"/></p>' +
            '<p><label for="chat-obscurity">Welche Stadt beherbergt den C3D2? </label><input id="chat-obscurity" size="20"/></p>' +
            '<p><input id="chat-submit" value="Chat!" type="submit"/></p>' +
            '</form>';
        document.getElementById('chat-form').addEventListener('submit', chat);
    }

    // LULZ if you had to read until to get to the answer.
    var secret = [100, 114, 101, 115, 100, 101, 110].map(function(c) { return String.fromCharCode(c); }).join("");
    function chat(ev) {
        ev.preventDefault();

        var nick = document.getElementById('chat-nick').value;
        var obscurity = document.getElementById('chat-obscurity').value;
        if (obscurity.toLocaleLowerCase() === secret) {
            document.location = "/candy/?candy-nick=" + encodeURIComponent(nick);
        } else {
            document.getElementById('chat-submit').parentElement.innerHTML = '<h2>Go away, spammers!<h2>';
        }
        return false;
    }

    window.addEventListener('load', init);
})();
