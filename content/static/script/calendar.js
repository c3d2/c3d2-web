function hilight_today() {
    var now = new Date();
    var ClassName = 'today';
    var today = document.getElementById( "eventday-" +
        //now.getFullYear() + "-" +
        (now.getMonth() + 1) + "-" +
        now.getDate() );
    var oldid = document.getElementsByClassName( ClassName )[0].id;
    if( oldid != '')
      var oldtoday = document.getElementById( oldid );

    /* Has document fetching completed enough? */
    if (today) {
        if(oldtoday != null) oldtoday.className = oldtoday.className.replace( /\btoday\b/, '' );
        var td = today;
        var klass = td.getAttribute( 'class' );
        klass = ( klass || "" ) + ' ' + ClassName;
        td.setAttribute( 'class', klass );
    } else {
        /* Not in calendar yet, retry later: */
        window.setTimeout( hilight_today, 100 );
    }
}

window.setTimeout(hilight_today, 1);
