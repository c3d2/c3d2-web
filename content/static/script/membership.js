function escape(s) {
  return s.toString()
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;");
}

function getFormData() {
  const rates = {'foerder': 42, ordentlich: 42, 'reduziert': 13.37};
  const types = {'foerder': 'Fördermitglied', ordentlich: '', 'reduziert': ''};
  return {
    antrag: document.querySelector('input[name="antrag"]:checked').value,
    zustimmung: document.querySelector('input[id="zustimmung"]:checked').value,
    mitgliedschaft: types[document.getElementById("mitgliedschaft").value],
    rate: rates[document.getElementById("mitgliedschaft").value],
    name: document.getElementById("name").value,
    email: document.getElementById("email").value,
    pgp: document.getElementById("pgp").value,
    note: document.getElementById("note").value,
    dtg: document.getElementById("dtg").value,
  };
}

function getXmlData(data) {
  var xmlData=`
<member${(data.mitgliedschaft!=='')?' type="'+escape(data.mitgliedschaft)+'"':''}>
  <name>${escape(data.name)}</name>
  <email>${escape(data.email)}</email>
${(data.pgp!=='')?'  <pgp-fpr>'+escape(data.pgp)+'</pgp-fpr>':''}
${(data.note!=='')?'  <note>'+escape(data.note)+'</note>':''}
  <term rate="${data.rate}" start="${data.dtg}"/>
  <date>${data.dtg}</date>
</member>`;
  return xmlData.trim();
}

function setXmlLink(id) { /*id="ok"*/
  const data = getFormData();
  const link = document.getElementById(id);
  const xmlData = getXmlData(data);
  link.href = 'data:application/xml;charset=utf-8,' + encodeURIComponent(xmlData);
  link.download = data.dtg + '-antrag.xml';
}

function setMailtoLink(to) {
  /* mailto handler can take arguments to, subject, body, cc, bcc */
  const data = getFormData();
  const xmlContent = getXmlData(data);
  let subject = `Antrag vom ${data.dtg} auf ${(data.antrag=='mitglied')?'Mitgliedschaft':'Änderung meiner Daten'}`;
  let emailBody = `Ich beantrage die ${(data.antrag=='mitglied')?'Mitgliedschaft':'Änderung meiner Daten'} im  Netzbiotop Dresden eV. Die Daten sind als XML ausgezeichnet anbei.\n

\`\`\`
${xmlContent}
\`\`\`
`;
  const mailtoLink = `mailto:${to}?subject=${encodeURIComponent(subject.trim())}&body=${encodeURIComponent(emailBody.trim())}`;
  const link = document.getElementById('mailto');
  link.href = mailtoLink;
}

$('.membership-jsform').each(function() {
  var email='vorstand@c3d2.de';
  var container = $(this);
  container[0].classList.remove('noscript');
  container.html(`
<fieldset id="membership">
  <legend><h3>Mitgliedschaft im <em>Netzbiotop Dresden e. V.</em></h3></legend>
  <p>Ich möchte <input type="radio" id="neumitglied" value="neumitglied" name="antrag"><label for="neumitglied">Mitglied werden</label><input type="radio" id="aktualisierung" id="aktualisierung" name="antrag" checked><label for="aktualisierung">meine Mitgliedsdaten aktualisieren</label>.</p>
  <p class="required"><label for="zustimmung"><input type="checkbox" id="zustimmung"> und erkläre mich mit Satzung, Unvereinbarkeitserklärung und Ordnungen einverstanden.</label></p>
  <p class="required"><select name="mitgliedschaft" id="mitgliedschaft"><option id="empty" value="">Mitgliedschaft auswählen</option><option id="foerder" value="foerder">Fördermitgliedschaft</option><option id="ordentlich" value="ordentlich">Ordentliche Mitgliedschaft</option><option id="reduziert" value="reduziert">Ermäßigte Mitgliedschaft</option></select></p>
  <p class="desc"><label for="mitgliedschaft">Art der Mitgliedschaft</label></p>
  <p class="required"><input type="text" id="name" autocomplete="name" placeholder="Frank Nord"></p>
  <p class="desc"><label for="name">Name</label></p>
  <p class="required"><input type="email" id="email" autocomplete="email" placeholder="frank@nord.de"></p>
  <p class="desc"><label for="email">E-Mail</label></p>
  <p><input type="text" id="pgp" placeholder="ABCDEF1100101CCC"></p>
  <p class="desc"><label for="pgp">PGP-Fingerprint (optional, erforderlich für verschlüsselte Komunikation)</label></p>
  <p><textarea rows="4" cols="40" id="note" placeholder="Optional: Text für ergänzende Angaben möglich"></textarea>
  <p class="desc"><label for="note">Bemerkungen, Link auf PGP-Key, Ergänzungen und Hinweise</label></p>
  <p class="required"><input type="date" id="dtg"></p>
  <p class="desc"><label for="dtg">Datum Inkrafttreten</label></p>
  <p style="margin: 2rem 0.5rem 1rem">Antrag als <a href="#membership" download="antrag-netzbiotop.xml" class="button" id="ok">XML generieren</a> und, bestenfalls signiert, <a id="mailto" href="#membership">per E-Mail senden</a>!</p>
</fieldset>`);

  function update() {
    console.log('update!');
    let hold=false;
    let children = document.getElementsByClassName('required');
    if (children !== undefined) {
    Array.from(children).forEach(function(item){
      let child = item;
      let grandchild = child.firstChild;
      if (grandchild.tagName == 'LABEL') {
        child = grandchild;
        grandchild = grandchild.firstChild;
      }
      if(grandchild.value == '' || (grandchild.getAttribute('id')=='zustimmung' && !grandchild.checked)){
        if (grandchild.getAttribute('id') == 'dtg') {
          var d = new Date();
          d.setHours(23);
          d.setDate(1);
          var s = d.toISOString().replace(/T.+/, "");
          document.getElementById('dtg').value=s;
        } else {
          if (grandchild.type=='checkbox') {
            child.setAttribute('required','required');
          }
          grandchild.setAttribute('required','required');
          if(!hold){
            document.getElementById('ok').href = '#'+grandchild.getAttribute('id');
            document.getElementById('mailto').href = '#'+grandchild.getAttribute('id');
          }
          hold=true;
          console.log(grandchild.getAttribute('id')+': "'+grandchild.value+'" – '+hold);
        }
      }else{
        if (grandchild.type=='checkbox') {
          child.removeAttribute('required');
        }
        grandchild.removeAttribute('required');
      }
    });
    }
    if(!hold){
      setMailtoLink(email);
      setXmlLink('ok');
    }
  }
  container.find('input').on('change', update);
  container.find('select').on('change', update);
  container.find('textarea').on('change', update);
  update();
});
