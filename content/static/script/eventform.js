function escape(s) {
  return s.toString()
      .replace(/&/g, "&amp;")
      .replace(/</g, "&lt;")
      .replace(/>/g, "&gt;");
}

function getFormData() {
  return {
    eventType: document.querySelector('input[name="eventtype"]:checked').value,
    title: document.getElementById("title").value,
    startDate: document.getElementById("startdate").value,
    startTime: document.getElementById("starttime").value,
    endDate: document.getElementById("enddate").value,
    endTime: document.getElementById("endtime").value,
    link: document.getElementById("link").value,
    locationName: document.getElementById("location-name").value,
    locationAddress: document.getElementById("location-address").value,
    locationLink: document.getElementById("location-link").value,
    newsImage: document.getElementById("newsimage").value,
    newsImageTitle: document.getElementById("imagetitle").value,
    author: document.getElementById("author").value,
    pubDate: document.getElementById("pubdate").value,
    pubTime: document.getElementById("pubtime").value,
    description: document.getElementById("description").value,
  };
}

function toBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => resolve(reader.result);
    reader.onerror = reject;
    reader.readAsDataURL(file); // Bilddaten als Base64-String lesen
  });
}

function getISODate(date, time='') {
  //~ console.log(date + " T " + time)
  if(date.length < 10 || date.length > 10){return ''};
  //~ if(time.length=0){time=''};
  let apptDate = new Date(date);
  let timeSplit = time.split(':');
  if (timeSplit.length !== undefined && timeSplit.length >= 2 && timeSplit.length <= 3){
    apptDate.setUTCHours(timeSplit[0]);
    apptDate.setUTCMinutes(timeSplit[1]);
    apptDate.setUTCSeconds((timeSplit[2]!==0)?timeSplit[2]:0);
  }
  let iso = apptDate.toISOString();
  //~ console.log(iso)
  return iso.split('.')[0];
}

function getXmlData(data) {
  var xmlData='';
  switch(data.eventType){
    case 'simpleevent':
        xmlData = `<event title="${escape(data.title)}">
    <start>${getISODate(data.startDate, data.startTime)}</start>
${data.endDate!==''?'    <end>'+getISODate(data.endDate, data.endTime)+'</end>':''}
${(data.locationName!=='' || data.locationAddress!=='')?'    <location>'+    ((data.locationLink!=='')?'<link href="'+data.locationLink+'">'+escape(data.locationName)+'</link> ':escape(data.locationName))+((data.locationAddress!=='')?' '+escape(data.locationAddress):'')+'</location>':''}
${(data.link!=='' || data.locationLink!=='')?'    <link>'+((data.link!=='')?data.link:data.locationLink)+'</link>':''}
</event>
`;
      break;
    case 'newsevent':
      xmlData = `<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE item SYSTEM "http://www.c3d2.de/dtd/c3d2web.dtd">

<item title="${escape(data.title)}" date="${(data.pubDate)?getISODate(data.pubDate, data.pubTime):''}" author="${escape(data.author)}">
${(data.newsImage!=='')?'  <image'+((data.newsImageTitle!=='')?' title="'+escape(data.newsImageTitle)+'"':'')+'>'+data.newsImage+'</image>':''}
  <event>
    <start>${getISODate(data.startDate, data.startTime)}</start>
${data.endDate!==''?'    <end>'+getISODate(data.endDate, data.endTime)+'</end>':''}
${(data.locationName!=='' || data.locationAddress!=='')?'    <location>'+    ((data.locationLink!=='')?'<link href="'+data.locationLink+'">'+escape(data.locationName)+'</link> ':escape(data.locationName))+((data.locationAddress!=='')?' '+escape(data.locationAddress):'')+'</location>':''}
${(data.link!=='' || data.locationLink!=='')?'    <link>'+((data.link!=='')?data.link:data.locationLink)+'</link>':''}
  </event>
  ${data.description}
</item>
`;
      break;
  }
  console.log(xmlData);
  return xmlData.trim();
}

$(".event-jsform").each(function() {
  var email='mail@c3d2.de';
  var container = $(this);
  container[0].classList.remove('noscript');
/*
 enforce required fields first
 generate XML für event / news entry 
 */
  var formhtml=`
<fieldset id="event">
  <legend><h3>Neuer Eintrag</h3></legend>
  
  <p><input value="simpleevent" type="radio" name="eventtype" class="eventtype" checked/>Termin<!--
  --><input value="newsevent" type="radio" name="eventtype" class="eventtype">Newseintrag<!-- --></p>
  <p class="desc">Einfacher Termin oder News mit längerer Beschreibung und eigener Seite</p>
  
  <p class="required"><input type="text" id="title" autocomplete="on" placeholder="Pearl Treffen"/></p>
  <p class="desc"><label for="title">Titel</label></p>

  <p><input class="newselm" type="text" id="newsimage" placeholder="onion.png"/></p>
  <p class="newselm desc"><label for="newsimage">data:uri oder <code>./Bild</code> aus <a href="https://gitea.c3d2.de/c3d2/c3d2-web/src/branch/master/content/static/images/news"><code>images/news/</code></a> oder <code>../Bild</code> aus <a href="https://gitea.c3d2.de/c3d2/c3d2-web/src/branch/master/content/static/images/news"><code>images/</code></a></label></p>

  <p><input class="newselm" type="file" accept="image/*" id="localnewsimage" placeholder="~/Bilder/image.png"/></p>
  <p class="newselm desc"><label for="localnewsimage"><code>Bilddatei</code> vom lokalen Rechner auswählen</label></p>

  <p><input class="newselm" type="text" id="imagetitle" placeholder="tl;dr: Was ist zu sehen, Attribution etc."/></p>
  <p class="newselm desc"><label for="imagetitle">Bildbeschreibung, Attribution, Lizenz …</label></p>

  <p class="required"><input class="newselm" type="text" id="author" autocomplete="on" placeholder="Friedrich Nord"/></p>
  <p class="newselm desc"><label for="author">Autor</label></p>

  <p><input class="newselm" type="date" id="pubdate"/></p>
  <p class="newselm desc"><label for="pubdate">Datum Veröffentlichung</label></p>

  <p><input class="newselm" type="time" id="pubtime" step="300"/></p>
  <p class="newselm desc"><label for="pubtime">Zeit Veröffentlichung</label></p>

  <p class="required"><input type="date" id="startdate" autocomplete="on"/></p>
  <p class="desc"><label for="startdate">Datum Start</label></p>

  <p><input type="time" id="starttime" step="300"/></p>
  <p class="desc"><label for="starttime">Zeit Start</label></p>

  <p><input type="date" id="enddate"/></p>
  <p class="desc"><label for="enddate">Datum Ende</label></p>

  <p><input type="time" id="endtime" step="300"/></p>
  <p class="desc"><label for="endtime">Zeit Ende</label></p>

  <p><input type="url" id="link" placeholder="https://pads.yourserver.com/notes"/></p>
  <p class="desc"><label for="link">Link für den Termin – bspw. eine Agenda</label></p>

  <p><input type="text" id="location-name" placeholder="HQ/proc"/></p>
  <p class="desc"><label for="location-name">Ort des Termins</label></p>

  <p><input type="url" id="location-link" placeholder="https://website.de/"/></p>
  <p class="desc"><label for="location-link">Link zum Ort</label></p>

  <p><input type="text" id="location-address" placeholder="Gebäude B, Raum 1.04.01, Riesaer Straße 32, 01127 Dresden"/></p>
  <p class="desc"><label for="location-address">Anschrift, ggf. Gebäude, Raum oder Bundesland, Staat soweit außerhalb der Stadt</label></p>

  <p class="required"><textarea class="newselm" rows="4" cols="40" id="description" placeholder="Beschreibungstext für Newsseite (erlaubtes XML: div, p, br, link, strong, em, ul, ol, li, …)&#xA;&lt;strong&gt;Teaser&lt;/strong&gt;&#xA;&lt;p&gt;Beschreibungstext mit &lt;link href=&quot;https://web.tld/&quot;&gt;link&lt;/link&gt;&lt;/p&gt;
  "></textarea>
  <p class="newselm desc"><label for="description">Beschreibung</label></p>

  <p style="margin: 2rem 0.5rem 1rem">Eintrag <a href="#wie" download="file.xml" class="button" id="ok">in XML generieren</a> und selbst eintragen oder <a href="#wie" id="mailto">als E-Mail</a> senden, am besten signiert!</p>

</fieldset>
`;
  container.html(formhtml);

  /* xml file download */
  function setXmlLink(id) { /*id="ok"*/
    const data = getFormData();
    const link = document.getElementById(id);
    const xmlData = getXmlData(data);
    link.href = 'data:application/xml;charset=utf-8,' + encodeURIComponent(xmlData);
    //~ container.find(id).attr('href', 'data:application/xml;charset=utf-8,' + encodeURIComponent(getXmlData(data)));
    switch (data.eventType) {
      case 'simpleevent':
        link.download = data.startDate + '-event.xml';
        break;
      case 'newsevent':
        link.download = data.startDate + '-news.xml';
        break;
      default:
        console.log(`Sorry, no match for data.eventType "${data.eventType}".`);
    }
  }

    function setMailtoLink(to) {
        /* mailto handler can take arguments to, subject, body, cc, bcc */
        const data = getFormData();
        const xmlContent = getXmlData(data);
        let subject = '';
        let emailBody = `Ich bitte um Eintrag des ${(data.eventType=='newsevent')?'Newseintrags auf der Website unter content/news/':'Termins auf der Website in content/events.xml'}. Die Daten sind als XML ausgezeichnet anbei.\n`;
        switch (data.eventType) {
            case 'newsevent':
                subject = 'Newseintrag' + data.startDate;
                emailBody += `
Titel: ${data.title}
Autor: ${data.author}
${(data.pubDate !== '')?'Veröffentlichung: '+data.pubDate+' '+data.pubTime:''}
Startdatum: ${data.startDate} ${data.startTime}
${(data.endDate !== '')?'Enddatum: '+data.endDate+' '+data.endTime:''}
${(data.link)?'Link: '+data.link:''}
${(data.locationName !== '' || data.locationAddress !== '')?'Ort: '+((data.locationName !== '')?data.locationName+', ':'')+data.locationAddress:''}
${(data.locationLink !== '')?'Ort-Link: '+data.locationLink:''}
${(data.newsImage !== '')?'Bild: in XML':''}
Bildbeschreibung: ${data.newsImageTitle}
Beschreibung: ${data.description}

\`\`\`
${xmlContent}
\`\`\`
`;
                break;
            case 'simpleevent':
                subject = 'Termineintrag: '+data.startDate;
                emailBody = `Event-Typ: ${data.eventType}
Titel: ${data.title}
Startdatum: ${data.startDate} ${data.startTime}
Enddatum: ${data.endDate} ${data.endTime}
Link: ${data.link}
Ort: ${data.locationName}, ${data.locationAddress}
Ort-Link: ${data.locationLink}

\`\`\`
${xmlContent}
\`\`\`
`;
              break;
          default:
              console.log(`Sorry, no match for data.eventType "${data.eventType}".`);
      }

      const mailtoLink = `mailto:${to}?subject=${encodeURIComponent(subject.trim())}&body=${encodeURIComponent(emailBody.trim())}`;
      const link = document.getElementById('mailto');
      link.href = mailtoLink;
  }

  function update() {
    const data = getFormData();
    /* for News Event */
    if (data.eventType=='newsevent') {
      container.find('.newselm').show();

      const fileInput = document.getElementById('localnewsimage');
      fileInput.addEventListener("change", async (event) => {
        const file = event.target.files[0];

        if (file) {
          const base64Image = await toBase64(file);
          if (base64Image.lengt > 2048) {
            document.getElementById('newsimage').value = base64Image;
          }else{}
        }
        fileInput.value = '';
      });
    } else {
      container.find('.newselm').hide();
    }

    /* enforce required fields */
    let hold=false;
    let children = document.getElementsByClassName('required');
    if (children !== undefined) {
    Array.from(children).forEach(function(item){
      let grandchild = item.firstChild;
      if (grandchild.style.display!=='none') {
        if(grandchild.value == ''){
          grandchild.setAttribute('required','required');
          if(!hold){
            document.getElementById("ok").href = '#'+grandchild.getAttribute('id');
            document.getElementById("mailto").href = '#'+grandchild.getAttribute('id');
          }
          hold=true;
        }else{
          grandchild.removeAttribute('required');
        }
      }else if(!hold) {
        grandchild.removeAttribute('required');
      }
    });
    }
    if(!hold){
      setMailtoLink(email);
      setXmlLink('ok');
    }
  }

  container.find('input').on('change', update);
  container.find('textarea').on('change', update);
  update();
});
