# Notizen zum RAID-TA des c3d2 am 18.03.2005 - RAID-1-Teil
# Fragen an: Frank Benkstein <frank@benkstein.net>

# Voraussetzungen
# RAID ist fest im Kernel, nicht als Modul!
# devfs wahlweise, aber hilfreich

# qemu -hda hda -hdb hdb -boot c
# System mit init=/bin/sh booten
# (der init=-Parameter sagt Linux, welchen Prozess es als ersten
# starten soll. normalerweise ist das /sbin/init, welcher nach den
# Eintr�gen in der inittab alle weiteren Prozesse startet)
# Achtung: die Init-Shell kann kein Job-Management
# Strg-C, Strg-Z funktionieren also nicht - kein ping, watch etc.
# benutzen

# Proc-Dateisystem mounten
mount -t proc none /proc
# devfs-Daemon starten
/sbin/devfsd /dev

# ist RAID �berhaupt da?
cat /proc/mdstat

# Platten anschauen
fdisk -l /dev/hda
fdisk -l /dev/hdb

# hdb partitionieren
fdisk /dev/hdb

## neue (prim�re) Partition
## Typ auf RAID autodetect (fd)

# RAID bauen - die zweite Platte ist noch nicht da (missing)
mdadm --create --raid-devices 2 --level 1 /dev/md0 /dev/hdb1 missing
cat /proc/mdstat

# Dateisystem erstellen
mkreiserfs /dev/md0

# ...mounten
mount /dev/md0 /mnt

# Kontrolle
df -h /mnt /

# root-system kopieren (-x ist dein Freund...)
# rsync empfiehlt sich, wenn acls verwendet werden,
# ansonsten auch star (besseres tar)
cp -ax / /mnt

# Kontrolle
df -h /mnt /

# jetzt f�ngt die Coolness an... :-)

# pivot_root wechselt das root-Dateisystem im Linux-VFS aus
# siehe pivot_root(2) und pivot_root(8)
# Vorher:
# /dev/hda1 -> /
# /dev/md0 -> /mnt
# Nachher:
# /dev/hda1 -> /mnt
# /dev/md0 -> /
#
# / wird nach /mnt/mnt verschoben und dann /mnt nach /
pivot_root /mnt /mnt/mnt

# alle vorher gemounteten Dateisystem sind jetzt nat�rlich
# relativ zu /mnt gemountet

# jetzt wird das alte Root freigemacht
umount /mnt/proc
mount -t proc proc /proc
killall devfsd
# mount --move verschiebt einen mount-Point
# /mnt/dev kann nicht ungemountet werden, da es von
# /bin/sh noch benutzt wird
mount --move /mnt/dev /dev
# exec ersetzt das aktuelle Prozess-Image
# da das alte /bin/sh noch /mnt benutzt, muss ein neues
# gestartet werden, das auf / operiert
exec /bin/sh
# am Schluss den devfs-Daemon neu starten
devfsd /dev

umount /mnt # Whoops! - das alte root ist weg

# /dev/hda wird nicht mehr benutzt - das System l�uft jetzt
# von /dev/md0 (==/dev/hdb1)
# bis hier hin kann man ohne Sorgen Reset dr�cken, ohne dass
# Nebeneffekte auftreten
fdisk /dev/hda

## gleiche Partitionierung wie /dev/hdb
## Typ nicht vergessen (fd)

# Partition dem RAID hinzuf�gen
mdadm --add /dev/md0 /dev/hda1

# wer vorsichtig ist, wartet erstmal bis das rebuild fertig ist
cat /proc/mdstat

# fstab und grub.conf anpassen
vi /etc/fstab
vi /boot/grub/grub.conf
# jeweils /dev/hda1 durch /dev/md0 ersetzen

# grub installieren auf hd0 und hd1 - rebuild muss vorher fertig sein!!!
grub
## root (hd0,0)
## setup (hd0)
## root (hd1,0)
## setup (hd1)

# (neues) root read only mounten
mount -o remount,ro /

reboot -f
# oder, cooler
exec /sbin/init

