# Notizen zum RAID-TA des c3d2 am 18.03.2005 - RAID-5-Teil
# Fragen an: Frank Benkstein <frank@benkstein.net>

## Ben�tigt: Kernel mit md, RAID-5 und loopback, mdadm

# folgendes alles als root

# falls existierende Loops ben�tigt werden, folgende Befehle anpassen

# alle bereits bestehenden Loop-Devices l�schen
echo /dev/loop? | xargs -n1 losetup -d

# Loopdevices anlegen
for i in `seq 0 3` ; 
    do dd if=/dev/zero of=disk$i bs=1M count=100
    losetup /dev/loop$i disk$i
done

# Empfehlung: screen oder splitvt mit "watch -n1 cat /proc/mdstat" oben, shell
# unten oder zweites Terminal
# falls nicht, regelm��ig cat /proc/mdstat ausf�hren

# auch nie verkehrt: zweite console mit man mdadm

# RAID anlegen - so einfach!
mdadm --create /dev/md0 --level 5 --raid-devices 4 /dev/loop{0,1,2,3}

# geh�rt ein bestimmtes Blockdevice zu einem RAID?
mdadm --examine /dev/loop1

# bestehendes RAID untersuchen, UUID notieren
mdadm --detail /dev/md0

vi /etc/mdadm.conf

<code>
DEVICE /dev/loop*
ARRAY /dev/md0 uuid=...
</code>

# f�r Faule statt des vorherigen:
# echo 'DEVICE /dev/loop*' >> /etc/mdadm.conf
# mdadm --detail --scan >> /etc/mdadm.conf

# Monitor mode: mailt wichtige Ereignisse (z.B. Ausfall) an Admin.

# Die starndardm��ige Ausgabe ist ziemlich sporadisch, besser vielleicht
# folgendes Script
# dieses Skript wird von mdadm mit zwei oder drei Parametern aufgerufen:
# 1. die Art des Events (MissingSpareEvent, FailEvent, RebuildStarted, ...)
# 2. das RAID-Device, um das es geht (meist /dev/md0)
# 3. das Block-Device, mit dem das Event was zu tun hat (nicht immer)

<code path="/usr/local/sbin/handle-mdadm-events">
#!/bin/sh

FILE=`mktemp /tmp/mdadm-event.XXXXXX`
MAILTO=frank

cat > $FILE << EOF

A $1 event had been detected on $2, details follow.

# mdadm --detail $2 -- begin
`mdadm --detail $2`
# mdadm --detail $2 -- end

EOF

if [ ! -z "$3" ] ; then
    cat >> $FILE << EOF
# mdadm --examine $3 -- begin
`mdadm --examine $3`
# mdadm --examine $3 -- end

EOF
fi

echo "#contents of /proc/mdstat -- begin"
cat /proc/mdstat >> $FILE
echo "#contents of /proc/mdstat -- end"

mail -s "$1 event on $2:`hostname`" "$MAILTO" < $FILE
rm $FILE
</code>

# Monitor mode aktivieren:
mdadm --monitor /dev/md0 --program /usr/sbin/handle-mdadm-events --daemonise
# oder, wenn einem die sporadische Ausgabe reicht
mdadm --monitor /dev/md0 --mail frank --daemonise
# am besten in init-Skript aufnehmen
# wichtig: ein funktonierender lokaler Mailer wird hierbei vorausgesetzt
# falls dies nicht gew�hrleistet ist, statt die Zeile 
# mail -s "$1 event on $2:`hostname`" "$MAILTO" < $FILE
# durch
# cat $FILE > /var/log/$2.log
# oder �hnliches ersetzen

# mal ein bisschen mit dem neuen RAID rumspielen
mdadm --stop /dev/md0       # anhalten
mdadm --assemble /dev/md0   # zusammenbauen lassen

# Dateisystem auf RAID erstellen
mkreiserfs /dev/md0
mount /dev/md0 /mnt
df -h /mnt

# Dateisystem f�llen, z.B.
tar xvjf linux-2.6.10.tar.bz2 -C /mnt
df -h /mnt

# zum sp�teren Vergleich
tar xvjf linux-2.6.10.tar.bz2

# was passiert, wenn eine Platte ausf�llt?
# per Hand setzen mit --fail
# /proc/mdstat und evt. mutt beobachten

mdadm --fail /dev/md0 /dev/loop0

# Platte muss zun�chst entfernt werden
mdadm --remove /dev/md0 /dev/loop0

# nach evt. Neustart und Auswechseln der Platte, neue hinzuf�gen

mdadm --add /dev/md0 /dev/loop0

# Wichtig: /proc/mdstat beobachten!

