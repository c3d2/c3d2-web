require 'xml/xslt'

$xslt = XML::XSLT.new
$xslt.xsl = File::dirname($0) + '/../xsl/ddate.xsl'

def call_xslt(date)
  $xslt.xml = "<ddate-test xmlns='http://www.c3d2.de/ddate-test'>#{date.strftime('%Y-%m-%d')}</ddate-test>"
  result = $xslt.serve
  result.sub!(/^<\?.+?>/, '')
  result.strip
end

def call_prog(date)
  `ddate '+%A, the %e of %B, %Y - %H' #{date.strftime('%d %m %Y')}`.strip
end

day = Time.now
loop do
  puts day.strftime('%Y-%m-%d')
  xslt = call_xslt(day)
  prog = call_prog(day)
  if prog != xslt
    p('xslt' => xslt, 'prog' => prog)
  end
  day += 24 * 60 * 60
end
