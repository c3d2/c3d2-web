var ltx = require('ltx');
var http = require('http');
var fs = require('fs');
var url = require('url');


var running = 0, queue = [];

function resolveSizes(urls, cb) {
    if (running > 0) {
	queue.push(function() {
	    resolveSizes(urls, cb);
	});
	return;
    }
    running++;

    urls = urls.map(url.parse);
    var cl = http.createClient(urls[0].port || 80, urls[0].hostname);

    var pending = 0, results = [],
	done = function() {
	    if (pending < 1) {
		cb(results);

		running--;
		var f = queue.shift();
		f && f();
	    }
	};

    urls.forEach(function(u) {
	var req = cl.request('HEAD', u.pathname, { Host: u.host });
	req.end();
	req.on('response', function(res) {
	    if (res.statusCode === 200)
		results.push({ url: url.format(u),
			       size: res.headers['content-length'] });
	    pending--;
	    done();
	});
	pending++;
    });
    done();
}

function mimeByExt(url) {
    var ext = url.split('.').pop();
    var type = ({ avi: 'video/avi',
		  mp4: 'video/mp4',
		  webm: 'video/webm',
		  ogv: 'video/ogg',
		  flv: 'video/x-flv' })[ext];
    return type || 'application/octet-stream';
}

function processEvent(event) {
    var base = 'http://ftp.c3d2.de/datenspuren/2011/ds11_' + event.id + '_' + event.slug + '.';
    var urls = ['avi', 'mp4', 'webm', 'ogv', 'flv'].map(function(fmt) {
	return base + fmt;
    });
    resolveSizes(urls, function(infos) {
	infos = infos.filter(function(i) {
	    return i.size !== undefined;
	});

	if (infos.length < 1) {
	    console.warn("<!-- No resources for " + event.id + ": " + event.title + " -->");
	    return;
	}
	var resAttrs = {
	    title: event.title,
	    size: infos[0].size,
	    type: mimeByExt(infos[0].url),
	    url: infos[0].url,
	    preview: base + "gif",
	    poster: base + "jpg"
	};
	if (event.id != "0000") {
	    resAttrs['details-link'] = "fahrplan/events/" +
		event.id + ".de.html";
	    resAttrs['feedback-link'] = "https://cccv.pentabarf.org/feedback/DS2011/event/" +
		event.id + ".de.html";
	}
	var res = new ltx.Element('resource', resAttrs);
	infos.slice(1).forEach(function(info) {
	    res.c('alternative', { size: info.size,
				   type: mimeByExt(info.url),
				   url: info.url });
	});
	console.info(res.toString());
    });
}

processEvent({ id: '0000',
	       title: "Eröffnung/Abschluß",
	       slug: "eroeffnung_abschluss"
	     });

var scheduleFile = fs.readFileSync('content/static/datenspuren/2011/fahrplan/schedule.de.xml');
var schedule = ltx.parse(scheduleFile);
schedule.getChildren('day').forEach(function(dayEl) {
    var dayEvents = [];
    var roomIdx = 0;
    dayEl.getChildren('room').forEach(function(roomEl) {
	roomEl.getChildren('event').forEach(function(eventEl) {
	    dayEvents.push({ id: eventEl.attrs.id,
			     time: eventEl.getChildText('start'),
			     room: roomIdx,
			     title: eventEl.getChildText('title'),
			     slug: eventEl.getChildText('slug')
			   });
	});
	roomIdx++;
    });
    dayEvents = dayEvents.sort(function(e1, e2) {
	return (e1.time < e2.time) ? -1 :
	    ((e1.time > e2.time) ? 1 : e1.room - e2.room);
    });
    dayEvents.forEach(processEvent);
});

