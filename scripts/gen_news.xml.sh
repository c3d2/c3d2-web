#!/bin/sh

cat <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE page SYSTEM "http://www.c3d2.de/dtd/c3d2web.dtd">
<page title="News"><news>
EOF

for item in $* ; do
  echo "    <newsfile>${item}</newsfile>"
done

cat <<EOF
  </news>
</page>
EOF
