require 'uri'
require 'net/ftp'

if ARGV.size != 1
  puts "Usage: #{$0} ftp://ftp.skyhub.de/foo/bar/baz/"
  exit
end

uri = URI::parse(ARGV[0])
files = {}

Net::FTP.open(uri.host) { |ftp|
  ftp.passive = true
  ftp.login
  ftp.chdir(uri.path)
  ftp.list.each { |line|
    chunks = line.split(' ', 9)
    size, name = chunks[4], chunks[8]
    files[name] = size.to_i
  }
  ftp.close
}

files.sort { |a,b| a.first <=> b.first }.each { |name,size|
  puts "<resource title=\"#{name}\" size=\"#{size}\" type=\"application/ogg\">#{uri}#{name}</resource>"
}
