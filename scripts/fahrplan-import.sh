#!/usr/bin/env bash

set -e
set -x

CONF=DS2017
ROOT=$(dirname $0)/..

cd $ROOT/content/static/datenspuren/2017
curl -b "_another_frab_session=$FRAB_SESSION" \
     https://frab.cccv.de/en/$CONF/schedule/download_static_export?export_locale=de \
     > $CONF.tar.gz
tar xzf $CONF.tar.gz
rm $CONF.tar.gz

[ -d fahrplan/ ] && git rm -r fahrplan/
mv $CONF/ fahrplan/
git add fahrplan
git commit -m "ds17: fahrplan update"
