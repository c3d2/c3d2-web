#!/bin/bash -e

for i in `ls ./content/news/*.xml`
do
	xmllint --shell <<<'cat //image[not(starts-with(text(), "http"))]/text()' "$i" \
		| sed '/^\/\? [>-]/d' \
		| awk '{ print "./content/static/images/" $1 }' \
		| xargs identify -quiet 2> /dev/null || true
done

