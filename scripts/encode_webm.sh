#!/bin/sh -e


if=$1
of=$2

# No. CPU cores:
threads=4
# Video bitrate:
vb=400k
# Audio bitrate:
ab=96k

ffmpeg -i "$if" \
    -threads $threads \
    -f webm \
    -an \
    -vf hqdn3d=0:0:16:14 \
    -vcodec libvpx \
    -b:v $vb \
    -s 720x404 \
    -pass 1 \
    -passlogfile "$of.ffmpeg2pass" \
    -y /dev/null
ffmpeg -i "$if" \
    -threads $threads \
    -f webm \
    -acodec libvorbis \
    -ab $ab \
    -ac 1 \
    -vf hqdn3d=0:0:16:14 \
    -vcodec libvpx \
    -b:v $vb \
    -s 720x404 \
    -pass 2 \
    -passlogfile "$of.ffmpeg2pass" \
    -y "$of"
