var SLUGS = {
    4509: "hostproofzerofootprintweb20",
    4510: "verdecktekanaele",
    4573: "dezentralegeldschoepfung",
    4602: "micropayment",
    4608: "x2go",
    4612: "unhosted",
    4614: "angriffistdiebesteverteidigung",
    4627: "0zapftisdiejagdaufdenstaatstrojaner",
    4643: "hackawar_teil1"
};

var ltx = require('ltx');
var http = require('http');
var fs = require('fs');
var url = require('url');


var running = 0, queue = [];

function resolveSizes(urls, cb) {
    if (running > 0) {
	queue.push(function() {
	    resolveSizes(urls, cb);
	});
	return;
    }
    running++;

    urls = urls.map(url.parse);
    var cl = http.createClient(urls[0].port || 80, urls[0].hostname);

    var pending = 0, results = [],
	done = function() {
	    if (pending < 1) {
		cb(results);

		running--;
		var f = queue.shift();
		f && f();
	    }
	};

    urls.forEach(function(u) {
	var req = cl.request('HEAD', u.pathname, { Host: u.host });
	req.end();
	req.on('response', function(res) {
	    if (res.statusCode === 200)
		results.push({ url: url.format(u),
			       size: res.headers['content-length'] });
	    pending--;
	    done();
	});
	pending++;
    });
    done();
}

function mimeByExt(url) {
    var ext = url.split('.').pop();
    var type = ({ mp4: 'video/mp4',
		  avi: 'video/avi',
		  webm: 'video/webm',
		  ogv: 'video/ogg',
		  flv: 'video/x-flv' })[ext];
    return type || 'application/octet-stream';
}

var scheduleFile = fs.readFileSync('content/static/datenspuren/2011/fahrplan/schedule.de.xml');
var schedule = ltx.parse(scheduleFile);
schedule.getChildren('day').forEach(function(dayEl) {
    dayEl.getChildren('room').forEach(function(roomEl) {
	roomEl.getChildren('event').forEach(function(eventEl) {
	    var id = eventEl.attrs.id;
	    var title = eventEl.getChildText('title');

	    var base = 'http://ftp.c3d2.de/datenspuren/2011/ds11_' + id + '_' + SLUGS[id] + '.';
	    var urls = ['avi', 'mp4', 'webm', 'ogv', 'flv'].map(function(fmt) {
		return base + fmt;
	    });
	    resolveSizes(urls, function(infos) {
		infos = infos.filter(function(i) {
		    return i.size !== undefined;
		});

		if (infos.length < 1)
		    return;

		var res = new ltx.Element('resource',
					  { title: title,
					    size: infos[0].size,
					    type: mimeByExt(infos[0].url),
					    url: infos[0].url,
					    preview: base + 'gif',
					    poster: base + 'jpg',
					    hide: 'yes' });
		infos.slice(1).forEach(function(info) {
		    res.c('alternative', { size: info.size,
					   type: mimeByExt(info.url),
					   url: info.url });
		});
		console.info(res.toString());
	    });
	});
    });
});

