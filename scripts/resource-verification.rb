#!/usr/bin/env ruby

require 'rexml/document'
require 'uri'
require 'net/http'


def verify_url(url)
  uri = URI::parse(url)

  response = Net::HTTP::start(uri.host, uri.port) { |http|
    http.head(uri.request_uri)
  }

  class << response
    attr_reader :header
  end

  if response.kind_of? Net::HTTPOK
    response['content-length'].to_s.to_i
  else
    raise response.class.to_s
  end
end

def verify_resource(resource)
  url = resource.attributes['url']
  size = resource.attributes['size'].to_i
  print "#{url} "
  $stdout.flush
  begin
    content_length = verify_url url
    if content_length != 0 and content_length != size
      puts "#{content_length}(remote) != #{size}(resource)"
    else
      puts "Ok (#{content_length} B)"
    end
  rescue
    puts $!.to_s
  end
end

Dir::chdir(File::dirname(__FILE__) + "/..")

NEWSDIR = "content/news"
Dir.entries(NEWSDIR).each do |file|
  next if file =~ /^\./

  fn = "#{NEWSDIR}/#{file}"
  begin
    item = REXML::Document.new(File.new(fn)).root

    raise unless item.name == 'item'
    item.each_element('resource') { |r|
      verify_resource r

      r.each_element('alternative') { |a|
        verify_resource a
      }
    }

  rescue Exception => e
    puts "#{fn} is invalid: #{e}"
  end
end
