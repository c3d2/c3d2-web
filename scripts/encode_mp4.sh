#!/bin/sh -e

if=$1
of=$2

# No. CPU cores:
threads=4
# Video bitrate:
vb=400k
# Audio bitrate:
ab=96k


ffmpeg -i "$if" \
    -f mp4 \
    -acodec libfaac \
    -ab $ab \
    -ac 1 \
    -threads $threads \
    -vf hqdn3d=0:0:16:14 \
    -vcodec libx264 \
    -b:v $vb \
    -s 720x404 \
    -profile:v baseline -level 3.0 \
    -movflags +faststart \
    -passlogfile "$of.ffmpeg2pass" \
    -y "$of"
