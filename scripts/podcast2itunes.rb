#!/usr/bin/env ruby

require 'xmlrpc/client'

# https://phobos.apple.com/WebObjects/MZFinance.woa/wa/pingPodcast
cl = XMLRPC::Client.new('phobos.apple.com', '/WebObjects/MZFinance.woa/wa/pingPodcast', nil, nil, nil, nil, nil, true)
p cl.call('weblogUpdates.ping', 'C3D2 Podcast', 'http://www.c3d2.de/podcast.xml')
