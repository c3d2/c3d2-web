#!/bin/sh -e

if=$1
of=$2

# No. CPU cores:
threads=8
# Audio bitrate:
ab=128k

ffmpeg -i "$if" \
    -f mp3 \
    -ab $ab \
    -threads $threads \
    -y "$of"
