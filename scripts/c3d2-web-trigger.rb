#!/usr/bin/env ruby

require 'pty'
require 'thread'
$:.unshift('/home/astro/xmpp4r/lib')
require 'xmpp4r'
require 'xmpp4r/roster'

JID = 'c3d2-web-trigger@pentabarf.org/svn-trigger'
PASSWORD = '***'
NOTIFIERS = ['subversion@pentabarf.org', 'astro@spaceboyz.net']
COMMAND = '/home/astro/make-c3d2-web.sh'
TIMEFORMAT = '%F %R'

#Jabber::debug = true


def execute_in_terminal(cmd)
  output = ''

  begin
    puts "+++ spawning"
    PTY.spawn(cmd) do |r,w,pid|
      while buf = r.gets
        print buf
        output += buf
      end
    end
  rescue Errno::EIO
  rescue Exception => e
    output += "\n#{e.class}: #{e}\n#{e.backtrace.join("\n")}"
  end

  output
end

cl = Jabber::Client.new(Jabber::JID.new(JID))
cl.connect('bubbles.pentabarf.org', 5222)
cl.auth(PASSWORD)

presence = Jabber::Presence.new(:xa, 'Not been triggered yet')
cl.send(presence)

cl.add_message_callback { |msg|
  puts "Received a message from #{msg.from}: #{msg.body}"
  if NOTIFIERS.include? msg.from.strip.to_s and msg.type != :error
    presence.show = :dnd
    presence.status = "Working hard since #{Time.new.strftime(TIMEFORMAT)}"
    cl.send(presence)

    #output = execute_in_terminal(COMMAND).split(/\n/)[-10..-1].join("\n")
    output_lines = execute_in_terminal(COMMAND).split(/\n/)
    if output_lines.size <= 5
      output = output_lines.join("\n")
    else
      output = output_lines[0] +
               "\n[... #{output_lines.size - 4} lines suppressed ...]\n" +
               output_lines[-3..-1].join("\n")
    end

    presence.show = :xa
    presence.status = "Last build at #{Time.new.strftime(TIMEFORMAT)}\n#{output}"
    cl.send(presence)
  end
}

roster = Jabber::Roster::Helper.new(cl)
roster.add_subscription_request_callback { |item,pres|
  roster.accept_subscription(pres.from)
  puts "Subscribed from #{pres.from}"
}

puts "Ready"

Thread.stop

