<?xml version="1.0" encoding="utf-8"?>
<!-- `xsltproc -``-novalid scripts/tidy_links.xsl -``-content/news/pentacast-2-ip.xml` -->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:str="http://exslt.org/strings"
                exclude-result-prefixes="xsl str">

<xsl:output method="xml"
            version="1.0"
            encoding="utf-8"
            indent="yes"/>

<xsl:template match="link">
  <xsl:variable name="href">
    <xsl:choose>
      <xsl:when test="@href"><xsl:value-of select="@href"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="fixedhref" select="str:encode-uri($href, false())"/>

  <link>
    <xsl:if test="$fixedhref != .">
      <xsl:attribute name="href">
        <xsl:value-of select="$fixedhref"/>
      </xsl:attribute>
    </xsl:if>
    <xsl:value-of select="."/>
  </link>
</xsl:template>

<xsl:template match="*">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates/>
  </xsl:copy>
</xsl:template>
<xsl:template match="@*">
  <xsl:copy/>
</xsl:template>

</xsl:stylesheet>
