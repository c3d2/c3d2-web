<!ENTITY % URI "CDATA">
    <!-- a Uniform Resource Identifier, see [RFC2396] -->

<!ENTITY % Inline "(#PCDATA | link | em | strong | image )*">
<!ENTITY % Block "p|pre|dl|ul|ol|news-list">

<!ELEMENT page ((news)|(%Block;)*)>
<!ATTLIST page
  title     CDATA   #IMPLIED
  pagetitle CDATA   #IMPLIED
  >

<!ELEMENT news (newsfile)+>
<!ELEMENT newsfile (#PCDATA)>

<!ELEMENT item ((image)?, (event)?, (%Block;)*, (addendum)?, (resource)*)>
<!ATTLIST item
  title     CDATA   #REQUIRED
  date      CDATA   #REQUIRED
  author    CDATA   #REQUIRED
  >

<!ELEMENT addendum (%Block;)*>

<!ELEMENT resource (alternative)*>
<!ATTLIST resource
  size      CDATA   #REQUIRED
  type      CDATA   #REQUIRED
  hide      (yes|no)  #IMPLIED
  title     CDATA   #IMPLIED
  url       %URI;   #REQUIRED
  poster    %URI;   #IMPLIED
  preview   %URI;   #IMPLIED
  >

<!ELEMENT alternative EMPTY>
<!ATTLIST alternative
  size      CDATA   #REQUIRED
  type      CDATA   #REQUIRED
  url       %URI;   #REQUIRED
  >

<!ELEMENT news-list EMPTY>
<!ATTLIST news-list
  prefix    CDATA   #IMPLIED
  details   CDATA   #REQUIRED
  >

<!ELEMENT p %Inline;>
<!ATTLIST p
  class     CDATA   #IMPLIED
  title     CDATA   #IMPLIED
  >

<!ELEMENT link (#PCDATA | image)*>
<!ATTLIST link
  href      %URI;   #IMPLIED
  title     CDATA   #IMPLIED
  class     CDATA   #IMPLIED
  >

<!ELEMENT image (#PCDATA)>
<!ATTLIST image
  title     CDATA   #REQUIRED
  class     CDATA   #IMPLIED
  thumb     (yes|no)  #IMPLIED
  >

<!ELEMENT event ((start), (end)?, (location)?)>
<!ELEMENT start (#PCDATA)>
<!ELEMENT end (#PCDATA)>
<!ELEMENT location (#PCDATA)>

<!ELEMENT pre (#PCDATA)>
<!ATTLIST pre
  xml:space (preserve) #FIXED 'preserve'
  >

<!ELEMENT em (#PCDATA)>

<!ELEMENT strong (#PCDATA)>

<!ELEMENT dl ((dh)?,(dt|dd)+)>
<!ATTLIST dl
  class     CDATA   #IMPLIED
  >


<!ELEMENT dh %Inline;>
<!ELEMENT dt %Inline;>
<!ELEMENT dd %Inline;>

<!ELEMENT ul ((li)+)>
<!ELEMENT ol ((li)+)>

<!ELEMENT li (#PCDATA | link | em | strong | image | %Block; )*>


