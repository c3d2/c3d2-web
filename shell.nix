with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "env";
  buildInputs = [
    (libxslt.override {
      cryptoSupport = true;
    })
    libxml2
    wget
    rsync
    gnumake
  ];
  shellHook = ''
    unset SOURCE_DATE_EPOCH
  '';
}
