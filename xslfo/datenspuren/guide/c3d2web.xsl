<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format">


  <xsl:template match='p'>
    <xsl:choose>
      <xsl:when test='@class="title"'>
        <fo:block font-weight='bold' text-align='center' padding='2mm 0mm'
            keep-together.within-column="always" keep-with-next.within-column="always">
          <xsl:apply-templates/>
        </fo:block>
      </xsl:when>
      <xsl:otherwise>
        <fo:block font-family='serif' text-align='justify' padding='2mm 0mm'>
          <xsl:apply-templates/>
        </fo:block>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match='ul'>
    <fo:block padding='4mm 0mm'>
      <xsl:apply-templates select='li'/>
    </fo:block>
  </xsl:template>
  <xsl:template match='ul/li'>
    <fo:block padding='2mm 0mm'>
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match='dl'>
    <fo:block>
      <xsl:apply-templates select='dt|dd'/>
    </fo:block>
  </xsl:template>

  <xsl:template match='dl/dt'>
    <fo:block font-weight='bold' padding='1mm 0mm'
              keep-together.within-column="always" keep-with-next.within-column="always">
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match='dl/dd'>
    <fo:block text-align='right' padding-bottom='2mm'
              keep-together.within-column="always">
      <xsl:apply-templates/>
    </fo:block>
  </xsl:template>

  <xsl:template match='link'>
    <fo:basic-link text-decoration="underline" color="blue">
       <xsl:attribute name='external-destination'>
         url('<xsl:value-of select='.'/>')
       </xsl:attribute>
      <xsl:apply-templates/>
    </fo:basic-link>
  </xsl:template>
</xsl:stylesheet>
