<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
  xmlns:guide="http://www.datenspuren.de/#guide"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:template match="/schedule/conference">
  </xsl:template>

  <xsl:template match='/schedule/day/room'>
    <fo:block padding='10mm 2mm' font-size='14pt' font-family='serif' text-align='center'
              keep-together.within-column="always" keep-with-next.within-column="always">
      <fo:block padding='5mm 0mm'>
        <xsl:choose>
          <xsl:when test='../@index="1"'>Samstag, 5. Mai</xsl:when>
          <xsl:when test='../@index="2"'>Sonntag, 6. Mai</xsl:when>
        </xsl:choose>,
      </fo:block>
      <fo:block padding='5mm 0mm'>
        <xsl:value-of select='@name'/>
      </fo:block>
    </fo:block>

    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match='/schedule/day/room/event'>
    <fo:block font-size='14pt' padding='1mm 1mm'
              border-top-style='solid' border-bottom-style='solid'
              border-top-color='black' border-bottom-color='black'
              border-top-width='0.15mm' border-bottom-width='0.15mm'
              keep-together.within-column="always" keep-with-next.within-column="always"
              text-align='center'>
      <fo:block font-weight='bold' font-size='14pt' font-family='serif'>
        <xsl:value-of select='title'/>
      </fo:block>
      <fo:block font-size='12pt'>
        <xsl:value-of select='subtitle'/>
      </fo:block>
      <fo:block font-size='10pt'>
        <xsl:value-of select='start'/> Uhr
      </fo:block>
    </fo:block>

    <fo:block font-style='italic' padding='2mm 0mm 0mm'
              keep-together.within-column="always" keep-with-next.within-column="always">
      <xsl:for-each select='person'>
        <xsl:if test='position() &gt; 1'>, </xsl:if>
        <xsl:value-of select='.'/>
      </xsl:for-each>
    </fo:block>

    <xsl:if test='string-length(abstract) &gt; 1'>
      <fo:block font-size='10pt' font-weight='bold' padding='5mm 0mm 2mm 2mm'
                keep-together.within-column="always" keep-with-next.within-column="always">
        Abstract
      </fo:block>
      <fo:block font-family='serif' text-align='justify'>
        <xsl:value-of select='abstract'/>
      </fo:block>
    </xsl:if>
    <xsl:if test='string-length(description) &gt; 1'>
      <fo:block font-size='10pt' font-weight='bold' padding='5mm 0mm 2mm 2mm'
                keep-together.within-column="always" keep-with-next.within-column="always">
        Beschreibung
      </fo:block>
      <fo:block font-family='serif' text-align='justify'>
        <xsl:value-of select='description'/>
      </fo:block>
    </xsl:if>

    <fo:block padding='3mm 0mm'>
    </fo:block>
  </xsl:template>

  <!-- schedule-table -->
  <xsl:template name='schedule-table'>
    <xsl:variable name='file' select='@file'/>
    <xsl:variable name='day' select='@day'/>

    <xsl:message>1</xsl:message>
      <xsl:for-each select='document($file)//xhtml:table'>
    <xsl:message>2</xsl:message>
        <xsl:if test='($day = string(position())) or not($day)'>
    <xsl:message>3</xsl:message>

          <xsl:if test='not($day)'>
    <xsl:message>4</xsl:message>
            <fo:block font-size='12pt' font-family='serif' font-weight='bold'
                padding='5mm 0mm 1mm' text-align='center'
                keep-together.within-column="always" keep-with-next.within-column="always">
              <xsl:choose>
                <xsl:when test='position() = "1"'>Samstag, 5. Mai</xsl:when>
                <xsl:when test='position() = "2"'>Sonntag, 6. Mai</xsl:when>
              </xsl:choose>
            </fo:block>
          </xsl:if>

          <fo:table table-layout='fixed' width='100%' border-spacing='1mm' border-collapse='collapse'>
            <fo:table-column column-width="12mm"/>
            <!--fo:table-column column-width="72mm"-->
            <fo:table-column column-width="proportional-column-width(1)">
              <xsl:attribute name='number-columns-repeated'>
                <xsl:value-of select='count(xhtml:thead/xhtml:tr/xhtml:th/xhtml:strong)'/>
              </xsl:attribute>
            </fo:table-column>

            <fo:table-header>

              <fo:table-row>
                <xsl:for-each select='xhtml:thead/xhtml:tr/xhtml:th'>
                  <fo:table-cell>
                    <fo:block text-align='center' font-size='10pt' font-weight='bold'>
                      <xsl:value-of select='.'/>
                    </fo:block>
                  </fo:table-cell>
                </xsl:for-each>
              </fo:table-row>
            </fo:table-header>

            <fo:table-body>
    <xsl:message>5</xsl:message>
              <xsl:for-each select='xhtml:tbody/xhtml:tr'>
    <xsl:message>6</xsl:message>
                <fo:table-row>
                  <xsl:for-each select='xhtml:td'>
                    <fo:table-cell padding='0.2mm 1mm' border='0.2mm dotted red' vertical-align='middle' display-align='center'>
                      <xsl:attribute name="number-rows-spanned">
                        <xsl:value-of select="@rowspan"/>
                      </xsl:attribute>

                      <xsl:choose>
                        <xsl:when test='@class="time"'>
    <xsl:message>7</xsl:message>
                          <fo:block font-size='8pt'>
                            <xsl:value-of select='.'/>
                          </fo:block>
                        </xsl:when>
                        <xsl:when test='contains(@class, "room") and contains(@class, "event ")'>
    <xsl:message>8</xsl:message>
                          <xsl:attribute name='border'>0.2mm solid black</xsl:attribute>
                          <fo:block font-size='12pt' font-family='serif' padding='0.5mm'
                              text-align='left' vertical-align='top' display-align='top'>
                            <xsl:value-of select='xhtml:p[@class="title"]/xhtml:a'/>
                          </fo:block>
                          <fo:block font-size='10pt' text-align='right' padding='0.5mm'
                              vertical-align='bottom' display-align='after'>
                            <xsl:for-each select='xhtml:ul[@class="speakers"]/xhtml:li/xhtml:a'>
                              <xsl:if test='position() &gt; 1'>, </xsl:if>
                              <xsl:value-of select="."/>
                            </xsl:for-each>
                          </fo:block>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:attribute name='background-color'>#e0e0e0</xsl:attribute>
                          <fo:block height='1em' font-size='10pt'/>
                        </xsl:otherwise>
                      </xsl:choose>

                    </fo:table-cell>
                  </xsl:for-each>
                </fo:table-row>
              </xsl:for-each>
            </fo:table-body>

          </fo:table>

        </xsl:if>
      </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
