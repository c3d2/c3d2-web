<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:x="http://www.w3.org/1999/xhtml"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  exclude-result-prefixes="x xsl date">

<xsl:output method="xml"
            version="1.0"
            encoding="utf-8"
            indent="yes"/>

<xsl:strip-space elements="table thead tbody tr"/>

<xsl:template match="/x:html">
  <schedule-matrix version="{document('schedule.de.xml')/schedule/conference/release}" ctime="{date:date-time()}">
    <title>
      <xsl:value-of select="//x:p[@class='intro']/x:strong"/>
    </title>
    <subtitle>
      <xsl:value-of select="//x:div[@id='schedule']/x:h1"/>
    </subtitle>

    <xsl:apply-templates select="//x:div[@id='schedule']/x:table"/>
  </schedule-matrix>
</xsl:template>

<xsl:template match="x:table">
  <rooms>
    <xsl:apply-templates select="x:thead/x:tr/x:th/x:strong"/>
  </rooms>
  <table>
    <xsl:apply-templates select="x:tbody/x:tr[string(@class)!='noprint']"/>
  </table>
</xsl:template>

<xsl:template match="x:th/x:strong">
  <room>
    <xsl:apply-templates/>
  </room>
</xsl:template>

<xsl:template match="x:tbody/x:tr">
  <timeslot time="{x:td[@class='time']}">
    <xsl:apply-templates/>
  </timeslot>
</xsl:template>

<xsl:template match="x:tbody/x:tr/x:td[@class='time']"/>

<xsl:template match="x:tbody/x:tr/x:td[@class='room empty']">
  <empty/>
</xsl:template>

<xsl:template match="x:tbody/x:tr/x:td[starts-with(@class,'room event ')]">
  <xsl:variable name="id" select="substring-before(substring-after(x:p[@class='title']/x:a/@href, 'event/'), '.de.html')"/>

  <event slots="{@rowspan}" id="{$id}">
    <xsl:copy-of select="document('schedule.de.xml')/schedule/day/room/event[@id=$id]/*"/>
  </event>
</xsl:template>

</xsl:stylesheet>
