<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:date="http://exslt.org/dates-and-times"
  xmlns="http://www.w3.org/1999/XSL/Format">

<xsl:attribute-set name="table-headers">
  <xsl:attribute name="font-weight">bold</xsl:attribute>
  <xsl:attribute name="font-size">16pt</xsl:attribute>
  <xsl:attribute name="text-align">center</xsl:attribute>
</xsl:attribute-set>

<xsl:template match="/schedule-matrix">
  <root>

    <layout-master-set>

      <simple-page-master master-name="FullPage"
         page-width="210mm"   page-height="297mm"
         margin-top="10mm"  margin-bottom="10mm"
         margin-left="10mm" margin-right="10mm">
        <region-body/>
      </simple-page-master>

    </layout-master-set>

    <page-sequence master-reference="FullPage">
      <flow flow-name="xsl-region-body" font-size="18pt">

        <!--block text-align="right" font-size="8pt">
          Release <xsl:value-of select="@version"/>,
          <xsl:value-of select="concat(date:day-in-month(@ctime),'.',date:month-in-year(@ctime),'.',date:year(@ctime),' ',date:time(@ctime))"/>
        </block-->

        <xsl:apply-templates select="title"/>
        <xsl:apply-templates select="subtitle"/>

          <xsl:variable name="table-width" select="210-15-15-20"/>
          <table table-layout="fixed" width="{$table-width}mm" border-collapse="collapse">

          <table-column column-width="20mm"/>
          <xsl:for-each select="rooms/room">
            <table-column column-width="{$table-width div count(../room)}mm"/>
          </xsl:for-each>

          <table-header>
            <table-row>
              <table-cell>
                <xsl:element name="block" use-attribute-sets="table-headers">
                  Zeit
                </xsl:element>
              </table-cell>
              <xsl:apply-templates select="rooms"/>
            </table-row>
          </table-header>

          <table-body>
            <xsl:apply-templates select="table"/>
          </table-body>

        </table>
      </flow>
    </page-sequence>

  </root>
</xsl:template>

<xsl:template match="title">
  <block font-size="28pt" text-align="center" font-weight="bold">
    <xsl:apply-templates/>
  </block>
</xsl:template>

<xsl:template match="subtitle">
  <block font-size="24pt" text-align="center" font-weight="bold" margin-bottom="10mm">
    <xsl:apply-templates/>
  </block>
</xsl:template>

<xsl:template match="rooms/room">
  <table-cell>
    <xsl:element name="block" use-attribute-sets="table-headers">
      <xsl:apply-templates/>
    </xsl:element>
  </table-cell>
</xsl:template>

<xsl:template match="table/timeslot">
  <xsl:variable name="timeslot" select="position()"/>
  <xsl:variable name="previous-timeslot" select="preceding-sibling::*"/>
  <xsl:message>== <xsl:value-of select="$previous-timeslot/@time"/> ++ <xsl:value-of select="@time"/> ==</xsl:message>
  <table-row>
    <table-cell display-align="center">
      <block text-align="center" font-size="12pt" font-weight="bold" padding-bottom="0.5em">

        <xsl:variable name="posteventempty">
          <xsl:for-each select="*">
            <xsl:variable name="track" select="position()"/>
            <!--xsl:if test="(name(.) = 'empty') and (name(../preceding-sibling::*[position() = $track]) = 'span')"-->
            <xsl:message><xsl:value-of select="$track"/>: <xsl:value-of select="name(../preceding-sibling::timeslot/*[position() = $track])"/> + <xsl:value-of select="name(.)"/></xsl:message>
            <xsl:if test="name(.) != name(../preceding-sibling::timeslot/*[position() = $track])">
              y
            </xsl:if>
          </xsl:for-each>
        </xsl:variable>

        <xsl:if test="(count(span) != count(../../rooms/room)) or contains($posteventempty, 'y')">
          <xsl:value-of select="@time"/>
        </xsl:if>

      </block>
    </table-cell>

    <xsl:apply-templates/>
  </table-row>
</xsl:template>

<xsl:template match="table/timeslot/empty">
  <table-cell margin="0" background-color="#ccc">
    <block>
    </block>
  </table-cell>
</xsl:template>

<xsl:template match="table/timeslot/span"/>

<xsl:template match="table/timeslot/event">
  <table-cell number-rows-spanned="{@slots}"
              border-style="solid" border-width="0.4mm" border-color="#333"
              padding="1mm">
    <block font-weight="bold" font-size="16pt">
      <xsl:value-of select="title"/>
    </block>
    <block font-style="italic" font-size="14pt">
      <xsl:value-of select="subtitle"/>
    </block>
    <block display-align="after" font-size="10pt">
      <xsl:for-each select="persons/person[string(.) != '&lt;&lt;&lt;/&gt;&gt; Network']">
        <xsl:if test="position() &gt; 1">
          <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:value-of select="."/>
      </xsl:for-each>
    </block>
  </table-cell>
</xsl:template>

</xsl:stylesheet>
