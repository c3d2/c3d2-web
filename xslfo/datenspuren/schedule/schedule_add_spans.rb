#!/usr/bin/env ruby

require 'rexml/document'

matrix = REXML::Document.new($stdin).root
tracks = 0
matrix.each_element('rooms/room') {
  tracks += 1
}
trackspans = [0] * tracks
matrix.each_element('table/timeslot') { |timeslot|
  events = []
  timeslot.each_element { |event|
    timeslot.delete(event)
    events << event
  }
  $stderr.puts trackspans.inspect
  tracks.times { |i|
    if trackspans[i] > 1
      timeslot.add(REXML::Element.new('span'))
      trackspans[i] -= 1
    else
      event = events.shift || REXML::Element.new('empty')
      timeslot.add(event)
      if event.name == 'event'
        trackspans[i] = (event.attributes['slots'] || 1).to_i
      end
    end
  }
  $stderr.puts "Stale elements: #{events.inspect}" if events.size > 0
}
puts matrix.to_s
