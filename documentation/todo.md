= was man noch machen könnte

== autom. fetch calendar entries for open data ==

JSON-File: http://offenesdresden.de/events.json
should be joined with content/events.xml
    "title": "Open-Data-Treffen",
title can be used as is
    "date": "2014-09-17T20:00:00+0200",
date needs to be cut by the last 5 chars
    "location": "http://www.c3d2.de/space.html",
location needs to be changed to "GCHQ, Lingerallee 3, Dresden" in case it is "http://www.c3d2.de/space.html", other cases might be necessary in future
    "info": "Allgemeine Infos: http://pad.okfn.org/p/oklab-dresden. Meetingnotizen: http://pad.okfn.org/p/oklab-dresden-treffen"
link shoule be "http://offenesdresden.de"

== errmail for admin

in Makefile soll noch die Benachrichtigung der Admins für Fehler beim build-Prozess eingebaut werden
- z.B. wenn media.c3d2.de nicht erreichbar ist und daher Inhalte nicht wie vorgesehen aktualisiert werden
Hierzu ist nullmailer vorgesehen und muss noch entsprechen befüllt werden
die Empänger könnten z.b. in c3d2-web/xsl/errmailto.xsl definiert werden.

== potal-mediagoblin

eine option für das bekanntgeben eines neuen Bildcodes fehlt im Mediagobline. Derzeit werden alle Bilder von bestimmten Nutzern (john, astro, lachmoewe, bigalex) bekannt gegeben.. nuja.
Wurde teilweise behoben indem ein Nutzer konkrete Kollektionen anstatt seines kompletten Repertoires angeben (lassen) kann.

== hq-status image

* status.png wird vom server der space-api gehandelt, ist keine datei
* static images sind schon im c3d2-web
* hq-full muss noch in der json und im server beachtet werden
* pfade in der json müssen auf c3d2-web umgebogen werden

== description for events.xml

to reduce single event-*.xml files there could be a short-description for events 

== links in location-tag der events.xmls erlauben

wenn hier Links erlaubt werden, ist der Verweis auf Details einfacher.
führt aber dazu, dass Ort und Zeit sauber statt im title des links über ein on-event-span/div angezeigt werden müssten
Für die Umsetzung soll zuvor die Struktur gesäubert werden

== Konsolidierung der XML-Dateien und der Hierarchie

Die Dateinamen der XSL sind teilweise irreführend und die modularisierung nicht leicht nachvollziehbar. 
z.B.
Links auf events im portal (common.xsl) werden nicht durch einen Aufruf des gleichen Template gebaut wie die Links im calender, daraus resultiert eine unnötige doppelte Beschreibung.

== Periodische Termine für calender etc.

Termin: Di 04mar14
* Absprache alternatives Format für Datum = RRULES (iCal)
1. Erstellen einer RRULE-Datei / Vorlage je Wiederholenden Event
2. Parsing in Makefile um die Dateien unter news/ für den aktuellen und den folgenden Monat zu erstellen, existierende nicht überschreiben 
3. existierende Events können editiert werden um Details (Thema, Änderungen) hinzu zu fügen
4. ausgefallene Termine, 2 Vorschläge: 
	I Exclude-Regel in Vorlage
	Z News-Datei mit /ausgefallen/ kennzeichnen

== Termine verbreiten

Manche Termine sollen in die Welt posaunt werden, hier gibt es schon atoms, feeds.. mit sehr geringer Nutzung weil $user meist nur Email haben und eventuell in Listen mitlesen.
Hier wäre es gut Termine und Terminänderungen anwenderfreundlich z.B. über eine ML zu verbreiten.

== Termine scrapen

von einzelnen Projekten könnten durch einen Eintrag die Termine automatisiert von anderen Seiten ausgelesen werden um sie nicht manuell an mehreren Orten veröffentlichen zu müssen.

== weitere Quellen für Portal-Seite
* z.B. Redmine: https://redmine.c3d2.de/projects/ghcq/issues.atom

== <page>

Tag-Attribute:
 nav_title - Gibt den Titel an, der in der Navigation angezeigt werden soll. Wenn nicht vorhanden, ggf. Fallback auf title-Attribut
 title     - Gibt den eigentlichen Seitentitel an

== Shop

 - noch unklar, ob Bilder komplett in Mediagoblin ausgelagert werden sollen
 
== Zusammenfassung der Navigation
 - Für bessere Übersicht bis zu max. einer Seitenabhängigen Unternavigation
   -> Martin macht noch Entwurf dazu mit GIMP
 
 - Navigation:
 
  | Unterseite     |  Unterseiten              |
  ----------------------------------------------
  |  News          |  Keine                    |
  ----------------------------------------------
  |  Kalender      |  Events, Themenabende     |
  ----------------------------------------------
  |  Projekte      |  Schule, Hackerspace, Wiki|
  ----------------------------------------------
  |  Unterstützen  |                           |
  ----------------------------------------------
  |  Pentamedia    |  Pentaradio, -cast, -music |
  ----------------------------------------------
  |  Kontakt       |  Whois, E-Mail, Chat, Impressum  |
  ----------------------------------------------
  |  Downloads     |  Warez (evtl. auch Shop)  |
  ----------------------------------------------
  
  - Links zu Unterseiten ggf. schon vor dem Klick per CSS einblenden
  - Floating der Navigation etc. bei Umsetzung des Designs in HTML u. CSS noch zu klären
    -> Bei Fragen z.B. bei Dodo melden (Mail: dodo@c3d2.de)

== Generelles Seitendesign

- möglichst per HTML5 (http://www.joshduck.com/periodic-table.html)
