# introduction
* [c3d2 wiki](https://wiki.c3d2.de/C3D2-Web)
* [Darstellung von XML-Daten](http://de.selfhtml.org/xml/darstellung/index.htm)
* [How Not to Use XSLT](http://www.sims.berkeley.edu/academics/courses/is290-8/s04/lectures/5/dragons/allslides.html)
* http://bobcat.webappcabaret.net/javachina/faq/xpath_01.htm#xpath_Q15 (b0rked?)
* [Mozilla HTML elements reference](https://developer.mozilla.org/en-US/docs/HTML/Element)

# validation
* [W3C online validation)[http://validator.w3.org/check)
* local: ```make -j4```

# adding content

## events
aka news will be listet in calendar and on the portal on the site and presented as ical, xcal and reminder files to be automatically absorbed by your calendar-sw.

**single events with few details**
can be enlisted in content/events.xml - just see the details of existing ones.

**events with more details**
are thought to have an own xml file in news/ directory named events-{$title}.xml
the format can be copied from existing events there as well. images or other static files for these might be placed in content/static/images/news/ directory.

**repeating events / regular meetings**
are not implemented yet, you have to creaty them multiple times manually.
