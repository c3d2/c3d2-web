
XML muss valide umgesetzt werden
 - heißt auch: & für Parameter in URIs wird immer escaped: &amp;

Template-Parameter:
 - Wann testen, ob gesetzt? (z.B. gleich zu Beginn des Templates)
 
XSL-Stylesheet
 - Wird IMMER explizit als Parameter an xsltproc übergeben, nicht als Attribut in xml-stylesheet-Tag
 - Begründung: Ermöglicht Transformation in verschiedene Formate durch Angabe des entsprechenden XSL-Stylesheets
               (z.B. item-XML-Datei in HTML oder alternativ in RSS transformieren) 

# Features

Podcast-Einbettung
 - mit HTML5 media-Element Play-Knopf
 - Kapitelmarken
 - Poster-JPG + Preview-GIF für Videos

Kalender
 - klein auf Frontpage
 - Volle Kalenderseite zeigt diesen + nächsten Monat
 - Quellen: news mit Events, events.xml, auch Syndizierung anderer
   Websites

Portal:


Versionierung:
 - git

Deployment:
 - static files
 - cronjob für Kalender- und Feed-Aktualisierung
 - dynamische Inhalte mit JSONP von pentamedia.org

Kommentare:
 - Pentamedia.org
 - Sumtcha + Cattcha

MUC:
 - Integration mit Nick input + Submit button
 - Vielleicht auch iframe?

## news pre-posting

some news shall be written right now but published later, thats why the date next to author of the news/events item etc. should be compared and if greater than the current one not considered for publication yet.
