with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "env";
  buildInputs = [
    texlive.combined.scheme-full
  ];
  shellHook = ''
    unset SOURCE_DATE_EPOCH
  '';
}
