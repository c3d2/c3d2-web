# Build should to be run with `--option sandbox false` as it fetches
# resources from the network.

with import <nixpkgs> {};

let
  # Select the assets needed for build but not {build,www,.git} or
  # any temporary stuff.
  sources = [ "Makefile" "content" "dtd" "scripts" "xsd" "xsl" ];
  src = runCommand "c3d2-web-Makefile" {} (
    ''
      mkdir $out
    '' +
    (lib.concatMapStrings (name: ''
        ln -s ${./. + "/${name}"} $out/${name}
      '') sources
    )
  );
in
stdenv.mkDerivation {
  name = "c3d2-web";
  inherit src;
  buildInputs = (import ./shell.nix).buildInputs;
  enableParallelBuilding = true;
  makeFlags = [ "www" ];
  installTargets = [ "export" ];
  installFlags = [ "DESTDIR=$$out" ];
  # 2038-01-19 03:14:08
  SOURCE_DATE_EPOCH = 2147483648;
}
